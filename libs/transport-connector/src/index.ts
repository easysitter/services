import { KafkaOptions } from '@nestjs/microservices';
import { NestApplication } from '@nestjs/core';

export function getClientConfig(): KafkaOptions['options']['client'] {
  return {
    connectionTimeout: 5000,
    retry: {
      retries: 3,
    },
    brokers: process.env.KAFKA_BROKERS.split(','),
    ...(process.env.KAFKA_SASL_USER
        ? {
          sasl: {
            mechanism: 'plain',
            username: process.env.KAFKA_SASL_USER,
            password: process.env.KAFKA_SASL_PASSWORD,
          },
          ssl: true,
        }
        : {}
    ),
  };
}

export function patchNest() {
// @ts-ignore
  NestApplication.prototype.listenToPromise = (microservice) => {
    const { start } = microservice.server;
    microservice.server.start = async (callback) => {
      try {
        await start.apply(microservice.server, [callback]);
      } catch (e) {
        callback(e);
      }
    };
    return new Promise(async (resolve, reject) => {
      microservice.listen((err) => err ? reject(err) : resolve());
    });
  };

  NestApplication.prototype.startAllMicroservices = function(callback) {
    Promise.all(this.microservices.map(this.listenToPromise))
      .then(() => callback && callback())
      .catch(err => callback && (callback as any)(err));
    return this;
  };

  NestApplication.prototype.startAllMicroservicesAsync = function() {
    return new Promise(
      (resolve, reject) => this.startAllMicroservices((err) => err ? reject(err) : resolve()),
    );
  };
}
