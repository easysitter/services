import { EventBusEvent } from '..';

interface NewAttentionPayload {
  type: string;
  refId: string;
  refType: any;
  data: any;
}

export class NewAttention implements EventBusEvent<NewAttentionPayload> {
  static eventName = 'attention';
  name = NewAttention.eventName;

  constructor(
    public readonly payload: NewAttentionPayload,
  ) {
  }
}
