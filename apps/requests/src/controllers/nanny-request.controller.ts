import { BadRequestException, Controller, Get, Logger, NotFoundException, Param, ParseIntPipe, Put, Query } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { NannyRequest } from '../entities/nanny-request.entity';
import { ParentRequest } from '../entities/parent-request.entity';
import { NannyRequestQuery, NannyRequestsQuery } from '../cqrs/queries/impl';
import { AcceptRequestCommand, CloseNannyRequestCommand, RefuseRequestCommand } from '../cqrs/commands/impl';
import { ApiParam, ApiTags } from '@nestjs/swagger';

@Controller('nanny/:nanny/requests')
@ApiTags('nanny')
export class NannyRequestController {
  private nannyCommandMap = {
    accept: AcceptRequestCommand,
    refuse: RefuseRequestCommand,
    close: CloseNannyRequestCommand,
  };

  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Get()
  @ApiParam({ name: 'nanny', required: true, type: Number })
  getRequests(
    @Param('nanny', ParseIntPipe) nanny: NannyRequest['user'],
    @Query('isActive') isActive: '0' | '1' = '1',
  ) {
    return this.queryBus.execute(new NannyRequestsQuery(nanny, isActive === '1'));
  }

  @Get(':request')
  @ApiParam({ name: 'nanny', required: true, type: Number })
  @ApiParam({ name: 'request', required: true, type: Number })
  async getRequest(
    @Param('nanny', ParseIntPipe) user: NannyRequest['user'],
    @Param('request') requestId: ParentRequest['id'],
  ) {
    const request: NannyRequest = await this.queryBus.execute(
      new NannyRequestQuery(requestId, user),
    );
    return request;
  }

  @Put(':request/:action')
  @ApiParam({ name: 'nanny', required: true, type: Number })
  @ApiParam({ name: 'request', required: true, type: Number })
  async nannyAction(
    @Param('nanny', ParseIntPipe) user: NannyRequest['user'],
    @Param('request') requestId: ParentRequest['id'],
    @Param('action') action: string,
  ) {
    const request: NannyRequest = await this.queryBus.execute(
      new NannyRequestQuery(requestId, user),
    );
    if (!this.nannyCommandMap.hasOwnProperty(action)) {
      throw new NotFoundException();
    }
    try {
      await this.commandBus.execute(new this.nannyCommandMap[action](request));
      return request;
    } catch (e) {
      Logger.error(e.message, e.trace, 'Nanny action');
      throw new BadRequestException(e.message);
    }
  }
}
