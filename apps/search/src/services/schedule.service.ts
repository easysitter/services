import { Schedule } from '../entities/schedule.entity';
import { Brackets, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { TimeService } from './time.service';

export interface ScheduleFindOptions {
  day: number;
  timeStart: number;
  timeEnd: number;
  users?: number[];
}

export class ScheduleService {
  constructor(
    @InjectRepository(Schedule)
    private readonly repository: Repository<Schedule>,
    private readonly time: TimeService,
  ) {
  }

  async findAvailable(options: ScheduleFindOptions): Promise<number[]> {
    const queryBuilder = this.repository.createQueryBuilder('s')
      .select('DISTINCT (s.user)', 'user');
    const { timeEnd, timeStart, day } = options;
    const groups = [];
    if (timeEnd < 24 * 60) {
      groups.push({ timeStart, timeEnd, day });
    } else {
      groups.push(
        { timeStart, timeEnd: 1440, day },
        { timeStart: 0, timeEnd: timeEnd - 1440, day: day + 1 },
      );
    }

    queryBuilder.andWhere(new Brackets(groupsBrackets => {
      groups.forEach(group => {
        groupsBrackets.orWhere(new Brackets(timeBrackets => {
          timeBrackets
            .andWhere(':start between s.start and s.end', {
              start: group.timeStart,
            })
            .andWhere(':end between s.start and s.end', { end: group.timeEnd })
            .andWhere('day=:day', { day: group.day });
          return timeBrackets;
        }));
      });
      return groupsBrackets;
    }));

    if (options.users && options.users.length > 0) {
      queryBuilder.andWhere('user in (:users)', { users: options.users });
    }

    queryBuilder
      .groupBy('s.user')
      .having('count(s.id) = :groupCount', { groupCount: groups.length });

    return (await queryBuilder.getRawMany()).map(s => s.user);
  }

  async update(
    user,
    schedule: Array<{ day: number; from: string; to: string }>,
  ) {
    const rows = schedule.reduce((allRows, item) => {
      const fromTime = this.time.parse(item.from);
      let toTime = this.time.parse(item.to);
      if (toTime <= fromTime) {
        toTime += 1440;
      }

      if (toTime <= 24 * 60) {
        const row = this.repository.create({
          user,
          day: item.day,
          start: fromTime,
          end: toTime,
        });
        allRows.push(row);
      } else {
        let row = this.repository.create({
          user,
          day: item.day,
          start: fromTime,
          end: 1440,
        });
        allRows.push(row);
        row = this.repository.create({
          user,
          day: item.day + 1,
          start: 0,
          end: toTime - 1440,
        });
        allRows.push(row);
      }
      return allRows;
    }, []);
    await this.repository.delete({
      user,
    });
    return this.repository.save(rows);
  }
}
