import { Module } from '@nestjs/common';
import { NannyProfileController } from './nanny-profile.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NannyProfile } from './nanny-profile.entity';
import { NannyUser } from './nanny-user.entity';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { CqrsModule } from '@nestjs/cqrs';
import { ProfileUpdatedHandler } from './events/profile-updated.handler';
import { getClientConfig } from '@utils/transport-connector';
import { LoggerModule } from 'nestjs-pino';

@Module({
  imports: [
    CqrsModule,
    LoggerModule.forRoot({
      pinoHttp: {
        level: process.env.NODE_ENV === 'dev' ? 'debug' : 'info',
        prettyPrint: process.env.NODE_ENV === 'dev',
      },
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      database: process.env.DB_NAME,
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      username: process.env.DB_USER_NAME,
      password: process.env.DB_USER_PASSWORD,
      entities: [NannyProfile, NannyUser],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([NannyProfile, NannyUser]),
    ClientsModule.register([{
      name: 'EVENT_BUS',
      transport: Transport.KAFKA,
      options: {
        client: {
          ...getClientConfig(),
          clientId: 'nanny-profile',
        },
        producer: {
          allowAutoTopicCreation: true,
          idempotent: true,
        },
      },
    }]),
  ],
  controllers: [NannyProfileController],
  providers: [
    ProfileUpdatedHandler,
  ],
})
export class AppModule {
}
