import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';

import { ParentRequestsQuery } from '../impl';

import { ParentRequest } from '../../../entities/parent-request.entity';

@QueryHandler(ParentRequestsQuery)
export class ParentRequestsQueryHandler
  implements IQueryHandler<ParentRequestsQuery> {
  constructor(
    @InjectRepository(ParentRequest)
    private readonly parentRequestRepository: Repository<ParentRequest>,
  ) {
  }

  async execute({params}: ParentRequestsQuery): Promise<ParentRequest[]> {
    const { filter, sort, limit } = params;
    const options: FindManyOptions<ParentRequest> = {
      where: {
        ...filter,
      },
      order: {
        [sort.sortBy || 'id']: sort.sortDirection || 'DESC',
      },
      relations: ['nannyRequests'],
    };
    if (limit) {
      options.take = limit;
    }
    return await this.parentRequestRepository.find(options);
  }
}
