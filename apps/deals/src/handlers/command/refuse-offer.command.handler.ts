import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { RefuseOfferCommand } from '../../commands';
import { DealService } from '../../deal.service';
import { DealExceptionEvent } from '../../events/deal-exception.event';

@CommandHandler(RefuseOfferCommand)
export class RefuseOfferCommandHandler
  implements ICommandHandler<RefuseOfferCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly service: DealService,
  ) {
  }

  async execute(command: RefuseOfferCommand): Promise<any> {
    const deal = this.publisher.mergeObjectContext(command.deal);
    try {
      deal.refuseOffer(command.responder.user, command.responder.type);
    } catch (e) {
      deal.apply(new DealExceptionEvent(deal, 'refuse-offer', e));
      deal.commit();
      throw e;
    }
    await this.service.save(deal);
    deal.commit();
    return deal;
  }
}
