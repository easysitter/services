export interface TraceValue {
  from: Date;
  to: Date;
  price: number;
}

export abstract class CalculatorTracer {
  traced: TraceValue[];

  abstract fork(context): CalculatorTracer;

  abstract trace(value: TraceValue, context?: string);

  abstract flush();
}

// tslint:disable-next-line:max-classes-per-file
export class NoopTracer implements CalculatorTracer {
  traced: TraceValue[] = [];

  // tslint:disable-next-line:no-empty
  flush() {
  }

  fork(context): CalculatorTracer {
    return this;
  }

  // tslint:disable-next-line:no-empty
  trace(value: TraceValue, context?: string) {
  }

}

// tslint:disable-next-line:max-classes-per-file
export class StackTracer implements CalculatorTracer {
  private stack = [];

  constructor(
    private readonly context: string = '',
    private readonly parent?: CalculatorTracer,
  ) {
  }

  fork(context: string) {
    return new StackTracer(context, this);
  }

  get traced() {
    return this.parent ? this.parent.traced : this.stack;
  }

  trace(value: any, context = '') {
    if (this.parent) {
      let newContext;
      if (this.context) {
        newContext = `[${this.context}] ${context}`;
      } else {
        context = context && `[${context}]`;
      }
      this.parent.trace(value, newContext.trim());
    } else {
      this.stack.push({ ...value, context });
    }
  }

  flush() {
    this.stack = [];
  }
}
