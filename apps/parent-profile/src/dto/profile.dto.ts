import { ApiProperty } from '@nestjs/swagger';

export class ProfileDto {
  @ApiProperty()
  name: string;
  @ApiProperty()
  birthDate: string;
  @ApiProperty()
  photo: string;
  @ApiProperty()
  phone: string;
  @ApiProperty()
  description: string;
  @ApiProperty()
  address: string;
  @ApiProperty()
  zipCode: string;
  @ApiProperty({ isArray: true, enum: [0, 1, 2, 3, 4, 5] })
  children: number[];
}
