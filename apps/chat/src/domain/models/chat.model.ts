import { ChatMessage } from './message.model';

export type ChatId = string;

export type ChatMeta = Record<string, number | string | object | boolean>;

export class ChatModel {
  private newMessages: ChatMessage[] = [];

  constructor(
    public readonly id: ChatId,
    public readonly users: string[],
    public readonly meta: ChatMeta,
    public lastMessage: ChatMessage = null,
  ) {
  }

  get messages() {
    return [
      ...this.newMessages,
    ];
  }

  addMessage(message: ChatMessage) {
    if (!this.users.includes(message.user)) {
      throw new Error(`User doesn't belong to this chat`);
    }
    this.newMessages.push(message);
  }
}
