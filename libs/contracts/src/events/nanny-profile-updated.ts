import { EventBusEvent } from '@event-bus/contracts';

interface NannyProfileDto {
  id: number;
  rate: number;
  currency?: string;
  acceptTrial: boolean;
  allowedChildren: number[];
  maxChildrenCount: number;
}

export class NannyProfileUpdated implements EventBusEvent<NannyProfileDto> {
  static readonly eventName = 'profile_updated';

  readonly name = NannyProfileUpdated.eventName;

  constructor(
    public readonly payload: NannyProfileDto,
  ) {
  }
}
