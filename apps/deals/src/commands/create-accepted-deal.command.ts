import { OfferDto } from '../dto';

export class CreateAcceptedDealCommand {
  constructor(public readonly offer: OfferDto) {
  }
}
