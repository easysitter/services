export class ChatMessage {
  constructor(
    public readonly id: string,
    public readonly text: string,
    public readonly user: string,
    public readonly createdAt: Date,
    public readonly viewedBy: string[] = [],
  ) {
  }

  isViewedBy(user) {
    if (this.user === user) {
      return true;
    }
    return this.viewedBy.includes(user);
  }

  addViewedBy(user) {
    if (this.isViewedBy(user)) {
      return;
    }

    this.viewedBy.push(user);
  }

  isBefore(message: ChatMessage) {
    return this.createdAt.getTime() < message.createdAt.getTime();
  }
}
