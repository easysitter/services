import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('session')
export class VideoChatSessionEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  openTokSessionId: string;

  @CreateDateColumn()
  createdAt: Date;
}
