/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { NestFactory } from '@nestjs/core';

import { RequestsModule } from './requests.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { Transport } from '@nestjs/microservices';
import { getClientConfig, patchNest } from '@utils/transport-connector';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  patchNest();
  const app = await NestFactory.create(RequestsModule);
  const logger = app.get(Logger);
  app.useLogger(logger);
  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);
  try {
    app.connectMicroservice({
      transport: Transport.KAFKA,
      options: {
        client: getClientConfig(),
        consumer: {
          groupId: 'request-service',
        },
      },
    });
    await app.startAllMicroservicesAsync();
  } catch (e) {
    logger.error(e.message, null, 'Boot');
    return process.exit(1);
  }
  const options = new DocumentBuilder()
    .setTitle('Request api')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  const port = process.env.HTTP_PORT || 3333;
  SwaggerModule.setup('/docs', app, document);
  await app.listen(port, () => {
    logger.log('Listening at http://localhost:' + port + '/' + globalPrefix);
    logger.log('Listening at http://localhost:' + port + '/docs');
  });

  process.on('SIGTERM', () => {
    logger.log('SIGTERM');
    app.close();
  });
}

bootstrap();
