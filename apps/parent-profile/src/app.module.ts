import { Module } from '@nestjs/common';
import { ParentProfileController } from './parent-profile.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ParentProfile, ParentUser } from './entities';
import { ParentProfileService } from './parent-profile.service';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      database: process.env.DB_NAME,
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      username: process.env.DB_USER_NAME,
      password: process.env.DB_USER_PASSWORD,
      entities: [ParentProfile, ParentUser],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([ParentProfile, ParentUser]),
  ],
  controllers: [
    ParentProfileController,
  ],
  providers: [
    ParentProfileService,
  ],
})
export class AppModule {
}
