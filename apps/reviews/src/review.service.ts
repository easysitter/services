import { InjectRepository } from '@nestjs/typeorm';
import { Review } from './review.entity';
import { Repository } from 'typeorm';
import { DuplicateError } from './duplicate.error';
import { defer, Observable, throwError } from 'rxjs';
import { switchMap } from 'rxjs/operators';

export class ReviewService {
  constructor(
    @InjectRepository(Review)
    private repository: Repository<Review>,
  ) {
  }

  create(data: Partial<Review>): Observable<Review> {
    const { userId, userType, refId, refType } = data;
    return this.findOne({ userId, userType, refId, refType }).pipe(
      switchMap(review => {
        if (review) {
          return throwError(new DuplicateError(review));
        } else {
          review = this.repository.create(data);
          return this.repository.save(review);
        }
      }),
    );
  }

  find(params: Partial<Review>): Observable<Review[]> {
    return defer(() => this.repository.find(params));
  }

  findOne(params: Partial<Review>): Observable<Review> {
    return defer(() => this.repository.findOne(params));
  }
}
