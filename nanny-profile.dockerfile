FROM node:12-alpine AS deps

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm ci
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .
EXPOSE 3000


FROM deps as nanny-profile-build
RUN npm run build nanny-profile
RUN npm prune --production

FROM node:12-alpine AS nanny-profile
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm ci --only=production
COPY --from=nanny-profile-build /usr/src/app/node_modules ./node_modules
COPY --from=nanny-profile-build /usr/src/app/dist/apps/nanny-profile ./dist/apps/nanny-profile
CMD [ "node", "dist/apps/nanny-profile/main.js" ]
