import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { PushService } from './push.service';
import { SavePushTokenDto } from './save-push-token.dto';
import { PushNotificationDto } from './push-notifications.dto';
import { ApiTags } from '@nestjs/swagger';
import { EventPattern } from '@nestjs/microservices';
import {
  ChatMessageSent,
  DealConcluded,
  NannyRequestCreated,
  NewAttention,
  OfferCreated,
  RequestAccepted,
  VideochatParticipantInvited,
  VideochatParticipantTerminated,
} from '@event-bus/contracts';
import { format } from 'date-fns';

@Controller('/push')
@ApiTags('push')
export class PushNotificationsController {
  constructor(private push: PushService) {}

  @Post('/subscribe')
  @HttpCode(HttpStatus.NO_CONTENT)
  async subscribeToPush(@Body() data: SavePushTokenDto) {
    await this.push.subscribe(data.user, data.token);
  }

  @Post('/send')
  @HttpCode(HttpStatus.NO_CONTENT)
  async notify(@Body() { user, ...notification }: PushNotificationDto) {
    await this.push.notify(user, notification);
  }

  @EventPattern(ChatMessageSent.eventName)
  async onChatMessage({ value, ...brokerMessage }) {
    if (brokerMessage.key === 'message') {
      const { chat, message } = value as ChatMessageSent['payload']['value'];

      chat.users.forEach(user => {
        if (user !== message.user) {
          const isRequest = chat.meta && chat.meta.request;
          const title = isRequest
            ? `Запрос №${chat.meta.request}`
            : 'Сообщение';
          const data: any = {
            type: 'chat-message',
            chat: chat.id,
            chat_type: chat.meta.type || 'request',
            message: message.id,
            message_user: message.user,
            chat_users: chat.users.join(':'),
          };
          if (isRequest) {
            data.request = chat.meta.request;
          }
          this.push.notify(user, {
            title,
            body: message.text,
            icon: '/assets/icons/icon.png',
            tag: `chat-${chat.id}-message`,
            renotify: true,
            vibrate: [100, 100],
            data,
          });
        }
      });
    }
  }

  @EventPattern(NannyRequestCreated.eventName)
  onNannyRequestCreated({
    value,
    ...brokerMessage
  }: {
    value: NannyRequestCreated['payload'];
  }) {
    const { nanny, request, parent } = value;
    this.push.notify(`n${nanny}`, {
      title: 'Поступил запрос',
      body: 'Просмотреть предложения',
      icon: '/assets/icons/icon.png',
      tag: `request-${request}-new`,
      data: {
        type: 'new-request',
        nanny,
        request,
        parent,
      },
    });
  }

  @EventPattern(RequestAccepted.eventName)
  onRequestAccepted({
    value,
    ...brokerMessage
  }: {
    value: RequestAccepted['payload'];
  }) {
    const { parent, nanny, request } = value;
    // this.push.notify(`p${parent}`, {
    //   title: 'Ваш запрос принят',
    //   icon: '/assets/icons/icon.png',
    //   tag: `request-${request}-accepted`,
    //   data: {
    //     type: 'request-accepted',
    //     parent, nanny, request,
    //   },
    // });
  }

  @EventPattern(OfferCreated.eventName)
  onOfferCreated({
    value,
    ...brokerMessage
  }: {
    value: OfferCreated['payload'];
  }) {
    const { initiator, parent, nanny, request, deal } = value;
    if (initiator === 'parent') {
      this.push.notify(`n${nanny}`, {
        title: `Новая сделка по запросу #${request}`,
        icon: '/assets/icons/icon.png',
        tag: `deal-${deal}-offer-created`,
        data: {
          type: 'new-offer',
          parent,
          nanny,
          request,
          deal,
        },
      });
    }
    if (initiator === 'nanny') {
      this.push.notify(`p${parent}`, {
        title: `Новая сделка по запросу #${request}`,
        icon: '/assets/icons/icon.png',
        tag: `deal-${deal}-offer-created`,
        data: {
          type: 'new-offer',
          parent,
          nanny,
          request,
          deal,
        },
      });
    }
  }

  @EventPattern(DealConcluded.eventName)
  onDealConcluded({
    value,
    ...brokerMessage
  }: {
    value: DealConcluded['payload'];
  }) {
    const { parent, nanny, request, id, date, fromTime, toTime } = value;
    this.push.notify(`n${nanny}`, {
      title: `Заключена сделка по запросу #${request}`,
      body: `Визит состоится ${format(
        new Date(date),
        'd.MM.yyyy',
      )} ${fromTime}-${toTime}`,
      icon: '/assets/icons/icon.png',
      tag: `deal-${id}-concluded`,
      data: {
        type: 'new-deal',
        parent,
        nanny,
        request,
        deal: id,
      },
    });
    this.push.notify(`p${parent}`, {
      title: `Заключена сделка по запросу #${request}`,
      icon: '/assets/icons/icon.png',
      tag: `deal-${id}-concluded`,
      data: {
        type: 'new-deal',
        parent,
        nanny,
        request,
        deal: id,
      },
    });
  }

  @EventPattern(NewAttention.eventName)
  onAttention({ value, ...brokerMessage }: { value: NewAttention['payload'] }) {
    const { type, refType, refId, data } = value;
    if (type === 'reminder' && refType === 'deal') {
      const { parent, nanny, request, date, fromTime, toTime } = data;
      this.push.notify(`n${nanny}`, {
        title: `Напоминание о визите #${refId}`,
        body: `Визит состоится ${format(
          new Date(date),
          'd.MM.yyyy',
        )} ${fromTime}-${toTime}`,
        icon: '/assets/icons/icon.png',
        tag: `${refType}-${refId}-${type}`,
        data: {
          type: `${refType}-${type}`,
          parent,
          nanny,
          request,
          deal: refId,
        },
      });
    }
    if (type === 'reminder' && refType === 'outdated-request') {
      const { user: parent } = data;
      this.push.notify(`p${parent}`, {
        title: `Ваш запрос #${refId} истекает`,
        body: `Успейте забронировать няню`,
        icon: '/assets/icons/icon.png',
        tag: `${refType}-${refId}-${type}`,
        data: {
          type: `${refType}-${type}`,
          parent,
          request: refId,
        },
      });
    }
  }

  @EventPattern(VideochatParticipantInvited.channelName)
  onVideochatParticipant({
    key,
    value,
  }:
    | { key: string; value: VideochatParticipantInvited['payload'] }
    | { key: 'terminated'; value: VideochatParticipantTerminated['payload'] }) {
    if (key === 'terminated') {
      this.onVideochatParticipantTerminated(value);
    } else {
      this.onVideochatParticipantInvited(value);
    }
  }

  private onVideochatParticipantTerminated(
    value: VideochatParticipantTerminated['payload'],
  ) {
    const prefix = value.role === 'nanny' ? 'n' : 'p';
    this.push.notify(`${prefix}${value.userId}`, {
      title: `Звонок завершен`,
      icon: '/assets/icons/icon.png',
      tag: `videochat_${value.sessionId}`,
      data: {
        type: 'videochat_terminated',
        sessionId: value.sessionId,
      },
      vibrate: [200, 100, 200],
      renotify: true,
    });
  }

  private onVideochatParticipantInvited(
    value: VideochatParticipantInvited['payload'],
  ) {
    if (value.role !== 'nanny') {
      return;
    }
    this.push.notify(`n${value.userId}`, {
      title: `Запрос видеособеседования`,
      icon: '/assets/icons/icon.png',
      tag: `videochat_${value.sessionId}`,
      data: {
        type: 'videochat',
        sessionId: value.sessionId,
      },
      vibrate: [200, 100, 200],
      renotify: true,
    });
  }
}
