import { CalculatorParams } from './calculator/calculator-params';
import { Calculator } from './calculator/calculator';
import { CalculatorTracer } from './utils/calculator-tracer';

export class PriceCalculator {

  constructor(
    private calculator: Calculator,
    private overtime: Calculator,
    private calculatorTracer: CalculatorTracer,
  ) {
  }

  async getDealPrice(params: CalculatorParams) {
    const result = this.calculator.getPrice(params, this.calculatorTracer);
    console.log(this.calculatorTracer.traced);
    return result;
  }

  async getOvertimePrice(params: CalculatorParams) {
    return this.overtime.getPrice(params, this.calculatorTracer);
  }
}
