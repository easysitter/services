import { DealCommand } from './deal.command';
import { Deal } from '../models';
import { OfferResponderDto } from '../dto/offer-responder.dto';

export class AcceptOfferCommand extends DealCommand {
  constructor(deal: Deal, public readonly responder: OfferResponderDto) {
    super(deal);
  }
}
