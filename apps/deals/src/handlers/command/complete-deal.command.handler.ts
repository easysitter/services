import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { CompleteDealCommand } from '../../commands';
import { DealService } from '../../deal.service';
import { DealExceptionEvent } from '../../events/deal-exception.event';

@CommandHandler(CompleteDealCommand)
export class CompleteDealCommandHandler
  implements ICommandHandler<CompleteDealCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly service: DealService,
  ) {
  }

  async execute(command: CompleteDealCommand): Promise<any> {
    const deal = this.publisher.mergeObjectContext(command.deal);
    try {
      deal.complete();
    } catch (e) {
      deal.apply(new DealExceptionEvent(deal, 'complete-deal', e));
      deal.commit();
      throw e;
    }
    await this.service.save(deal);
    deal.commit();
    return deal;
  }
}
