import { EventBusEvent } from '../event-bus-event';

interface NannyRequestCreatedPayload {
  request: number;
  parent: number;
  nanny: number;
}

export class NannyRequestCreated implements EventBusEvent<NannyRequestCreatedPayload> {
  static readonly eventName = 'nanny_request_created';

  public readonly name = NannyRequestCreated.eventName;

  constructor(
    public readonly payload: NannyRequestCreatedPayload,
  ) {
  }
}
