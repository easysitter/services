import { FindConditions, In, MoreThanOrEqual, Raw, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';

import { Nanny } from '../entities/nanny.entity';

function ContainsAll(param: any[]) {
  return Raw(alias => `${alias} @> ALL( array[${param.map(_ => `'${_}'`).join(', ')}]::jsonb[])`);
}

@Injectable()
export class NannyService {
  constructor(
    @InjectRepository(Nanny)
    private repo: Repository<Nanny>,
  ) {
  }

  create(data: Partial<Nanny>): Nanny {
    return this.repo.create(data);
  }

  update(entity: Nanny) {
    return this.repo.save(entity);
  }

  async find(param: { users: number[], children?: number[], trial?: boolean }) {
    const isUsersEmpty = !param.users || !param.users.length;
    if (isUsersEmpty) {
      return [];
    }
    const where = this.createFindCondition(param);
    return this.repo.find({
      where,
    });
  }

  private createFindCondition(param: { users: number[]; children?: number[]; trial?: boolean }) {
    const where: FindConditions<Nanny> = {
      id: In(param.users),
    };
    if (param.children) {
      where.maxChildrenCount = MoreThanOrEqual(param.children.length);
      where.allowedChildren = ContainsAll(param.children);
    }
    if (param.trial === true) {
      where.acceptTrial = true;
    }
    return where;
  }
}
