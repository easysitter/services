import { Module } from '@nestjs/common';
import { TimeParser } from './time-parser';

@Module({
  providers: [TimeParser],
  exports: [TimeParser],
})
export class TimeParserModule {
}
