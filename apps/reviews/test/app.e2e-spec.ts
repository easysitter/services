import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { ReviewsModule } from '../src/reviews.module';
import { CreateReviewDto } from '../src/create-review.dto';
import { SearchReviewDto } from '../src/search-review.dto';
import { ReviewService } from '../src/review.service';
import { Review } from '../src/review.entity';
import { INestApplication } from '@nestjs/common';
import { of } from 'rxjs';
import SpyInstance = jest.SpyInstance;

describe('ReviewController (e2e)', () => {
  let app: INestApplication;
  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test
      .createTestingModule({
        imports: [ReviewsModule],
      })
      .compile();
    app = moduleFixture.createNestApplication();
    app.setGlobalPrefix('/api');
    await app.init();
  });
  afterAll(async () => {
    await app.close();
  });

  describe('/api/reviews (POST)', () => {
    let newReviewId;
    let spyReviewServiceCreate: SpyInstance;
    const createReviewDto: CreateReviewDto = {
      userId: '1',
      userType: 'nanny',
      refId: '1',
      refType: 'deal',
      comment: '',
      rating: 3,
    };
    beforeEach(async () => {
      const reviewService: ReviewService = app.get(ReviewService);
      newReviewId = 'new';
      spyReviewServiceCreate = jest
        .spyOn(reviewService, 'create')
        .mockImplementation((data: Partial<Review>) => {
          return of({
            ...data,
            id: newReviewId,
          } as Review);
        });
    });
    afterEach(() => {
      spyReviewServiceCreate.mockRestore();
      newReviewId = null;
    });
    it('should create new review', () => {
      return request(app.getHttpServer())
        .post('/api/reviews')
        .send(createReviewDto)
        .expect(201)
        .expect({ ...createReviewDto, id: newReviewId });
    });
    describe('duplicate', () => {
      let existedReviewId;
      beforeEach(() => {
        existedReviewId = 'existed';
        const reviewService: ReviewService = app.get(ReviewService);
        spyReviewServiceCreate.mockRestore();
        jest.spyOn(reviewService, 'findOne')
          .mockImplementation(args => {
            return of({
              ...createReviewDto,
              id: existedReviewId,
            } as Review);
          });
      });
      it('should return existing review', () => {
        return request(app.getHttpServer())
          .post('/api/reviews')
          .send(createReviewDto)
          .expect(201)
          .expect({ ...createReviewDto, id: existedReviewId });
      });
    });
  });

  describe('Search', () => {
    let spy: SpyInstance;
    const ownedReview: Review = {
      id: 'owned',
      refType: 'deal',
      refId: '1',
      userType: 'nanny',
      userId: '1',
      comment: '',
      rating: 4,
      createdAt: Date.now(),
    };
    const notOwnedReview: Review = {
      id: 'notOwned',
      refType: 'deal',
      refId: '1',
      userType: 'parent',
      userId: '1',
      comment: '',
      rating: 4,
      createdAt: Date.now(),
    };
    beforeEach(() => {
      const reviewService: ReviewService = app.get(ReviewService);
      spy = jest
        .spyOn(reviewService, 'find')
        .mockImplementation((data: Partial<Review>) => {
          const keys = Object.keys(data);
          const found = [ownedReview, notOwnedReview]
            .filter(review => keys.every(key => review[key] === data[key]));
          return of(found);
        });
    });
    afterEach(() => {
      spy.mockRestore();
    });

    it('/api/reviews (GET)', () => {
      return request(app.getHttpServer())
        .get('/api/reviews')
        .query({
          userId: '1',
          userType: 'nanny',
        } as  SearchReviewDto )
        .expect(200)
        .then(({ body }) => {
          expect(body).toEqual([ownedReview]);
        });
    });
  });
});
