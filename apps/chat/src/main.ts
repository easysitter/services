/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { NestFactory } from '@nestjs/core';

import { ChatApplication } from './application/chat.application';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { Transport } from '@nestjs/microservices';
import { getClientConfig, patchNest } from '@utils/transport-connector';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  patchNest();
  const app = await NestFactory.create(ChatApplication);
  const logger = app.get(Logger);
  app.useLogger(logger);
  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);
  try {
    app.connectMicroservice({
      transport: Transport.KAFKA,
      options: {
        client: getClientConfig(),
        consumer: {
          groupId: 'chat-service',
        },
      },
    });
    await app.startAllMicroservicesAsync();
  } catch (e) {
    logger.error(e.message, null, 'Boot');
    return process.exit(1);
  }
  const port = process.env.HTTP_PORT || 3333;
  const options = new DocumentBuilder()
    .setTitle('Chat api')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('/docs', app, document);

  await app.listen(port, async () => {
    logger.log('Listening at http://localhost:' + port + '/' + globalPrefix, 'Boot');
    logger.log('Listening at http://localhost:' + port + '/docs', 'Boot');
  });
  process.on('SIGTERM', () => {
    logger.log('SIGTERM');
    app.close();
  });
}

process.on('uncaughtException', () => process.exit(-1));
bootstrap();
