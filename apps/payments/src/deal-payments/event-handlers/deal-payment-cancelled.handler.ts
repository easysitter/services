import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { PaymentCancelledEvent } from '../../events/payment-cancelled.event';
import { ClientKafka } from '@nestjs/microservices';
import { Inject } from '@nestjs/common';
import { DealPaymentCancelled } from '@event-bus/contracts';
import { DEAL_PAYMENT_TYPE } from '../constants';

@EventsHandler(PaymentCancelledEvent)
export class DealPaymentCancelledHandler implements IEventHandler<PaymentCancelledEvent> {
  constructor(
    @Inject('EVENT_BUS')
    private eventBus: ClientKafka,
  ) {
  }

  handle({ payment }: PaymentCancelledEvent): any {
    if (payment.type !== DEAL_PAYMENT_TYPE) {
      return;
    }
    const event = new DealPaymentCancelled({
      id: payment.id,
      amount: payment.amount,
      currency: payment.currency,
      deal: payment.meta.deal,
      user: payment.meta.user,
      cancelledAt: payment.cancelledAt,
    });
    const message = {
      key: event.name,
      value: event.payload,
    };
    this.eventBus.emit(event.channel, message);
  }
}
