import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Transaction, TransactionRepository } from 'typeorm';

import { CancelRequestCommand } from '../impl';
import { NannyRequest } from '../../../entities/nanny-request.entity';
import { ParentRequest } from '../../../entities/parent-request.entity';

@CommandHandler(CancelRequestCommand)
export class CancelRequestCommandHandler
  implements ICommandHandler<CancelRequestCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    @InjectRepository(NannyRequest)
    private readonly nannyRequest: Repository<NannyRequest>,
  ) {}

  async execute(command: CancelRequestCommand): Promise<void> {
    const parentRequest = this.publisher.mergeObjectContext(command.request);
    const nannyRequests = await this.nannyRequest.find({
      where: {
        parentRequest,
        isActive: true,
      },
    });

    nannyRequests.forEach(req => req.parentRequest = parentRequest);
    return this._execute(
      parentRequest,
      nannyRequests.map(r => this.publisher.mergeObjectContext(r)),
    );
  }

  @Transaction({
    isolation: 'READ COMMITTED',
  })
  private async _execute(
    parentRequest: ParentRequest,
    nannyRequests: NannyRequest[],
    @TransactionRepository(ParentRequest)
    parentRequestRepository?: Repository<ParentRequest>,
    @TransactionRepository(NannyRequest)
    nannyRequestRepository?: Repository<NannyRequest>,
  ) {
    parentRequest.cancel();
    await parentRequestRepository.save(parentRequest);

    nannyRequests.forEach(r => r.cancel());
    await nannyRequestRepository.save(nannyRequests);
    parentRequest.commit();
    nannyRequests.forEach(r => r.commit());
  }
}
