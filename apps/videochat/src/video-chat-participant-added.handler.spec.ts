import { VideoChatParticipantAddedHandler } from './video-chat-participant-added.handler';
import { Test } from '@nestjs/testing';
import { VideoChatParticipantEntity } from './video-chat-participant.entity';
import { VideochatParticipantInvited } from '@event-bus/contracts';
import { AggregateRoot, CqrsModule, EventPublisher } from '@nestjs/cqrs';
import { VideoChatParticipantAdded } from './video-chat-participant-added';

const mockClientProxy = {
  emit: jest.fn(),
};
describe('VideoChatParticipantAddedHandler', () => {
  let handler: VideoChatParticipantAddedHandler;
  let eventPublisher: EventPublisher;
  beforeEach(async () => {
    const testingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        VideoChatParticipantAddedHandler,
        { provide: 'EVENT_BUS', useValue: mockClientProxy },
      ],
    }).compile();
    await testingModule.init();
    handler = testingModule.get(VideoChatParticipantAddedHandler);
    eventPublisher = testingModule.get(EventPublisher);
  });

  it('should send event to event bus', async () => {
    const FAKE_PARTICIPANT = {
      role: 'parent',
      userId: '1',
      sessionId: '1',
    } as VideoChatParticipantEntity;
    await handler.handle({
      participant: FAKE_PARTICIPANT,
    });

    expect(mockClientProxy.emit).toHaveBeenCalledWith(
      VideochatParticipantInvited.channelName,
      expect.objectContaining({
        key: 'parent_1',
        value: FAKE_PARTICIPANT,
      }),
    );
  });

  it('should handle VideoChatParticipantAdded', async () => {
    const EventProducer = class extends AggregateRoot {};
    const FAKE_PARTICIPANT = eventPublisher.mergeObjectContext(
      new EventProducer(),
    );
    const spyHandle = jest.spyOn(handler, 'handle').mockResolvedValue();
    const event = new VideoChatParticipantAdded(null);
    FAKE_PARTICIPANT.apply(event);
    FAKE_PARTICIPANT.commit();
    expect(spyHandle).toHaveBeenCalled();
  });
});
