import { FindConditions, Not, Repository, Transaction, TransactionRepository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Deal } from './models';
import { SearchDto } from './dto';

export class DealService {
  constructor(
    @InjectRepository(Deal)
    private readonly r: Repository<Deal>,
  ) {
  }

  async findById(id: Deal['id']) {
    return this.r.findOne(id);
  }

  async find(params: SearchDto) {
    const conditions: FindConditions<Deal> = {};

    if (params.state) {
      conditions.state = params.state;
    }
    if (params.request) {
      conditions.requestId = params.request;
    }
    if (params.nanny) {
      conditions.nanny = params.nanny;
    }
    if (params.parent) {
      conditions.parent = params.parent;
    }
    if (params.isActive === true || params.isActive === false) {
      conditions.isActive = params.isActive;
    }
    return this.r.createQueryBuilder('deal')
      .select('deal')
      .addSelect(`CASE
                            WHEN (deal.tag IS NULL)             THEN  1
                            WHEN (deal.tag = 'in progress')     THEN  2
                            WHEN (deal.tag = 'pending_review')  THEN  3
                            WHEN (deal.tag = 'cancelled')       THEN  4
                            WHEN (deal.tag = 'suspended')       THEN  5
                            ELSE                                      6
                          END`, 'status_order')
      .where(conditions)
      .addOrderBy('status_order', 'ASC')
      .addOrderBy('date', 'DESC')
      .getMany();
  }

  async findRelatedOffers(deal: Deal) {
    return this.r.find({
      where: {
        id: Not(deal.id),
        requestId: deal.requestId,
        isActive: true,
        state: 'offer',
      },
    });
  }

  async save(deal: Deal) {
    return this.r.save(deal);
  }

  @Transaction()
  async saveAll(
    deals: Deal[],
    @TransactionRepository(Deal) r?: Repository<Deal>,
  ) {
    return r.save(deals);
  }
}
