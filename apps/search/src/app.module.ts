import { Module } from '@nestjs/common';
import { SearchController } from './search.controller';
import { SearchService } from './services/search.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Schedule } from './entities/schedule.entity';
import { Reserve } from './entities/reserve.entity';
import { Nanny } from './entities/nanny.entity';
import { SearchResult } from './entities/search-result.entity';
import { ScheduleService } from './services/schedule.service';
import { ReservationsService } from './services/reservations.service';
import { TimeService } from './services/time.service';
import { NannyService } from './services/nanny.service';
import { DealController } from './deal.controller';
import { ScheduleController } from './schedule.controller';
import { LoggerModule } from 'nestjs-pino';

@Module({
  imports: [
    LoggerModule.forRoot({
      pinoHttp: {
        level: process.env.NODE_ENV === 'dev' ? 'debug' : 'info',
        prettyPrint: process.env.NODE_ENV === 'dev',
      },
    }),
    TypeOrmModule.forRoot({
      synchronize: true,
      entities: [Schedule, Reserve, Nanny, SearchResult],
      type: 'postgres',
      database: process.env.DB_NAME,
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      username: process.env.DB_USER_NAME,
      password: process.env.DB_USER_PASSWORD,
    }),
    TypeOrmModule.forFeature([Schedule, Reserve, Nanny, SearchResult]),
  ],
  controllers: [SearchController, DealController, ScheduleController],
  providers: [SearchService, ScheduleService, ReservationsService, TimeService, NannyService],
})
export class AppModule {}
