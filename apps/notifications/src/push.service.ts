import { Inject, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as admin from 'firebase-admin';

import { PushToken } from './push-token.entity';
import { MESSAGING } from './tokens';
import { PushNotification } from './push-notification.entity';

@Injectable()
export class PushService {
  constructor(
    @InjectRepository(PushToken)
    private pushTokenRepository: Repository<PushToken>,
    @InjectRepository(PushNotification)
    private pushNotificationRepository: Repository<PushNotification>,
    @Inject(MESSAGING)
    private messaging: admin.messaging.Messaging,
  ) {
  }

  async subscribe(user, token) {
    try {
      let pushToken = await this.pushTokenRepository.findOne({
        where: {
          token,
        },
      });
      if (!pushToken) {
        pushToken = this.pushTokenRepository.create({
          user,
          token,
        });
      } else {
        pushToken.user = user;
      }
      await this.pushTokenRepository.save(pushToken);
      return true;
    } catch (e) {
      Logger.error(`Save token: ${e.message}`, e.stackTrace, 'PUSH SERVICE');
      return true;
    }
  }

  async notify(user: string, notification) {
    const userTokens = await this.getUserTokens(user);
    const message = {
      notification: {
        title: notification.title,
        body: notification.body,
      },
      data: Object.keys(notification.data).reduce((data, key) => {
        data[key] = notification.data[key].toString();
        return data;
      }, {}),
      android: {
        ttl: 3600 * 1000,
        notification: {
          sound: 'default',
        },
      },
      apns: {
        headers: {
          'apns-priority': '5',
        },
        payload: {
          aps: {
            alert: {
              title: notification.title,
              body: notification.body,
            },
            data: notification.data,
          },
        },
      },
      webpush: {
        notification,
      },
    };
    const pushNotification = this.pushNotificationRepository.create({
      user, notification: message as any, isSent: false, messageIds: [],
    });
    await this.pushNotificationRepository.save(pushNotification);
    for (const push of userTokens) {
      try {
        const messageId = await this.messaging.send({ ...message, token: push.token });
        pushNotification.messageIds.push(messageId);
      } catch (e) {
        console.error(e);
        await this.pushTokenRepository.remove(push);
      }
    }
    pushNotification.isSent = true;
    await this.pushNotificationRepository.save(pushNotification);
  }

  private async getUserTokens(user): Promise<PushToken[]> {
    return await this.pushTokenRepository.find({
      user,
    });
  }
}
