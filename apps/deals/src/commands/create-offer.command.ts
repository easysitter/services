import { ICommand } from '@nestjs/cqrs';
import { OfferDto } from '../dto';

export class CreateOfferCommand implements ICommand {
  constructor(public readonly offer: OfferDto) {}
}
