import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { DealByIdQuery } from '../../queries';
import { Deal } from '../../models';
import { DealService } from '../../deal.service';

@QueryHandler(DealByIdQuery)
export class DealByIdQueryHandler
  implements IQueryHandler<DealByIdQuery, Deal> {
  constructor(private readonly service: DealService) {}

  execute(query: DealByIdQuery): Promise<Deal> {
    return this.service.findById(query.id);
  }
}
