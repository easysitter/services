import { NannyProfile } from '../nanny-profile.entity';

export class ProfileUpdatedEvent {
  constructor(
    public readonly profile: NannyProfile,
  ) {
  }
}
