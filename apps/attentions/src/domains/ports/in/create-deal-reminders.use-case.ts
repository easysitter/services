import { CreateDealRemindersCommand } from './create-deal-reminders.command';
import { AttentionGroupEntity } from '../..';

export abstract class CreateDealRemindersUseCase {
  abstract createReminders(command: CreateDealRemindersCommand): Promise<AttentionGroupEntity>;
}
