import { CheckoutAPI, Client, Config } from '@adyen/api-library';
import { v4 as uuid } from 'uuid';
import { addDays, formatISO, parseISO } from 'date-fns';
import { Test } from '@nestjs/testing';
import { HttpModule, HttpService } from '@nestjs/common';

const {ADYEN_MERCHANT_ACCOUNT, ADYEN_API_KEY, ADYEN_USERNAME, ADYEN_PASSWORD} = process.env;

const MERCHANT_ACCOUNT = ADYEN_MERCHANT_ACCOUNT;
const API_KEY = ADYEN_API_KEY;
const AUTH = {
  username: ADYEN_USERNAME,
  password: ADYEN_PASSWORD,
};

describe('Adyen payments', () => {
  let client: Client;

  beforeAll(() => {
    client = new Client({
      config: new Config({
        apiKey: API_KEY,
        environment: 'TEST',
      }),
    });
  });

  describe('Checkout api', () => {
    let checkoutAPI: CheckoutAPI;
    let reference;
    beforeEach(() => {
      checkoutAPI = new CheckoutAPI(client);
      reference = uuid();
    });
    it('should create payment link', async () => {
      const amount = {
        value: 100,
        currency: 'PLN',
      };
      const expiresAt = formatISO(addDays(new Date(), 10));
      const result = await checkoutAPI.paymentLinks({
        amount,
        countryCode: 'PL',
        reference,
        merchantAccount: MERCHANT_ACCOUNT,
        expiresAt,
      });
      expect(result).toHaveProperty('url');

      expect(result.reference).toEqual(reference);
      expect(result.amount).toEqual(amount);
      expect(formatISO(parseISO(result.expiresAt))).toEqual(expiresAt);
    });
  });

  describe('MarketPay', () => {
    let module;
    let http: HttpService;
    beforeAll(async () => {
      module = await Test.createTestingModule({
        imports: [
          HttpModule,
        ],
      }).compile();

      http = module.get(HttpService);
    });
    describe('Registration', () => {
      it('should create account holder and account', async () => {
        const accountHolderCode = uuid();
        const createAccountEndpoint = `/Account/v5/createAccountHolder`;
        try {
          const result = await http.post(createAccountEndpoint, {
            accountHolderCode,
            accountHolderDetails: {
              email: 'test@example.com',
              individualDetails: {
                name: {
                  firstName: 'Testovy',
                  gender: 'UNKNOWN',
                  lastName: 'Test',
                },
              },
              address: {
                country: 'PL',
              },
            },
            createDefaultAccount: true,
            legalEntity: 'Individual',
          }, {
            baseURL: client.config.marketPayEndpoint,
            auth: AUTH,
          }).toPromise();
          console.log(result);
        } catch (e) {
          if (e.isAxiosError) {
            const { stack, config, ...err } = e.toJSON();
            console.error(err);
          }
          throw e;
        }
      });
    });
  });
});
