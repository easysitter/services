import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { CancelDealCommand } from '../../commands';
import { DealService } from '../../deal.service';
import { DealExceptionEvent } from '../../events/deal-exception.event';

@CommandHandler(CancelDealCommand)
export class CancelDealCommandHandler
  implements ICommandHandler<CancelDealCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly service: DealService,
  ) {
  }

  async execute(command: CancelDealCommand): Promise<any> {
    const deal = this.publisher.mergeObjectContext(command.deal);
    try {
      deal.cancel(command.reason);
    } catch (e) {
      deal.apply(new DealExceptionEvent(deal, 'cancel-deal', e));
      deal.commit();
      throw e;
    }
    await this.service.save(deal);
    deal.commit();
    return deal;
  }
}
