import { AttentionEntity, AttentionGroupEntity, NotifyNewAttentionsPort } from '../domains';
import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { EVENT_BUS } from '../injection.tokens';
import { NewAttention } from '@event-bus/contracts';

@Injectable()
export class PublishAttentionsService implements NotifyNewAttentionsPort {
  constructor(
    @Inject(EVENT_BUS)
    private eventBus: ClientProxy,
  ) {
  }

  async notify(attentionGroup: AttentionGroupEntity): Promise<void> {
    attentionGroup.attentions.map(attention => this.notifyOne(attention));
    return;
  }

  private async notifyOne(attention: AttentionEntity) {
    const msg = new NewAttention({
      type: attention.type,
      refId: attention.target.refId,
      refType: attention.target.refType,
      data: attention.target.data,
    });
    this.eventBus.emit(msg.name, {
      key: attention.type,
      value: msg.payload,
    });
    return;
  }
}
