import { DealEvent } from './deal.event';

export class DealCompletedEvent extends DealEvent {}
