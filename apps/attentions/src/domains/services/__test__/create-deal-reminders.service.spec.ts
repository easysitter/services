import { AttentionTargetEntity } from '../../entities/attention-target.entity';
import { AttentionEntity } from '../../entities/attention.entity';
import { AttentionGroupEntity } from '../../entities/attention-group.entity';

import { CreateDealRemindersCommand } from '../../ports/in/create-deal-reminders.command';
import { CreateDealRemindersService } from '../create-deal-reminders.service';

import { SaveAttentionsPort } from '../../ports/out/save-attentions.port';
import { LoadDealsToRemindPort } from '../../ports/out/load-deals-to-remind.port';
import { LoadAttentionsPort } from '../../ports/out/load-attentions.port';
import { NotifyNewAttentionsPort } from '../../ports/out/notify-new-attentions.port';

describe('CreateDealRemindersService', () => {
  const dealsTargets = [
    AttentionTargetEntity.of('deal', '1', { deal: 1, nanny: 1 }),
    AttentionTargetEntity.of('deal', '2', { deal: 2, nanny: 1 }),
    AttentionTargetEntity.of('deal', '3', { deal: 3, nanny: 1 }),
  ];
  const attentionType = 'reminder';
  let command: CreateDealRemindersCommand;
  let saveAttentionsFnMock: SaveAttentionsPort['save'];
  let saveAttentionPort: SaveAttentionsPort;
  let saveAttentionsResult: AttentionGroupEntity;
  let loadDealsToRemindFnMock: LoadDealsToRemindPort['load'];
  let loadDealsToRemindPort: LoadDealsToRemindPort;
  let loadAttentionsFnMock: LoadAttentionsPort['loadAttentions'];
  let loadAttentionsPort: LoadAttentionsPort;
  let notifyNewAttentionsFnMock: NotifyNewAttentionsPort['notify'];
  let notifyNewAttentionsPort: NotifyNewAttentionsPort;
  beforeEach(() => {
    saveAttentionsResult = new AttentionGroupEntity();
    saveAttentionsFnMock = jest.fn(async () => saveAttentionsResult);
    saveAttentionPort = jest.fn(() => {
      return { save: saveAttentionsFnMock };
    })();

    loadDealsToRemindFnMock = jest.fn(async () => dealsTargets);
    loadDealsToRemindPort = jest.fn(() => {
      return { load: loadDealsToRemindFnMock };
    })();

    loadAttentionsFnMock = jest.fn(async () => []);
    loadAttentionsPort = jest.fn(() => {
      return { loadAttentions: loadAttentionsFnMock };
    })();

    notifyNewAttentionsFnMock = jest.fn(async () => {
      return;
    });
    notifyNewAttentionsPort = jest.fn(() => {
      return { notify: notifyNewAttentionsFnMock };
    })();

    command = new CreateDealRemindersCommand();
  });

  it('should create reminders', async () => {
    const createDealRemindersService = new CreateDealRemindersService(
      loadDealsToRemindPort,
      saveAttentionPort,
      loadAttentionsPort,
      notifyNewAttentionsPort,
    );

    await createDealRemindersService.createReminders(command);

    const expectedAttentions = dealsTargets.map(dealTarget => AttentionEntity.of(attentionType, dealTarget))
      .reduce((group, attention) => group.addAttention(attention), new AttentionGroupEntity());
    expect(loadDealsToRemindFnMock).toBeCalled();
    expect(saveAttentionsFnMock).toBeCalledWith(expectedAttentions);
    expect(notifyNewAttentionsFnMock).toBeCalledWith(saveAttentionsResult);
  });

  describe('Existed Attentions', () => {
    beforeEach(() => {
      loadAttentionsFnMock = jest.fn(async ({ targetId }: any) => {
        if (targetId === '1') {
          return [
            AttentionEntity.of(attentionType, dealsTargets[0]),
          ];
        }
        return [];
      });
      loadAttentionsPort = jest.fn(() => {
        return { loadAttentions: loadAttentionsFnMock };
      })();
    });
    it('should not create duplicates', async () => {
      const createDealRemindersService = new CreateDealRemindersService(
        loadDealsToRemindPort,
        saveAttentionPort,
        loadAttentionsPort,
        notifyNewAttentionsPort,
      );

      await createDealRemindersService.createReminders(command);
      const expectedAttentions = dealsTargets
        .filter(deal => deal.refId !== '1')
        .map(dealTarget => AttentionEntity.of(attentionType, dealTarget))
        .reduce((group, attention) => group.addAttention(attention), new AttentionGroupEntity());
      expect(loadDealsToRemindFnMock).toBeCalled();
      expect(saveAttentionsFnMock).toBeCalledWith(expectedAttentions);
      expect(notifyNewAttentionsFnMock).toBeCalledWith(saveAttentionsResult);
    });
  });
});
