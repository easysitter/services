import { IEvent } from '@nestjs/cqrs';
import { ParentRequest } from '../../../entities/parent-request.entity';

export class RequestOutdatedEvent implements IEvent {
  constructor(public readonly request: ParentRequest) {
  }
}
