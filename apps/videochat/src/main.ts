import { NestFactory } from '@nestjs/core';
import { VideoChatApplication } from './video-chat.application';
import { Logger } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(VideoChatApplication);
  const logger = app.get(Logger);
  app.useLogger(logger);
  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);

  const options = new DocumentBuilder()
    .setTitle('Video chat api')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('/docs', app, document);

  const port = process.env.HTTP_PORT || 3333;
  await app.listen(port, async () => {
    logger.log('Listening at http://localhost:' + port + '/' + globalPrefix, 'Boot');
    logger.log('Listening at http://localhost:' + port + '/docs', 'Boot');
  });

  process.on('SIGTERM', async () => {
    logger.log('SIGTERM');
    await app.close();
    process.exit();
  });

}

bootstrap();
