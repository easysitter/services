import { IEvent } from '@nestjs/cqrs';
import { VideoChatParticipantEntity } from './video-chat-participant.entity';

export class VideoChatTerminated implements IEvent {
  constructor(public readonly participant: VideoChatParticipantEntity) {}
}
