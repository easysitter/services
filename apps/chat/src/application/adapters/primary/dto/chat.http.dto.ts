import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';

export class ChatHttpDto {
  @ApiProperty({required: false})
  @Expose()
  id: string;
  @ApiProperty()
  @Expose()
  users: string[];
  @ApiProperty()
  @Expose()
  meta: Record<string, string|number|boolean>;
}
