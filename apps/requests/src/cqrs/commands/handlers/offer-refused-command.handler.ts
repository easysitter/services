import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { OfferRefusedCommand } from '../impl';

import { NannyRequest } from '../../../entities/nanny-request.entity';

@CommandHandler(OfferRefusedCommand)
export class OfferRefusedCommandHandler
  implements ICommandHandler<OfferRefusedCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    @InjectRepository(NannyRequest)
    private readonly nannyRequestRepo: Repository<NannyRequest>,
  ) {}

  async execute(command: OfferRefusedCommand): Promise<void> {
    const request = this.publisher.mergeObjectContext(command.request);

    request.offerRefused();
    await this.nannyRequestRepo.save(request);

    request.commit();
  }
}
