import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Payment } from '../entities/payment';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class PaymentService {
  constructor(
    @InjectRepository(Payment)
    private readonly paymentRepository: Repository<Payment>,
  ) {
  }

  async getPaymentById(paymentId: string) {
    return await this.paymentRepository.findOne(paymentId);
  }
}
