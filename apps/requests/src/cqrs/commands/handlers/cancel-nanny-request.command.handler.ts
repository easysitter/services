import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { NannyRequest } from '../../../entities/nanny-request.entity';
import { CancelNannyRequestCommand } from '../impl';

@CommandHandler(CancelNannyRequestCommand)
export class CancelNannyRequestCommandHandler
  implements ICommandHandler<CancelNannyRequestCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    @InjectRepository(NannyRequest)
    private readonly nannyRequest: Repository<NannyRequest>,
  ) {}

  async execute(command: CancelNannyRequestCommand): Promise<void> {
    const request = this.publisher.mergeObjectContext(command.request);
    request.cancel();
    await this.nannyRequest.save(request);
    request.commit();
  }
}
