import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { AcceptRequestCommand } from '../impl';
import { NannyRequest } from '../../../entities/nanny-request.entity';

@CommandHandler(AcceptRequestCommand)
export class AcceptRequestCommandHandler
  implements ICommandHandler<AcceptRequestCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    @InjectRepository(NannyRequest)
    private readonly nannyRequest: Repository<NannyRequest>,
  ) {}

  async execute(command: AcceptRequestCommand): Promise<void> {
    const request = this.publisher.mergeObjectContext(command.request);
    request.accept();
    await this.nannyRequest.save(request);
    request.commit();
  }
}
