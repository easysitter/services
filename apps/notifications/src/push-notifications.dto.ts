import { ApiProperty } from '@nestjs/swagger';

export class PushNotificationDto {
  @ApiProperty()
  user: string;
  @ApiProperty()
  title: string;
  @ApiProperty()
  body: string;
}
