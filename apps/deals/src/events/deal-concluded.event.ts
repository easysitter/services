import { DealEvent } from './deal.event';

export class DealConcludedEvent extends DealEvent {}
