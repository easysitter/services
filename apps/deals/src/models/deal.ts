import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { AggregateRoot } from '@nestjs/cqrs';
import { OfferDto, Role } from '../dto';
import { plainToClassFromExist } from 'class-transformer';
import { DealCancelledEvent, DealCompletedEvent, DealConcludedEvent, OfferCancelledEvent, OfferCreatedEvent } from '../events';
import { ApiProperty } from '@nestjs/swagger';
import { VisitStartedEvent } from '../events/visit-started.event';
import { VisitFinishedEvent } from '../events/visit-finished.event';
import assert = require('assert');
import { SameStatusChangeEvent } from '../events/same-status-change.event';

@Entity()
export class Deal extends AggregateRoot {
  @PrimaryGeneratedColumn('increment')
  @ApiProperty()
  id: number;

  @Column()
  @ApiProperty()
  requestId: string;
  @Column()
  @ApiProperty()
  trackingId: string;

  @Column()
  @ApiProperty()
  parent: number;

  @Column()
  @ApiProperty()
  nanny: number;

  @Column({
    type: 'enum',
    enum: ['parent', 'nanny'],
  })
  @ApiProperty()
  initiator: Role;

  @Column({
    type: 'date',
  })
  @ApiProperty({ type: 'string', format: 'date' })
  date: Date;

  @Column()
  @ApiProperty()
  fromTime: string;

  @Column()
  @ApiProperty()
  toTime: string;

  @Column({
    transformer: {
      from(value: any): any {
        return Number(value) / 100;
      },
      to(value: number): any {
        return Math.floor(Number(value) * 100);
      },
    },
  })
  @ApiProperty()
  rate: number;

  @Column({ type: 'jsonb' })
  @ApiProperty()
  children: number[];

  @Column({
    type: 'text',
  })
  @ApiProperty()
  address: string;

  @Column({
    type: 'text',
    default: '',
  })
  comment: string;

  @Column()
  @ApiProperty()
  trial: boolean;

  @Column({
    type: 'boolean',
    default: true,
  })
  @ApiProperty()
  isActive: boolean;

  @Column({
    type: 'enum',
    enum: ['offer', 'deal'],
    default: 'offer',
  })
  @ApiProperty()
  state: 'offer' | 'deal';

  @Column({
    nullable: true,
  })
  @ApiProperty()
  tag: string;

  @Column({
    type: 'timestamp',
    nullable: true,
  })
  @ApiProperty()
  startedAt: Date;

  @Column({
    type: 'timestamp',
    nullable: true,
  })
  @ApiProperty()
  finishedAt: Date;

  createOffer(offer: OfferDto) {
    const { initiator, nanny, parent, requestId, trackingId, details } = offer;
    plainToClassFromExist(this, {
      initiator,
      nanny,
      parent,
      requestId,
      trackingId,
      ...details,
    });
    this.isActive = true;
    this.state = 'offer';
    this.apply(new OfferCreatedEvent(this));
    return this;
  }

  acceptOffer(user: Deal['parent'] | Deal['nanny'], type: Role) {
    if (this.state === 'deal') {
      this.apply(new SameStatusChangeEvent(this));
      return;
    }
    this.checkActivity();
    this.checkOfferResponder(user, type);

    this.state = 'deal';
    this.tag = null;
    this.apply(new DealConcludedEvent(this));
  }

  refuseOffer(user: Deal['parent'] | Deal['nanny'], type: Role) {
    if (this.state === 'deal') {
      this.apply(new SameStatusChangeEvent(this));
      return;
    }
    this.checkActivity();
    this.checkOfferResponder(user, type);

    this.isActive = false;
    this.tag = 'cancelled';
    this.apply(new OfferCancelledEvent(this));
  }

  createAccepted(offer: OfferDto) {
    const { initiator, nanny, parent, requestId, trackingId, details } = offer;
    plainToClassFromExist(this, {
      initiator,
      nanny,
      parent,
      requestId,
      trackingId,
      ...details,
    });
    this.isActive = true;
    this.state = 'deal';
    this.apply(new DealConcludedEvent(this));
    return this;
  }

  cancel(reason: 'cancelled' | 'suspended') {
    this.checkActivity();
    this.isActive = false;
    this.tag = reason;
    this.apply(new DealCancelledEvent(this));
  }

  complete() {
    this.checkActivity();
    this.isActive = false;
    this.tag = 'completed';
    this.apply(new DealCompletedEvent(this));
  }

  startVisit() {
    this.checkActivity();
    this.checkNotStarted();
    this.tag = 'in progress';
    this.startedAt = new Date();
    this.apply(new VisitStartedEvent(this));
  }

  finishVisit() {
    this.checkActivity();
    this.checkStarted();
    this.checkNotFinished();
    this.tag = 'pending_review';
    this.finishedAt = new Date();
    this.isActive = false;
    this.apply(new VisitFinishedEvent(this));
  }

  private checkOfferResponder(
    user: Deal['parent'] | Deal['nanny'],
    type: Role,
  ) {
    assert(this.initiator !== type, 'user cannot create and accept same offer');
    let responderId;
    if (this.initiator === 'parent') {
      responderId = this.nanny;
    } else {
      responderId = this.parent;
    }
    assert(responderId === user, 'you cannot process this offer');
  }

  private checkActivity() {
    assert(this.isActive, 'deal is inactive');
  }

  private checkNotStarted() {
    assert(!this.startedAt, 'deal already started');
  }

  private checkNotFinished() {
    assert(!this.finishedAt, 'deal already finished');
  }

  private checkStarted() {
    assert(this.startedAt, 'deal not started');
  }
}
