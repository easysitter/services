import { from, Observable, of } from 'rxjs';
import { catchError, filter, map } from 'rxjs/operators';
import { ZipCheckResult } from './zip-check-result';
import { District } from './district';
import {DISTRICT_DATA} from './data';
export class DistrictService {
  private districts = DISTRICT_DATA;

  getDistricts(): Observable<District[]> {
    return of(this.districts);
  }

  check(zip: string, districtId: string): Observable<ZipCheckResult> {
    return from(this.districts).pipe(
      filter(district => district.id === districtId),
      map(district => {
        return {
          zip,
          districtId,
          zipBelongsToDistrict: district.zipCodes.includes(zip),
        };
      }),
      catchError(() => of({ zip, districtId, zipBelongsToDistrict: false })),
    );
  }

}
