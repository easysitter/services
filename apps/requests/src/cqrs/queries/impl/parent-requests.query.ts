import { IQuery } from '@nestjs/cqrs';
import { ParentRequest } from '../../../entities/parent-request.entity';

export interface ParentRequestsQueryParams {
  filter: Partial<ParentRequest>;
  sort: {
    sortBy: keyof ParentRequest;
    sortDirection: 'ASC' | 'DESC';
  };
  limit?: number;
}

export class ParentRequestsQuery implements IQuery {
  constructor(
    public readonly params: ParentRequestsQueryParams,
  ) {
  }
}
