/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Transport } from '@nestjs/microservices';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { getClientConfig, patchNest } from '@utils/transport-connector';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  patchNest();
  const app = await NestFactory.create(AppModule);
  const logger = app.get(Logger);
  app.useLogger(logger);
  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);
  try {
    app.connectMicroservice({
      transport: Transport.KAFKA,
      options: {
        client: getClientConfig(),
        consumer: {
          groupId: 'search-service',
        },
      },
    });
    await app.startAllMicroservicesAsync();
  } catch (e) {
    Logger.error(e.message, null, 'Boot');
    return process.exit(1);
  }
  const options = new DocumentBuilder()
    .setTitle('Search api')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  const port = process.env.HTTP_PORT || 3333;
  SwaggerModule.setup('/docs', app, document);
  await app.listen(port, () => {
    logger.log('Listening at http://localhost:' + port + '/' + globalPrefix);
    logger.log('Swagger at http://localhost:' + port + '/docs');
  });
}

bootstrap();
