import { ApiProperty } from '@nestjs/swagger';

export class CreateTokenDto {
  @ApiProperty({
    example: '1',
  })
  user: string;
  @ApiProperty({
    example: 'parent',
    enum: ['parent', 'nanny'],
  })
  role: 'parent' | 'nanny';
}
