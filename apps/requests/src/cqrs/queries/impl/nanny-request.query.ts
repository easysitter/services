import { ParentRequest } from '../../../entities/parent-request.entity';
import { NannyRequest } from '../../../entities/nanny-request.entity';

export class NannyRequestQuery {
  constructor(
    public readonly parentRequest: ParentRequest['id'] | ParentRequest,
    public readonly nanny: NannyRequest['user'],
  ) {}
}
