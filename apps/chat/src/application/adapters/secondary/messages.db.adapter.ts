import { Repository } from 'typeorm';
import { defer, Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { MessageDbEntity } from './entities/message.db.entity';
import { MessageMapper } from './mappers/message.mapper';

import { ChatId, ChatMessage, ChatModel } from '../../../domain/models';
import { MessagesPort } from '../../../domain/ports/secondary';
import { ChatsDbAdapter } from './chats.db.adapter';

export class MessagesDbAdapter implements MessagesPort {
  private mapper = new MessageMapper();

  constructor(
    @InjectRepository(MessageDbEntity)
    private readonly repo: Repository<MessageDbEntity>,
    private readonly chatsDbAdapter: ChatsDbAdapter,
    private logger: Logger,
  ) {
  }

  loadByChatId(chatId: ChatId): Observable<ChatMessage[]> {
    return defer(() => this.repo.find({
      where: {
        chat: chatId.toString(),
      },
      order: {
        createdAt: 'ASC',
      },
    })).pipe(
      map(entities => entities.map(entity => this.mapper.toDomain(entity))),
    );
  }

  save(chat: ChatModel): Observable<boolean> {
    return of(chat.messages.map(message => {
      const entity = this.mapper.fromDomain(message);
      entity.chat = chat.id;
      return entity;
    }))
      .pipe(
        switchMap(entities => this.repo.save(entities)),
        map((result) => result.reduce((last, message) => {
          if (!last) {
            return message;
          }
          return message.createdAt > last.createdAt ? message : last;
        }, null)),
        map((message) => {
          chat.lastMessage = this.mapper.toDomain(message);
          return true;
        }),
        catchError((err: Error) => {
          this.logger.error(err.message, err.stack, 'ChatsDbAdapter');
          return of(false);
        }),
      );
  }

  saveViewed(messages: ChatMessage[]): Observable<boolean> {
    return of(messages.map(message => {
      return this.repo.create({
        id: message.id,
        viewedBy: message.viewedBy,
      });
    }))
      .pipe(
        switchMap(entities => this.repo.save(entities, { transaction: true })),
        map(() => true),
        catchError((err: Error) => {
          this.logger.error(err.message, err.stack, 'ChatsDbAdapter');
          return of(false);
        }),
      );
  }
}
