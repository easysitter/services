export * from './deal-by-id.query.handler';
export * from './find-deals.query.handler';
export * from './find-outdated-deals.query.handler';
