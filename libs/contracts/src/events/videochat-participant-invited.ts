class VideochatParticipantInvitedPayload {
  role: 'nanny' | 'parent';
  userId: string;
  sessionId: string;
}

// tslint:disable-next-line:max-classes-per-file
export class VideochatParticipantInvited {
  static readonly channelName = 'videochat';

  readonly channel = VideochatParticipantInvited.channelName;

  get key() {
    return `${this.payload.role}_${this.payload.userId}`;
  }

  constructor(
    public readonly payload: VideochatParticipantInvitedPayload,
  ) {
  }
}
