import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { DealExceptionEvent } from '../../events/deal-exception.event';
import { Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@EventsHandler(DealExceptionEvent)
export class DealExceptionEventHandler implements IEventHandler<DealExceptionEvent> {

  constructor(
    @Inject('EVENT_BUS')
    private channel: ClientProxy,
  ) {
  }

  handle(event: DealExceptionEvent): any {
    const { deal, exception, action } = event;
    this.channel.emit('exceptions', {
      key: 'deal-exception',
      value: {
        action,
        deal: {
          id: deal.id,
          parent: deal.parent,
          nanny: deal.nanny,
          status: deal.state,
          request: deal.requestId,
        },
        exception: exception.message,
        type: 'deal-exception',
      },
    });
  }
}
