import { IQuery } from '@nestjs/cqrs';

export class OutdatedRequestsQuery implements IQuery {
  constructor(
    public readonly interval = 0,
  ) {
  }
}
