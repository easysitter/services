import { Body, Controller, Get, InternalServerErrorException, Logger, Param, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { EventPattern } from '@nestjs/microservices';

import { DealCancelled, DealConcluded, VisitFinished } from '@event-bus/contracts';
import { PriceCalculator } from '@utils/price-calculator';
import { TimeParser } from '@utils/time-parser';

import { DealPaymentService } from './deal-payment.service';
import { CreateDealPaymentDto } from './dto/create-deal-payment.dto';
import { Payment } from '../entities/payment';
import { DealPaymentsQuery } from './queries/deal-payments.query';
import { UserDealsPaymentsQuery } from './queries/user-deals-payment.query';
import { CreateDealPaymentCommand } from './commands/create-deal-payment.command';
import { CancelDealPaymentsCommand } from './commands/cancel-deal-payments.command';
import { addDays, isBefore } from 'date-fns';

@Controller('users/:user/deals')
@ApiTags('deals')
export class UserDealsController {

  constructor(
    private dealPaymentService: DealPaymentService,
    private timeParser: TimeParser,
    private priceCalculator: PriceCalculator,
    private logger: Logger,
  ) {
  }

  @Get()
  getDealsPayments(@Param('user') user: string): Promise<Payment[]> {
    return this.dealPaymentService.getUserDealPayments(
      UserDealsPaymentsQuery.of(user),
    );
  }

  @Post()
  async createDealPayment(
    @Param('user') user: string,
    @Body() dealData: CreateDealPaymentDto,
  ): Promise<Payment> {
    try {
      const command = CreateDealPaymentCommand.of(
        dealData.deal,
        user,
        dealData.cost,
        'PLN',
      );
      return await this.dealPaymentService.createPayment(command);
    } catch (e) {
      throw new InternalServerErrorException(e.toString(), e);
    }
  }

  @Get(':deal')
  getDealsPayment(
    @Param('user') user: string,
    @Param('deal') deal: string,
  ): Promise<Payment[]> {
    return this.dealPaymentService.getDealPayments(DealPaymentsQuery.of(deal, user));
  }

  @Get(':deal/pending')
  getDealPendingPayment(
    @Param('user') user: string,
    @Param('deal') deal: string,
  ): Promise<Payment> {
    return this.dealPaymentService.getDealPendingPayment(DealPaymentsQuery.of(deal, user));
  }

  @EventPattern(DealConcluded.eventName)
  async onDealConcluded({ value }: { value: DealConcluded['payload'] }) {
    try {
      const { fromTime, toTime, date, nanny, children } = value;
      const fromDate = this.timeParser.parseTime(`${fromTime}`, new Date(date));
      let toDate = this.timeParser.parseTime(`${toTime}`, new Date(date));
      if (isBefore(toDate, fromDate)) {
        toDate = addDays(toDate, 1);
      }
      this.logger.log({
        from: fromDate,
        to: toDate,
        nanny,
        children,
      }, `UserDealsController [${DealConcluded.eventName}]`);
      const price = await this.priceCalculator.getDealPrice(
        {
          from: fromDate,
          to: toDate,
          nanny,
          children,
        },
      );
      await this.dealPaymentService.createPayment(
        CreateDealPaymentCommand.of(
          `${value.id}`,
          `${value.parent}`,
          price,
          'PLN',
        ),
      );
    } catch (e) {
      const error = e as Error;
      this.logger.error(error.toString(), error.stack, `UserDealsController [${DealConcluded.eventName}]`);
    }
  }

  @EventPattern(VisitFinished.eventName)
  async onVisitFinished({ value }: { value: VisitFinished['payload'] }) {
    try {
      const { fromTime, toTime, date, nanny, children } = value;
      const fromDate = this.timeParser.parseTime(`${fromTime}`, new Date(date));
      let toDate = this.timeParser.parseTime(`${toTime}`, new Date(date));
      if (isBefore(toDate, fromDate)) {
        toDate = addDays(toDate, 1);
      }
      const price = await this.priceCalculator.getOvertimePrice(
        {
          from: fromDate,
          to: toDate,
          startedAt: new Date(value.startedAt),
          finishedAt: new Date(value.finishedAt),
          nanny,
          children,
        },
      );
      await this.dealPaymentService.createPayment(
        CreateDealPaymentCommand.of(
          `${value.id}`,
          `${value.parent}`,
          price,
          'PLN',
        ),
      );
    } catch (e) {
      const error = e as Error;
      this.logger.error(error.toString(), error.stack, `UserDealsController [${DealConcluded.eventName}]`);
    }
  }

  @EventPattern(DealCancelled.eventName)
  async onDealCancelled({ value }: { value: DealCancelled['payload'] }) {
    try {
      const payments = await this.dealPaymentService.getDealPayments(
        DealPaymentsQuery.of(`${value.id}`, `${value.parent}`),
      );
      await this.dealPaymentService.cancel(
        CancelDealPaymentsCommand.of(payments),
      );
    } catch (e) {
      const error = e as Error;
      this.logger.error(error.toString(), error.stack, `UserDealsController [${DealCancelled.eventName}]`);
    }
  }
}
