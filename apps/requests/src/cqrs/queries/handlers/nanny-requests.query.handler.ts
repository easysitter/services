import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { NannyRequestsQuery } from '../impl';
import { NannyRequest } from '../../../entities/nanny-request.entity';

@QueryHandler(NannyRequestsQuery)
export class NannyRequestsQueryHandler
  implements IQueryHandler<NannyRequestsQuery> {
  constructor(
    @InjectRepository(NannyRequest)
    private readonly nannyRequestRepository: Repository<NannyRequest>,
  ) {}

  execute(query: NannyRequestsQuery): Promise<NannyRequest[]> {
    return this.nannyRequestRepository.find({
      where: {
        user: query.user,
        isActive: query.isActive,
      },
      order: {
        updatedAt: 'DESC',
      },
      relations: ['parentRequest'],
    });
  }
}
