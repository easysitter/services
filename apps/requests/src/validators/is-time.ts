import { Matches } from 'class-validator';

export const IsTime = (validationOptions = {}) => {
  const TIME_PATTERN = /^(([0-1]?\d)|(2[0-3])):[0-5]?\d(\+\d{2,4}|Z)?$/;
  return Matches(TIME_PATTERN, {
    message: '$property should have time format hh:mm',
    ...validationOptions,
  });
};
