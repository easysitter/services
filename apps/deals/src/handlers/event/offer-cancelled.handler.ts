import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { OfferCancelledEvent } from '../../events';
import { OfferCancelled } from '@event-bus/contracts';

@EventsHandler(OfferCancelledEvent)
export class OfferCancelledHandler implements IEventHandler<OfferCancelledEvent> {

  constructor(
    @Inject('EVENT_BUS')
    private channel: ClientProxy,
  ) {
  }

  async handle(command: OfferCancelledEvent): Promise<void> {
    const { deal } = command;
    const msg = new OfferCancelled({
      deal: deal.id,
      request: deal.requestId,
      nanny: deal.nanny,
      parent: deal.parent,
    });
    this.channel.emit(msg.name, msg.payload);
  }
}
