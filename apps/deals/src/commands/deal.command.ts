import { ICommand } from '@nestjs/cqrs';
import { Deal } from '../models';

export abstract class DealCommand implements ICommand {
  constructor(public readonly deal: Deal) {}
}
