import { ApiProperty } from '@nestjs/swagger';
import { ProfileDto } from './profile.dto';

export class CreateProfileDto {
  @ApiProperty()
  userId: string;
  @ApiProperty({ type: () => ProfileDto })
  profile: ProfileDto;
}
