import { AttentionGroupEntity } from '../..';
import { CreateOutdatedRequestRemindersCommand } from './create-outdated-request-reminders.command';

export abstract class CreateOutdatedRequestRemindersUseCase {
  abstract createReminders(command: CreateOutdatedRequestRemindersCommand): Promise<AttentionGroupEntity>;
}
