import { IQuery } from '@nestjs/cqrs';
import { NannyRequest } from '../../../entities/nanny-request.entity';

export class NannyRequestsQuery implements IQuery {
  constructor(
    public readonly user: NannyRequest['user'],
    public readonly isActive: boolean = true,
  ) {}
}
