import { Observable } from 'rxjs';

import { MessageDto } from './message.dto';

import { ChatId, ChatMessage, ChatModel } from '../../models';

export abstract class ChattingUseCase {
  abstract findChatById(id: ChatId): Observable<ChatModel>;

  abstract sendMessage(chat: ChatModel, data: MessageDto): Observable<ChatMessage>;

  abstract getMessages(chat: ChatModel): Observable<ChatMessage[]>;

  abstract getUnseenMessages(chat: ChatId, user: string): Observable<number>;

  abstract markMessagesAsViewed(chat: ChatModel, lastMessageDate: Date, user: string): Observable<boolean>;
}
