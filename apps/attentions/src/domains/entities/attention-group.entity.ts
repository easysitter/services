import { AttentionEntity, AttentionType } from './attention.entity';
import { AttentionTargetEntity, AttentionTargetType } from './attention-target.entity';

export class AttentionGroupEntity {
  public readonly attentions: AttentionEntity[] = [];

  addAttention(attention: AttentionEntity) {
    this.attentions.push(attention);
    return this;
  }

  getForType(type: AttentionType): AttentionEntity[] {
    return this.attentions.filter(attention => attention.isTypeOf(type));
  }

  getForTargetType(refType: AttentionTargetType): AttentionEntity[] {
    return this.attentions.filter(attention => attention.target.isTypeOf(refType));
  }

  getForTarget(target: AttentionTargetEntity): AttentionEntity[] {
    return this.attentions.filter(attention => attention.isFor(target));
  }
}
