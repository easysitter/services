import { AttentionTargetEntity } from './attention-target.entity';

export type AttentionType = string;

export class AttentionEntity {
  protected constructor(
    public readonly type: AttentionType,
    public readonly target: AttentionTargetEntity,
  ) {
  }

  static of(
    type: AttentionType,
    target: AttentionTargetEntity,
  ) {
    return new AttentionEntity(type, target);
  }

  isFor(target: AttentionTargetEntity) {
    return this.target.equals(target);
  }

  isTypeOf(type: AttentionType) {
    return this.type === type;
  }
}
