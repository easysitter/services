import { IsNotEmpty, IsOptional, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { Role } from '../types/role.type';
import { ApiProperty } from '@nestjs/swagger';

// tslint:disable-next-line:max-classes-per-file
class OfferDetails {
  @ApiProperty()
  @IsNotEmpty()
  rate: number;

  @ApiProperty({ type: 'string', format: 'date' })
  @IsNotEmpty()
  @Type(() => Date)
  date: Date;

  @ApiProperty()
  @IsNotEmpty()
  fromTime: string;

  @ApiProperty()
  @IsNotEmpty()
  toTime: string;

  @ApiProperty()
  @IsNotEmpty()
  address: string;

  @ApiProperty({ type: Number, isArray: true })
  @IsNotEmpty()
  children: number[];

  @ApiProperty()
  @IsNotEmpty()
  trial: boolean;

  @ApiProperty()
  @IsNotEmpty()
  safePayment: boolean;

  @ApiProperty()
  @IsOptional()
  comment: string;
}

// tslint:disable-next-line:max-classes-per-file
export class OfferDto {

  @ApiProperty()
  @IsNotEmpty()
  requestId: string;

  @ApiProperty()
  @IsNotEmpty()
  trackingId: string;

  @ApiProperty()
  @IsNotEmpty()
  parent: number;

  @ApiProperty()
  @IsNotEmpty()
  nanny: number;

  @ApiProperty({ enum: ['parent', 'nanny'] })
  @IsNotEmpty()
  initiator: Role;

  @ApiProperty()
  @IsNotEmpty()
  @ValidateNested({ each: true })
  @Type(of => OfferDetails)
  details: OfferDetails;
}
