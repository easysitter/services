import { Calculator } from './calculator';
import { addMinutes, differenceInMinutes } from 'date-fns';
import { CalculatorParams } from './calculator-params';
import { CalculatorTracer } from '../utils/calculator-tracer';

export class OvertimeCalculator extends Calculator {
  constructor(
    private wrapped: Calculator,
    private step: number,
    private overtimeOnly: boolean = false,
  ) {
    super();
  }

  async getPrice(deal: CalculatorParams, tracer: CalculatorTracer): Promise<number> {
    const { startedAt, finishedAt, from, to } = deal;

    const originalPrice = await (this.overtimeOnly
      ? 0
      : this.wrapped.getPrice(deal, tracer.fork('DEAL')));
    if (!startedAt || !finishedAt) {
      return originalPrice;
    }

    const overtimeAfter = Math.max(0, differenceInMinutes(finishedAt, to));
    const overtimeBefore = Math.max(0, differenceInMinutes(from, startedAt));

    if (overtimeAfter + overtimeBefore <= 15) {
      return originalPrice;
    }
    let price = originalPrice;

    if (overtimeBefore > 0) {
      const overtimeBeforeDeal: CalculatorParams = {
        ...deal,
        from: startedAt,
        to: from,
      };
      price = price + (await this.wrapped.getPrice(overtimeBeforeDeal, tracer.fork('PRE DEAL OVERTIME')));
    }

    if (overtimeAfter > 0) {
      const overtimeAfterRounded =
        Math.ceil(overtimeAfter / this.step) * this.step;
      const overtimeAfterDeal: CalculatorParams = {
        ...deal,
        from: to,
        to: addMinutes(to, overtimeAfterRounded),
      };
      price = price + (await this.wrapped.getPrice(overtimeAfterDeal, tracer.fork('POST DEAL OVERTIME')));
    }

    return price;
  }
}
