import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Transaction, TransactionRepository } from 'typeorm';
import { SuspendOutdatedRequestCommand } from '../impl';
import { ParentRequest } from '../../../entities/parent-request.entity';
import { NannyRequest } from '../../../entities/nanny-request.entity';

@CommandHandler(SuspendOutdatedRequestCommand)
export class SuspendOutdatedRequestCommandHandler implements ICommandHandler<SuspendOutdatedRequestCommand> {
  constructor(
    private publisher: EventPublisher,
    @InjectRepository(ParentRequest)
    private parentRequestRepository: Repository<ParentRequest>,
    @InjectRepository(NannyRequest)
    private nannyRequestRepository: Repository<NannyRequest>,
  ) {
  }

  async execute(command: SuspendOutdatedRequestCommand): Promise<any> {
    const parentRequest = this.publisher.mergeObjectContext(command.request);
    const nannyRequests = await this.nannyRequestRepository.find({
      where: {
        parentRequest,
        isActive: true,
      },
    });

    nannyRequests.forEach(req => req.parentRequest = parentRequest);
    return this._execute(
      parentRequest,
      nannyRequests.map(r => this.publisher.mergeObjectContext(r)),
    );
  }

  @Transaction({
    isolation: 'READ COMMITTED',
  })
  private async _execute(
    parentRequest: ParentRequest,
    nannyRequests: NannyRequest[],
    @TransactionRepository(ParentRequest)
      parentRequestRepository?: Repository<ParentRequest>,
    @TransactionRepository(NannyRequest)
      nannyRequestRepository?: Repository<NannyRequest>,
  ) {
    parentRequest.markOutdated();
    await parentRequestRepository.save(parentRequest);

    nannyRequests.forEach(r => r.markOutdated());
    await nannyRequestRepository.save(nannyRequests);
    parentRequest.commit();
    nannyRequests.forEach(r => r.commit());
  }
}
