import { AttentionGroupEntity } from '../../entities/attention-group.entity';

export interface SaveAttentionsPort {
  save(group: AttentionGroupEntity): Promise<AttentionGroupEntity>;
}
