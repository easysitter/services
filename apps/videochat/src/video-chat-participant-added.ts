import { IEvent } from '@nestjs/cqrs';
import { VideoChatParticipantEntity } from './video-chat-participant.entity';

export class VideoChatParticipantAdded implements IEvent {
  constructor(
    public readonly participant: VideoChatParticipantEntity,
  ) {
  }
}
