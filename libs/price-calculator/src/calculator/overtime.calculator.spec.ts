import { Calculator } from './calculator';
import { OvertimeCalculator } from './overtime.calculator';
import { PlainCalculator } from './plain.calculator';
import { parse } from 'date-fns';
import { DayNightSplitCalculator } from './day-night.calculator';
import { PlainRate } from '../rates/plain.rate';
import { CalculatorTracer, NoopTracer } from '../utils/calculator-tracer';

describe('Overtime calculator', () => {

  let calculator: Calculator;
  let tracer: CalculatorTracer;

  describe('Overtime with plain', () => {

    let plainRate: number;
    beforeEach(async () => {
      const rates = new PlainRate(10);
      plainRate = await rates.getRate(null);
      calculator = new OvertimeCalculator(
        new PlainCalculator(rates),
        30,
        true,
      );
      tracer = new NoopTracer();
    });

    it('should return 10PLN for 1h overtime', async () => {
      const deal = {
        from: parse('08:00+02', 'HH:mmX', new Date()),
        to: parse('12:00+02', 'HH:mmX', new Date()),
        startedAt: parse('08:15+02', 'HH:mmX', new Date()),
        finishedAt: parse('13:00+02', 'HH:mmX', new Date()),
      };
      expect(await calculator.getPrice(deal, tracer)).toBe(plainRate);
    });

    it('should return 0PLN for less then 15m overtime', async () => {
      const deal = {
        from: parse('08:00+02', 'HH:mmX', new Date()),
        to: parse('12:00+02', 'HH:mmX', new Date()),
        startedAt: parse('08:15+02', 'HH:mmX', new Date()),
        finishedAt: parse('12:15+02', 'HH:mmX', new Date()),
      };
      expect(await calculator.getPrice(deal, tracer)).toBe(0);
    });
  });

  describe('Overtime with day-night', () => {
    let dayRate: number;
    let nightRate: number;
    const dayRates = new PlainRate(10);
    const nightRates = new PlainRate(100);
    let baseCalculator: Calculator;
    beforeAll(async () => {
      dayRate = await dayRates.getRate(null);
      nightRate = await nightRates.getRate(null);
    });
    beforeEach(() => {
      baseCalculator = new DayNightSplitCalculator(
        new PlainCalculator(dayRates),
        new PlainCalculator(nightRates),
      );
    });
    describe('Overtime only', () => {
      beforeEach(async () => {
        calculator = new OvertimeCalculator(baseCalculator, 30, true);
      });

      it('should return 10PLN for 1h day overtime', async () => {
        const deal = {
          from: parse('08:00+02', 'HH:mmX', new Date()),
          to: parse('12:00+02', 'HH:mmX', new Date()),
          startedAt: parse('08:15+02', 'HH:mmX', new Date()),
          finishedAt: parse('13:00+02', 'HH:mmX', new Date()),
        };
        expect(await calculator.getPrice(deal, tracer)).toBe(dayRate);
      });
      it('should return 100PLN for 1h night overtime', async () => {
        const deal = {
          from: parse('20:00+02', 'HH:mmX', new Date()),
          to: parse('22:00+02', 'HH:mmX', new Date()),
          startedAt: parse('20:00+02', 'HH:mmX', new Date()),
          finishedAt: parse('23:00+02', 'HH:mmX', new Date()),
        };
        expect(await calculator.getPrice(deal, tracer)).toBe(nightRate);
      });
      it('should return 110PLN for 1h day and 1h night overtime', async () => {
        const deal = {
          from: parse('18:00+02', 'HH:mmX', new Date()),
          to: parse('20:00+02', 'HH:mmX', new Date()),
          startedAt: parse('18:00+02', 'HH:mmX', new Date()),
          finishedAt: parse('22:00+02', 'HH:mmX', new Date()),
        };
        expect(await calculator.getPrice(deal, tracer)).toBe(dayRate + nightRate);
      });

    });

    describe('Full price', () => {
      beforeEach(async () => {
        calculator = new OvertimeCalculator(baseCalculator, 30, false);
      });

      it('should return 130PLN for 2h plus 1h day and 1h night overtime', async () => {
        const deal = {
          from: parse('18:00+02', 'HH:mmX', new Date()),
          to: parse('20:00+02', 'HH:mmX', new Date()),
          startedAt: parse('18:00+02', 'HH:mmX', new Date()),
          finishedAt: parse('22:00+02', 'HH:mmX', new Date()),
        };
        expect(await calculator.getPrice(deal, tracer)).toBe(dayRate * 2 + dayRate + nightRate);
      });
    });
  });
});
