import { Connection, LessThan, Repository } from 'typeorm';
import { subMinutes } from 'date-fns';

import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreatePaymentParams, PaymentProvider } from '../payment-providers/payment-provider';
import { DealPayment } from './deal-payment';
import { CreateDealPaymentCommand } from './commands/create-deal-payment.command';
import { Payment } from '../entities/payment';
import { DealPaymentsQuery } from './queries/deal-payments.query';
import { UserDealsPaymentsQuery } from './queries/user-deals-payment.query';
import { OutdatedDealsQuery } from './queries/outdated-deals.query';
import { CancelDealPaymentsCommand } from './commands/cancel-deal-payments.command';
import { EventPublisher } from '@nestjs/cqrs';
import { DEAL_PAYMENT_TYPE } from './constants';

@Injectable()
export class DealPaymentService {
  constructor(
    private paymentProvider: PaymentProvider,
    @InjectRepository(DealPayment)
    private dealPaymentRepository: Repository<DealPayment>,
    @InjectRepository(Payment)
    private paymentRepository: Repository<Payment>,
    private connection: Connection,
    private publisher: EventPublisher,
    private logger: Logger,
  ) {
  }

  async getDealPayments(query: DealPaymentsQuery): Promise<Payment[]> {
    const dealPayments = await this.dealPaymentRepository.find({
      where: {
        userId: query.user,
        dealId: query.deal,
      },
      relations: ['payment'],
    });

    return dealPayments.map(dealPayment => dealPayment.payment);
  }

  async getUserDealPayments(query: UserDealsPaymentsQuery): Promise<Payment[]> {
    const dealPayments = await this.dealPaymentRepository.find({
      where: {
        userId: query.user,
      },
      relations: ['payment'],
    });
    return dealPayments.map(dealPayment => dealPayment.payment);
  }

  async createPayment(command: CreateDealPaymentCommand) {
    const paymentParams: CreatePaymentParams = {
      amount: command.amount,
      currency: command.currency,
      meta: {
        deal: command.deal,
        user: command.user,
        name: `Easysitter visit#${command.deal}`,
        returnUrl: `https://parent.easysitter.pl/deals/`,
      },
      type: DEAL_PAYMENT_TYPE,
    };
    this.logger.log(paymentParams, `DealPaymentService [createPayment]`);
    const payment = await this.paymentProvider.createPayment(paymentParams);
    const dealPayment = this.dealPaymentRepository.create({
      dealId: command.deal,
      userId: command.user,
      payment,
    });

    await this.dealPaymentRepository.save(dealPayment);
    return payment;
  }

  async getDealPendingPayment(query: DealPaymentsQuery) {
    const payments = await this.getDealPayments(query);
    return payments.find(payment => !payment.isCompleted);
  }

  async getOutdated(query: OutdatedDealsQuery) {
    const dealPayments = await this.dealPaymentRepository.find({
      where: {
        createdAt: LessThan(subMinutes(new Date(), query.outdatedPeriod).toISOString()),
      },
      relations: ['payment'],
    });
    return dealPayments.map(dealPayment => dealPayment.payment).filter(payment => payment.isActive);
  }

  async cancelOutdated(command: CancelDealPaymentsCommand) {
    const payments = command.payments.map(payment => {
      payment = this.publisher.mergeObjectContext(payment);
      payment.cancel();
      return payment;
    });
    await this.paymentRepository.save(payments, { transaction: true });
    payments.forEach(payment => {
      payment.commit();
    });
  }

  async cancel(command: CancelDealPaymentsCommand) {
    const payments = command.payments.map(payment => {
      payment.cancel();
      return payment;
    });
    await this.paymentRepository.save(payments, { transaction: true });
  }
}
