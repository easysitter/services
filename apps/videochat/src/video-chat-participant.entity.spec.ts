import { VideoChatParticipantEntity } from './video-chat-participant.entity';
import { VideoChatParticipantAdded } from './video-chat-participant-added';
import { VideoChatTerminated } from './video-chat-terminated';

describe('VideoChatParticipantEntity', () => {
  let entity: VideoChatParticipantEntity;
  let applyMock;
  beforeEach(() => {
    entity = new VideoChatParticipantEntity();
    applyMock = jest.spyOn(entity, 'apply');
  });

  describe('addedToSession', () => {
    it('should apply VideoChatParticipantAdded event ', () => {
      entity.addedToSession();
      expect(applyMock).toHaveBeenCalledWith(
        expect.any(VideoChatParticipantAdded),
      );
    });

    it('should be applied afterCreate', () => {
      entity.afterCreate();
      expect(applyMock).toHaveBeenCalledWith(
        expect.any(VideoChatParticipantAdded),
      );
    });
  });
  describe('terminated', () => {
    it('should apply VideoChatTerminated event ', () => {
      entity.terminated();

      expect(entity.terminatedAt).toBeDefined();
      expect(applyMock).toHaveBeenCalledWith(expect.any(VideoChatTerminated));
    });
  });
});
