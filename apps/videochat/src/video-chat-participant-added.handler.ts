import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { VideoChatParticipantAdded } from './video-chat-participant-added';
import { ClientProxy } from '@nestjs/microservices';
import { Inject } from '@nestjs/common';
import { VideochatParticipantInvited } from '@event-bus/contracts';

@EventsHandler(VideoChatParticipantAdded)
export class VideoChatParticipantAddedHandler implements IEventHandler<VideoChatParticipantAdded> {
  constructor(
    @Inject('EVENT_BUS')
    private readonly eventBus: ClientProxy,
  ) {
  }

  async handle({ participant }: VideoChatParticipantAdded): Promise<void> {
    const { role, userId, sessionId } = participant;
    const message = new VideochatParticipantInvited({ role, userId, sessionId });
    this.eventBus.emit(message.channel, {
      key: message.key,
      value: message.payload,
    });
  }
}
