import { IEvent } from '@nestjs/cqrs';
import { Payment } from '../entities/payment';

export class PaymentCancelledEvent implements IEvent {
  constructor(public readonly payment: Payment) {
  }

}
