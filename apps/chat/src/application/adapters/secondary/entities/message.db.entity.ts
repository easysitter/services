import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@Entity('message')
export class MessageDbEntity {
  @PrimaryGeneratedColumn('uuid')
  @ApiProperty()
  id: string;
  @Column()
  @ApiProperty()
  user: string;
  @Column()
  @ApiProperty()
  text: string;
  @Column({ type: 'timestamp' })
  @ApiProperty({
    type: 'string',
    format: 'date-time',
  })
  createdAt: number | Date;
  @Column()
  @ApiProperty()
  chat: string;

  @Column('jsonb', {
    default: () => `'[]'::jsonb`,
  })
  viewedBy: string[];
}
