import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class PushToken {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  user: string;

  @Column({
    unique: true,
  })
  token: string;
}
