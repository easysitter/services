FROM node:12-alpine AS deps

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm ci
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .
EXPOSE 3000

FROM deps as videochat-build
RUN npm run build videochat
RUN npm prune --production

FROM node:12-alpine AS videochat
WORKDIR /usr/src/app
COPY package*.json ./
COPY --from=videochat-build /usr/src/app/node_modules ./node_modules
COPY --from=videochat-build /usr/src/app/dist/apps/videochat ./dist/apps/videochat
CMD [ "node", "dist/apps/videochat/main.js" ]
