import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Payment } from './payment';

@Entity()
export class ServiceSubscription {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  userId: string;

  @Column({ default: false })
  isActive: boolean;

  @OneToOne(() => Payment)
  @JoinColumn()
  payment: Payment;

  activate() {
    this.isActive = true;
  }
}
