import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { FindOutdatedDeals } from '../../queries';
import { Deal } from '../../models';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@QueryHandler(FindOutdatedDeals)
export class FindOutdatedDealsQueryHandler implements IQueryHandler<FindOutdatedDeals, Deal[]> {
  constructor(
    @InjectRepository(Deal)
    private repo: Repository<Deal>,
  ) {
  }

  async execute(query: FindOutdatedDeals): Promise<Deal[]> {
    /**
     * @todo fix timezone
     */
    return this.repo
      .createQueryBuilder('deal')
      .where(`"isActive" = true`)
      .andWhere(`"state" = 'deal'`)
      .andWhere(`"startedAt" IS NULL`)
      .andWhere(`case
                        when (("date" + "toTime"::timetz)::timestamptz > ("date" + "fromTime"::timetz)::timestamptz)
                        then ("date" + "toTime"::timetz)::timestamptz
                        else ("date" + "toTime"::timetz)::timestamptz + interval '1 day'
                      end < CURRENT_TIMESTAMP`)
      .getMany();
  }
}
