import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AcceptOfferCommand } from '../../commands';
import { DealService } from '../../deal.service';
import { DealExceptionEvent } from '../../events/deal-exception.event';

@CommandHandler(AcceptOfferCommand)
export class AcceptOfferCommandHandler
  implements ICommandHandler<AcceptOfferCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly service: DealService,
  ) {
  }

  async execute(command: AcceptOfferCommand): Promise<any> {
    const currentDeal = this.publisher.mergeObjectContext(command.deal);
    try {
      currentDeal.acceptOffer(command.responder.user, command.responder.type);
    } catch (e) {
      currentDeal.apply(new DealExceptionEvent(currentDeal, 'accept-offer', e));
      currentDeal.commit();
      throw e;
    }
    await this.service.save(currentDeal);
    const related = await this.service.findRelatedOffers(currentDeal);
    const relatedDeals = related.map(r => {
      const offer = this.publisher.mergeObjectContext(r);
      try {
        offer.cancel('suspended');
      } catch (e) {
        offer.apply(new DealExceptionEvent(offer, 'suspended', e));
      }
      return offer;
    });

    const deals = [currentDeal, ...relatedDeals];
    await this.service.saveAll(deals);
    deals.forEach(deal => deal.commit());
    return currentDeal;
  }
}
