import { Injectable, NotFoundException, PipeTransform } from '@nestjs/common';
import { QueryBus } from '@nestjs/cqrs';
import { defer, of, throwError } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Deal } from './models';
import { DealByIdQuery } from './queries';

@Injectable()
export class DealByIdPipe implements PipeTransform<string, Promise<Deal>> {
  constructor(private readonly queryBus: QueryBus) {}

  async transform(value: string): Promise<Deal> {
    const query = new DealByIdQuery(+value);
    return defer(() => this.queryBus.execute<DealByIdQuery, Deal>(query)).pipe(
      switchMap(deal =>
        deal ? of(deal) : throwError(new NotFoundException()),
      ),
    ).toPromise();
  }
}
