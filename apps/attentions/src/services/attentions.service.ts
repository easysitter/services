import { Injectable } from '@nestjs/common';
import {
  AttentionEntity,
  AttentionGroupEntity,
  AttentionTargetEntity,
  LoadAttentionsPort,
  LoadAttentionsQuery,
  SaveAttentionsPort,
} from '../domains';
import { Attention } from '../db/attention.db-entity';
import { InjectRepository } from '@nestjs/typeorm';
import { FindConditions, Repository } from 'typeorm';

@Injectable()
export class AttentionsService implements SaveAttentionsPort, LoadAttentionsPort {

  constructor(
    @InjectRepository(Attention)
    private attentionRepository: Repository<Attention>,
  ) {
  }

  async loadAttentions(query: LoadAttentionsQuery): Promise<AttentionEntity[]> {
    const dbObjects = await this.find(query);
    return dbObjects.map(({ type, refType, refId, data }) => AttentionEntity.of(
      type, AttentionTargetEntity.of(refType, refId, data),
    ));
  }

  private async find(query: LoadAttentionsQuery): Promise<Attention[]> {
    const { attentionType, targetId, targetType } = query as unknown as any;
    const where: FindConditions<Attention> = {};
    if (attentionType) {
      where.type = attentionType;
    }
    if (targetType) {
      where.refType = targetType;
    }
    if (targetId) {
      where.refType = targetType;
      where.refId = targetId;
    }

    return this.attentionRepository.find({ where });
  }

  async save(group: AttentionGroupEntity): Promise<AttentionGroupEntity> {
    const waitFor = group.attentions.map(async (attention) => {
      const savedAttention = await this.findAttention(attention);
      if (savedAttention) {
        return false;
      }
      try {
        await this.attentionRepository.save({
          type: attention.type,
          refId: attention.target.refId,
          refType: attention.target.refType,
          data: attention.target.data,
        });
        return attention;
      } catch (e) {
        return false;
      }
    });
    const result = await Promise.all(waitFor);
    return result.filter(saved => !!saved)
      .reduce(
        (out, saved) => out.addAttention(saved as AttentionEntity),
        new AttentionGroupEntity(),
      );
  }

  private async findAttention(attention: AttentionEntity): Promise<Attention> {
    return this.attentionRepository.findOne({
      where: {
        type: attention.type,
        refType: attention.target.refType,
        refId: attention.target.refId,
      },
    });
  }
}
