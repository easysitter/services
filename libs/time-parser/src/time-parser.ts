import { parse } from 'date-fns';

export class TimeParser {
  parseTime(time, date = new Date()) {
    /**
     * @todo fix timezone
     */
    const timezone = '';
    return parse(`${time}${timezone}`, 'HH:mmX', date);
  }
}
