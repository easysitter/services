import { CallHandler, ExecutionContext, Injectable, Logger, NestInterceptor } from '@nestjs/common';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    // console.log('Before...');

    const now = Date.now();
    return next
      .handle()
      .pipe(
        tap(() => {
          const req = context.switchToHttp().getRequest();
          const res = context.switchToHttp().getResponse();
          Logger.log(`${req.method.toUpperCase()} ${req.url} ${res.statusCode} ${Date.now() - now}ms`, `DEALS`, false);
        }),
        catchError((err: Error) => {

          const req = context.switchToHttp().getRequest();
          const res = context.switchToHttp().getResponse();
          Logger.error(`${req.method.toUpperCase()} ${req.url} ${Date.now() - now}ms`, JSON.stringify(err.message, null, 2), `DEALS`, false);
          return throwError(err);
        }),
      );
  }
}
