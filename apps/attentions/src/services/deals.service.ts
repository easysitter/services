import { HttpService, Inject, Injectable } from '@nestjs/common';
import { AttentionTargetEntity, LoadDealsToRemindPort } from '../domains';
import { COMING_DEALS_URL } from '../injection.tokens';
import { map } from 'rxjs/operators';

@Injectable()
export class DealsService implements LoadDealsToRemindPort {
  constructor(
    private http: HttpService,
    @Inject(COMING_DEALS_URL)
    private comingDealsUrl: string,
  ) {
  }

  async load(): Promise<AttentionTargetEntity[]> {
    const deals = await this.http.get<Array<{ id, parent, nanny, date, fromTime, toTime }>>(this.comingDealsUrl).pipe(
      map(response => response.data),
    ).toPromise();
    // const deals = [
    //   { id: 1, parent: 1, nanny: 1, date: new Date(), fromTime: '9:00', toTime: '11:00' },
    //   { id: 2, parent: 1, nanny: 1, date: new Date(), fromTime: '12:00', toTime: '15:00' },
    //   { id: 3, parent: 1, nanny: 1, date: new Date(), fromTime: '19:00', toTime: '21:00' },
    // ];
    return deals.map(({ id, ...data }) => AttentionTargetEntity.of('deal', `${id}`, data));
  }
}
