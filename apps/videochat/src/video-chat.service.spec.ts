import { VideoChatService } from './video-chat.service';
import { Test } from '@nestjs/testing';
import { getConnectionToken, getRepositoryToken } from '@nestjs/typeorm';
import { OpentokAdapter } from './opentok.adapter';
import { VideoChatSessionEntity } from './video-chat-session.entity';
import { VideoChatParticipantEntity } from './video-chat-participant.entity';
import { EventPublisher } from '@nestjs/cqrs';

const mockParticipantSave = jest.fn();
const mockParticipantRepository = {
  findOne: jest.fn(),
  find: jest.fn(),
  save: mockParticipantSave,
  create: jest.fn().mockImplementation(value => {
    return {
      ...value,
      commit: jest.fn(),
    };
  }),
};
const mockSessionSave = jest.fn();
const mockSessionRepository = {
  find: jest.fn(),
  save: mockSessionSave,
  create: jest.fn().mockImplementation(value => value),
};

const mockTransactionCommit = jest.fn();
const mockQueryRunner = {
  startTransaction: jest.fn(),
  commitTransaction: mockTransactionCommit,
  rollbackTransaction: jest.fn(),
  manager: {
    getRepository: jest.fn().mockImplementation(entity => {
      switch (entity) {
        case VideoChatParticipantEntity:
          return mockParticipantRepository;
        case VideoChatSessionEntity:
          return mockSessionRepository;
        default:
          return null;
      }
    }),
  },
};

const mockConnection = {
  createQueryRunner: jest.fn().mockReturnValue(mockQueryRunner),
};

const mockEventPublisher = {
  mergeObjectContext: value => value,
};
const mockOpentokAdapter = {
  createSession: jest.fn().mockResolvedValue({ sessionId: 'fake ot session' }),
  createToken: jest.fn(),
  sendTerminationSignal: jest.fn(),
};

describe('VideoChat service', () => {
  let service: VideoChatService;
  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        VideoChatService,
        { provide: OpentokAdapter, useValue: mockOpentokAdapter },
        { provide: getConnectionToken(), useValue: mockConnection },
        {
          provide: getRepositoryToken(VideoChatSessionEntity),
          useValue: mockSessionRepository,
        },
        {
          provide: getRepositoryToken(VideoChatParticipantEntity),
          useValue: mockParticipantRepository,
        },
        {
          provide: EventPublisher,
          useValue: mockEventPublisher,
        },
      ],
    }).compile();

    service = moduleRef.get(VideoChatService);

    mockTransactionCommit.mockReset();
    mockSessionSave.mockReset();
  });

  describe('Create session', () => {
    const FAKE_PARENT_ID = '1';
    const FAKE_NANNY_ID = '1';
    const FAKE_SESSION_ID = '1';
    beforeEach(() => {
      mockSessionSave.mockImplementation(data => {
        data.id = FAKE_SESSION_ID;
        return data;
      });
    });
    describe('When session is not exists', () => {
      beforeEach(() => {
        mockSessionRepository.find.mockResolvedValue(null);
        mockParticipantRepository.find.mockResolvedValue(null);
      });
      it('should be created new session', async () => {
        const result = await service.createSession({
          nanny: FAKE_NANNY_ID,
          parent: FAKE_PARENT_ID,
        });

        expect(result).toBeDefined();
        expect(result.id).toEqual(FAKE_SESSION_ID);
        expect(mockTransactionCommit).toHaveBeenCalled();
      });
      it('should be created new session', async () => {
        const result = await service.createSession({
          nanny: FAKE_NANNY_ID,
          parent: FAKE_PARENT_ID,
        });

        expect(result).toBeDefined();
        expect(result.id).toEqual(FAKE_SESSION_ID);
        expect(mockTransactionCommit).toHaveBeenCalled();
      });
    });
    describe('When session exists', () => {
      const mockAddedToSession = jest.fn();
      beforeEach(() => {
        mockSessionRepository.find.mockResolvedValue(null);
        mockParticipantRepository.find.mockResolvedValue([
          {
            userId: FAKE_PARENT_ID,
            role: 'parent',
          } as VideoChatParticipantEntity,
        ]);
        mockParticipantRepository.findOne.mockResolvedValue({
          userId: FAKE_NANNY_ID,
          role: 'nanny',
          session: {
            id: FAKE_SESSION_ID,
          },
          addedToSession: mockAddedToSession,
          commit: jest.fn(),
        });
      });

      afterEach(() => {
        mockAddedToSession.mockReset();
      });

      it('should not create new session', async () => {
        const result = await service.createSession({
          parent: FAKE_PARENT_ID,
          nanny: FAKE_NANNY_ID,
        });

        expect(result).toBeDefined();
        expect(result.id).toEqual(FAKE_SESSION_ID);
        expect(mockSessionSave).not.toHaveBeenCalled();
      });

      it('should call added to session for nanny', async () => {
        await service.createSession({
          parent: FAKE_PARENT_ID,
          nanny: FAKE_NANNY_ID,
        });
        expect(mockAddedToSession).toHaveBeenCalledTimes(1);
      });

      describe('When no nanny participant', () => {
        beforeEach(() => {
          mockParticipantRepository.findOne.mockResolvedValue(null);
        });
        it('should create new session', async () => {
          const result = await service.createSession({
            parent: FAKE_PARENT_ID,
            nanny: FAKE_NANNY_ID,
          });

          expect(result).toBeDefined();
          expect(result.id).toEqual(FAKE_SESSION_ID);
          expect(mockSessionSave).toHaveBeenCalled();
        });
      });
    });
  });

  describe('Get token', () => {
    const FAKE_PARENT_ID = '1';
    const FAKE_NANNY_ID = '1';
    const FAKE_SESSION_ID = '1';
    it('should create OT token', async () => {
      mockParticipantRepository.findOne.mockResolvedValue({
        session: {
          opentokTokSessionId: 'fake OT session',
        },
      });

      const result = await service.getToken(FAKE_SESSION_ID, {
        role: 'parent',
        user: FAKE_PARENT_ID,
      });

      expect(result).toBeDefined();
    });
  });
  describe('Terminate session', () => {
    const FAKE_SESSION_ID = '1';
    const FAKE_OPENTOK_SESSION_ID = 'FAKE_OPENTOK_SESSION_ID';
    const FAKE_NANNY_ID = '1';
    const FAKE_PARENT_ID = '1';
    describe('when initiator exists', () => {
      const mockTerminated = jest.fn();
      const mockCommitEvents = jest.fn();
      beforeEach(() => {
        const FAKE_SESSION = {
          id: FAKE_SESSION_ID,
          openTokSessionId: FAKE_OPENTOK_SESSION_ID,
          createdAt: new Date(),
        };
        const FAKE_NANNY = {
          userId: FAKE_NANNY_ID,
          role: 'nanny',
          terminated: mockTerminated,
          commit: mockCommitEvents,
        };
        const FAKE_PARENT = {
          userId: FAKE_PARENT_ID,
          role: 'parent',
          terminated: mockTerminated,
          commit: mockCommitEvents,
        };
        mockParticipantRepository.findOne.mockResolvedValue({
          ...FAKE_NANNY,
          session: FAKE_SESSION,
        });
        mockParticipantRepository.find.mockResolvedValue([
          FAKE_NANNY,
          FAKE_PARENT,
        ]);
      });
      afterEach(() => {
        mockTerminated.mockReset();
        mockCommitEvents.mockReset();
      });
      it('should update records and commit transaction', async () => {
        await service.terminateSession(FAKE_SESSION_ID, {
          user: FAKE_NANNY_ID,
          role: 'nanny',
        });

        expect(mockParticipantSave).toBeCalledWith(
          expect.arrayContaining([
            expect.objectContaining({
              userId: FAKE_PARENT_ID,
              role: 'parent',
            }),
            expect.objectContaining({
              userId: FAKE_NANNY_ID,
              role: 'nanny',
            }),
          ]),
        );
        expect(mockTransactionCommit).toHaveBeenCalled();
      });

      it('should rollback when save failed', async () => {
        mockSessionSave.mockRejectedValue(true);

        await service.terminateSession(FAKE_SESSION_ID, {
          user: FAKE_NANNY_ID,
          role: 'nanny',
        });

        expect(mockTransactionCommit).not.toHaveBeenCalled();
      });

      it('should call terminated on participants', async () => {
        await service.terminateSession(FAKE_SESSION_ID, {
          user: FAKE_NANNY_ID,
          role: 'nanny',
        });
        expect(mockTerminated).toHaveBeenCalledTimes(2);
      });

      it('should commit events', async () => {
        await service.terminateSession(FAKE_SESSION_ID, {
          user: FAKE_NANNY_ID,
          role: 'nanny',
        });
        expect(mockCommitEvents).toHaveBeenCalledTimes(2);
      });

      it('should send signal to opentok clients', async () => {
        await service.terminateSession(FAKE_SESSION_ID, {
          user: FAKE_NANNY_ID,
          role: 'nanny',
        });

        expect(mockOpentokAdapter.sendTerminationSignal).toBeCalledWith(
          FAKE_OPENTOK_SESSION_ID,
        );
      });
    });
    describe('when initiator does not exists', () => {
      it('should not do anything ', async () => {
        mockParticipantRepository.findOne.mockResolvedValue(null);

        await service.terminateSession(FAKE_SESSION_ID, {
          user: FAKE_NANNY_ID,
          role: 'nanny',
        });

        expect(mockTransactionCommit).not.toHaveBeenCalled();
      });
    });
  });
});
