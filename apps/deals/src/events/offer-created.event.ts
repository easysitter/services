import { DealEvent } from './deal.event';

export class OfferCreatedEvent extends DealEvent {}
