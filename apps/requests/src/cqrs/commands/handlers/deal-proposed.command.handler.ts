import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { DealProposedCommand } from '../impl';
import { NannyRequest } from '../../../entities/nanny-request.entity';

@CommandHandler(DealProposedCommand)
export class DealProposedCommandHandler
  implements ICommandHandler<DealProposedCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    @InjectRepository(NannyRequest)
    private readonly nannyRequestRepo: Repository<NannyRequest>,
  ) {}

  async execute(command: DealProposedCommand): Promise<void> {
    const request = this.publisher.mergeObjectContext(command.request);
    request.dealProposed();
    await this.nannyRequestRepo.save(request);
    request.commit();
  }
}
