import { Type } from 'class-transformer';
import { IsArray, IsBoolean, IsDate, IsNotEmpty, IsNumber, IsOptional, IsPositive, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { IsTime } from '../validators/is-time';

class CreateRequestNanny {
  @IsNotEmpty()
  @IsPositive()
  @IsNumber()
  @ApiModelProperty({
    example: 1,
  })
  id: number;
  @IsNotEmpty()
  @IsPositive()
  @IsNumber()
  @ApiModelProperty({
    example: 10,
  })
  rate: number;
}

// tslint:disable-next-line:max-classes-per-file
export class CreateRequestDto {
  @IsPositive()
  @IsNumber()
  @IsNotEmpty()
  @ApiProperty({
    example: 1,
  })
  parent: number;

  @IsNotEmpty()
  @IsDate()
  @Type(to => Date)
  @ApiProperty({
    type: 'string',
    format: 'date',
  })
  date: Date;

  @IsNotEmpty()
  @IsTime()
  @ApiProperty({
    example: '9:00',
  })
  fromTime: string;

  @IsNotEmpty()
  @IsTime()
  @ApiProperty({
    example: '11:00',
  })
  toTime: string;

  @IsArray()
  @IsNotEmpty()
  @ApiProperty({
    type: Number,
    isArray: true,
    example: [1, 3],
  })
  children: number[];

  @IsArray()
  @IsNotEmpty()
  @ApiProperty({ type: CreateRequestNanny, isArray: true })
  @Type(() => CreateRequestNanny)
  nannies: CreateRequestNanny[];

  @IsBoolean()
  @IsNotEmpty()
  @ApiProperty({ type: Boolean })
  trial = false;

  @IsString()
  @IsOptional()
  @ApiProperty({
    example: '221 B, Backer St., London',
  })
  address: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    example: '00-000',
  })
  zipCode: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    example: '+123 456 78 90',
  })
  phone: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    example: 'Butler Did It',
  })
  comment: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    example: 'track-it',
  })
  trackId?: string;
}
