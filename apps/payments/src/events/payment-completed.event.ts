import { IEvent } from '@nestjs/cqrs';
import { Payment } from '../entities/payment';

export class PaymentCompletedEvent implements IEvent {
  constructor(public readonly payment: Payment) {

  }
}
