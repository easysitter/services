import { Repository } from 'typeorm';
import { Inject, Logger } from '@nestjs/common';
import { EventsHandler, IEventHandler, QueryBus } from '@nestjs/cqrs';
import { ClientProxy } from '@nestjs/microservices';
import { InjectRepository } from '@nestjs/typeorm';
import { NannyRequestStatusUpdated } from '@event-bus/contracts';
import { NannyRequestStatusUpdatedEvent } from '../impl/nanny-request-status-updated.event';
import { ParentRequest } from '../../../entities/parent-request.entity';
import { NannyRequest } from '../../../entities/nanny-request.entity';
import { ParentRequestByNannyRequestIdQuery } from '../../queries/impl';

@EventsHandler(NannyRequestStatusUpdatedEvent)
export class NannyRequestStatusUpdatedHandler implements IEventHandler<NannyRequestStatusUpdatedEvent> {

  constructor(
    @Inject('EVENT_BUS')
    private messages: ClientProxy,
    @InjectRepository(ParentRequest)
      parentRequestRepository: Repository<ParentRequest>,
    private logger: Logger,
    private queryBus: QueryBus,
  ) {
  }

  async handle(event: NannyRequestStatusUpdatedEvent): Promise<any> {
    const { request } = event;
    await this.populateParentRequest(request);
    try {
      const msg = new NannyRequestStatusUpdated({
        nanny: request.user,
        parent: request.parentRequest.user,
        request: request.parentRequest.id,
        status: request.status,
      });
      this.messages.emit(msg.name, msg.payload);
    } catch (e) {
      this.logger.error(e.message, e.stackTrace, 'NannyRequestStatusUpdatedHandler');
    }
  }

  private async populateParentRequest(request: NannyRequest): Promise<void> {
    let { parentRequest } = request;

    if (parentRequest instanceof ParentRequest) {
      return;
    }
    parentRequest = await this.queryBus.execute(new ParentRequestByNannyRequestIdQuery(request.id));
    request.parentRequest = parentRequest;
  }
}
