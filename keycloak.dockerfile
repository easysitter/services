FROM jboss/keycloak
WORKDIR /opt/jboss/keycloak/
COPY keycloak/standalone.xml ./standalone/configuration/standalone.xml
COPY keycloak/es-theme ./themes/es-theme
