import { CreateRequestDto } from '../../../dto/create-request.dto';
import { ICommand } from '@nestjs/cqrs';

export class CreateRequestCommand implements ICommand {
  constructor(public readonly data: CreateRequestDto) {}
}
