import { EventPublisher, IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { ParentRequestByNannyRequestIdQuery } from '../impl';

import { NannyRequest } from '../../../entities/nanny-request.entity';
import { ParentRequest } from '../../../entities/parent-request.entity';

@QueryHandler(ParentRequestByNannyRequestIdQuery)
export class ParentRequestByNannyRequestIdQueryHandler
  implements IQueryHandler<ParentRequestByNannyRequestIdQuery, ParentRequest> {
  constructor(
    private readonly publisher: EventPublisher,
    @InjectRepository(NannyRequest)
    private readonly nannyRequest: Repository<NannyRequest>,
  ) {
  }

  async execute(query: ParentRequestByNannyRequestIdQuery): Promise<ParentRequest> {
    const { id } = query;
    const nannyRequest = await this.nannyRequest.findOne(id, {
      relations: ['parentRequest'],
    });
    return nannyRequest.parentRequest;
  }
}
