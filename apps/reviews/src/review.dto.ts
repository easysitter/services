import { Transform } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

export class ReviewDto {
  @ApiProperty()
  id: string;
  @ApiProperty()
  rating: number;
  @ApiProperty()
  pros: string;
  @ApiProperty()
  cons: string;
  @Transform((v) => v instanceof Date ? v.toISOString() : v, { toPlainOnly: true })
  @ApiProperty()
  createdAt: string;
}
