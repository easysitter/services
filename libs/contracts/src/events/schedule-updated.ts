import { EventBusEvent } from '../event-bus-event';

interface ScheduleUpdatedPayload {
  user: number;
  items: Array<{ day: number, from: string, to: string }>;
}

export class ScheduleUpdated implements EventBusEvent<ScheduleUpdatedPayload> {
  static readonly eventName = 'schedule_updated';

  public readonly name = ScheduleUpdated.eventName;

  constructor(
    public readonly payload: ScheduleUpdatedPayload,
  ) {
  }
}
