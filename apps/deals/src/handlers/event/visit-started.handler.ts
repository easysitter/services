import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { VisitStartedEvent } from '../../events/visit-started.event';
import { VisitStarted } from '@event-bus/contracts';
import { Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@EventsHandler(VisitStartedEvent)
export class VisitStartedHandler implements IEventHandler<VisitStartedEvent> {

  constructor(
    @Inject('EVENT_BUS')
    private channel: ClientProxy,
  ) {
  }

  handle(event: VisitStartedEvent): any {
    const { deal } = event;

    const msg = new VisitStarted({
      ...deal,
    });
    this.channel.emit(msg.name, msg.payload);
  }
}
