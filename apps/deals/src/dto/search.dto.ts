import { IsIn, IsNumber, IsOptional, IsString } from 'class-validator';
import { Transform, Type } from 'class-transformer';
import { ApiExtraModels, ApiProperty } from '@nestjs/swagger';

export class SearchDto {
  @ApiProperty({required: false})
  @IsOptional()
  @IsString()
  request?: string;

  @IsOptional()
  @ApiProperty({required: false})
  @IsNumber()
  @Type(to => Number)
  parent?: number;

  @IsOptional()
  @ApiProperty({required: false})
  @IsNumber()
  @Type(to => Number)
  nanny?: number;

  @IsOptional()
  @IsIn(['offer', 'deal'])
  @ApiProperty({required: false, enum: ['offer', 'deal']})
  state?: 'offer' | 'deal';

  @ApiProperty({required: false})
  @IsOptional()
  @Transform((v) => v === 1 || v === '1' || v === true || v === 'true')
  isActive: boolean;
}
