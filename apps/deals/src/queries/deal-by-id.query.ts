import { IQuery } from '@nestjs/cqrs';
import { Deal } from '../models';

export class DealByIdQuery implements IQuery {
  constructor(public readonly id: Deal['id']) {}
}
