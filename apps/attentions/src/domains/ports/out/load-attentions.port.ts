import { AttentionEntity } from '../../entities/attention.entity';
import { LoadAttentionsQuery } from './load-attentions.query';

export interface LoadAttentionsPort {
  loadAttentions(query: LoadAttentionsQuery): Promise<AttentionEntity[]>;
}
