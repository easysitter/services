import { Controller, Get, ParseIntPipe, Post, Query } from '@nestjs/common';
import { DealPaymentService } from './deal-payment.service';
import { OutdatedDealsQuery } from './queries/outdated-deals.query';
import { CancelDealPaymentsCommand } from './commands/cancel-deal-payments.command';
import { ApiQuery } from '@nestjs/swagger';

@Controller('/system/deals')
export class SystemDealsController {
  constructor(
    private dealPaymentService: DealPaymentService,
  ) {
  }

  @Get('/outdated')
  @ApiQuery({
    name: 'period',
    type: Number,
    allowEmptyValue: true,
    example: 15,
  })
  getOutdated(
    @Query('period', ParseIntPipe) period = 15,
  ) {
    return this.dealPaymentService.getOutdated(OutdatedDealsQuery.of(period));
  }

  @Post('/outdated')
  @ApiQuery({
    name: 'period',
    type: Number,
    allowEmptyValue: true,
    example: 15,
  })
  async closeOutdated(
    @Query('period', ParseIntPipe) period = 15,
  ) {
    const payments = await this.dealPaymentService.getOutdated(OutdatedDealsQuery.of(period));
    await this.dealPaymentService.cancelOutdated(CancelDealPaymentsCommand.of(payments));
  }
}
