import { AttentionTargetEntity } from '../../entities/attention-target.entity';

export interface LoadOutdatedRequestToRemindPort {
  load(): Promise<AttentionTargetEntity[]>;
}
