import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { RemoveParentRequestsCommand } from '../impl/remove-parent-requests.command';
import { InjectRepository } from '@nestjs/typeorm';
import { ParentRequest } from '../../../entities/parent-request.entity';
import { Repository } from 'typeorm';

@CommandHandler(RemoveParentRequestsCommand)
export class RemoveParentRequestsCommandHandler
  implements ICommandHandler<RemoveParentRequestsCommand> {
  constructor(
    @InjectRepository(ParentRequest)
    private readonly parentRequest: Repository<ParentRequest>,
  ) {}

  async execute(command: RemoveParentRequestsCommand): Promise<void> {
    await this.parentRequest.delete({
      user: command.parent,
    });
  }
}
