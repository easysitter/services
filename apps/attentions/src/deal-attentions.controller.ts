import { CreateDealRemindersCommand, CreateDealRemindersUseCase } from './domains';
import { Controller, Get, Post } from '@nestjs/common';
import { FindAttentionsByTargetTypeUseCase } from './domains/ports/in/find-attentions-by-target-type.use-case';

@Controller('/deals')
export class DealAttentionsController {
  constructor(
    private createDealsReminder: CreateDealRemindersUseCase,
    private findAttentionsByTargetType: FindAttentionsByTargetTypeUseCase,
  ) {
  }

  @Post('/reminding')
  async createDealsReminding() {
    const command = new CreateDealRemindersCommand();
    const result = await this.createDealsReminder.createReminders(command);
    return { message: 'Create reminders', result };
  }

  @Get('')
  async getDealAttentions() {
    return this.findAttentionsByTargetType.find('deal');
  }
}
