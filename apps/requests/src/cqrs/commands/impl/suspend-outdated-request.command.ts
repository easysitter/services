import { ICommand } from '@nestjs/cqrs';
import { ParentRequest } from '../../../entities/parent-request.entity';

export class SuspendOutdatedRequestCommand implements ICommand {
  constructor(public readonly request: ParentRequest) {
  }
}
