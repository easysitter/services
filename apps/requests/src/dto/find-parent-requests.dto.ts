import { Transform } from 'class-transformer';
import { IsEnum, IsOptional, IsPositive } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { ParentRequest } from '../entities/parent-request.entity';

export class FindParentRequestsDto {
  @Transform(v => v === '1' || v === 'true')
  @IsOptional()
  @IsEnum(['0', '1', 'true', 'false', true, false])
  @ApiProperty({
    required: false,
  })
  isActive?: boolean;

  @IsOptional()
  @ApiProperty({
    type: String,
    required: false,
  })
  sortBy: keyof ParentRequest = 'id';

  @IsOptional()
  @IsEnum(['ASC', 'DESC'])
  @ApiProperty({
    required: false,
  })
  sortDirection: 'ASC' | 'DESC' = 'DESC';

  @IsPositive()
  @IsOptional()
  @ApiProperty({
    required: false,
  })
  @Transform((v) => Number(v))
  limit: number;
}
