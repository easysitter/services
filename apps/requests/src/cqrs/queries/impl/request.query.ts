import { ParentRequest } from '../../../entities/parent-request.entity';

export class RequestQuery {
  constructor(public readonly id: ParentRequest['id']) {}
}
