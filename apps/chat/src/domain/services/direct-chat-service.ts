import { v4 as uuid } from 'uuid';
import { Observable, of, throwError } from 'rxjs';
import { mapTo, switchMap } from 'rxjs/operators';

import { ChatModel } from '../models';
import { CreateDirectChatCommand, DirectChatUseCase } from '../ports/primary';
import { ChatNotificationsPort, ChatsPort } from '../ports/secondary';

export class DirectChatService implements DirectChatUseCase {
  constructor(
    private readonly chatsPort: ChatsPort,
    private readonly notificationsPort: ChatNotificationsPort,
  ) {
  }

  createDirectChat(command: CreateDirectChatCommand): Observable<ChatModel> {
    return this.findDirectChat(command.users).pipe(
      switchMap((chat) => {
        if (chat) {
          return of(chat);
        }
        const model = new ChatModel(
          uuid(),
          command.users,
          { type: 'direct' },
        );
        return this.chatsPort.save(model).pipe(
          switchMap(result => {
            if (!result) {
              return throwError(new Error('failed to create chat'));
            } else {
              return this.notificationsPort.notifyNewChat(model).pipe(mapTo(model));
            }
          }),
        );
      }),
    );
  }

  findDirectChat(users: string[]): Observable<ChatModel> {
    return this.chatsPort.loadByMetaForUsers(users, {
      type: 'direct',
    });
  }

  findDirectChatListForUser(user: string): Observable<ChatModel[]> {
    return this.chatsPort.loadAllByMetaForUser(user, {
      type: 'direct',
    });
  }
}
