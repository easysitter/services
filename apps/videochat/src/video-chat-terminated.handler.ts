import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { ClientProxy } from '@nestjs/microservices';
import { Inject } from '@nestjs/common';
import { VideoChatTerminated } from './video-chat-terminated';
import { VideochatParticipantTerminated } from '@event-bus/contracts';

@EventsHandler(VideoChatTerminated)
export class VideoChatTerminatedHandler
  implements IEventHandler<VideoChatTerminated> {
  constructor(
    @Inject('EVENT_BUS')
    private readonly eventBus: ClientProxy,
  ) {}

  async handle({ participant }: VideoChatTerminated): Promise<void> {
    const { role, userId, sessionId } = participant;
    const message = new VideochatParticipantTerminated({
      role,
      userId,
      sessionId,
    });
    this.eventBus.emit(message.channel, {
      key: message.key,
      value: message.payload,
    });
  }
}
