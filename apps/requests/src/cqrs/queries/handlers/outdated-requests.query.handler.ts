import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { OutdatedRequestsQuery } from '../impl';
import { ParentRequest } from '../../../entities/parent-request.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@QueryHandler(OutdatedRequestsQuery)
export class OutdatedRequestsQueryHandler implements IQueryHandler<OutdatedRequestsQuery, ParentRequest[]> {
  constructor(
    @InjectRepository(ParentRequest)
    private repo: Repository<ParentRequest>,
  ) {
  }

  async execute(query: OutdatedRequestsQuery): Promise<ParentRequest[]> {
    return this.repo.createQueryBuilder()
      .where(`"isActive" = true`)
      .andWhere(
        `"startTs" - INTERVAL '${query.interval} minutes' < CURRENT_TIMESTAMP + INTERVAL '2 hours'`,
      )
      .getMany();
  }
}
