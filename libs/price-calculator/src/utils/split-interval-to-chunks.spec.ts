import { splitIntervalToChunks } from './split-interval-to-chunks';
import { isEqual, parseISO } from 'date-fns';

describe('Interval chunks', () => {

  const sixAm = parseISO('2020-09-01T06:00:00+02');
  const eightAm = parseISO('2020-09-01T08:00:00+02');
  const eightPm = parseISO('2020-09-01T20:00:00+02');
  const tenPm = parseISO('2020-09-01T22:00:00+02');
  const sixAmNextDay = parseISO('2020-09-02T06:00:00+02');
  const eightAmNextDay = parseISO('2020-09-02T08:00:00+02');

  const dayBoundary = parseISO('2020-09-01T07:00:00+02');
  const nightBoundary = parseISO('2020-09-01T21:00:00+02');
  const nextDayBoundary = parseISO('2020-09-02T07:00:00+02');

  it(`should have 1 chunk for ${eightAm.toTimeString()} to ${eightPm.toTimeString()}`, () => {
    const chunks = splitIntervalToChunks(eightAm, eightPm);
    expect(chunks).toHaveLength(1);
    const [chunk] = chunks;
    expect(chunk.intervalType).toBe('day');
    expect(isEqual(eightAm, chunk.from)).toBeTruthy();
    expect(isEqual(eightPm, chunk.to)).toBeTruthy();
  });
  it(`should have 1 chunk for ${tenPm.toTimeString()} to ${sixAmNextDay.toTimeString()}`, () => {
    const chunks = splitIntervalToChunks(tenPm, sixAmNextDay);
    expect(chunks).toHaveLength(1);
    const [chunk] = chunks;
    expect(chunk.intervalType).toBe('night');
    expect(isEqual(tenPm, chunk.from)).toBeTruthy();
    expect(isEqual(sixAmNextDay, chunk.to)).toBeTruthy();
  });
  it(`should have 2 chunk for ${sixAm.toTimeString()} to ${eightAm.toTimeString()}`, () => {
    const chunks = splitIntervalToChunks(sixAm, eightAm);
    expect(chunks).toHaveLength(2);
    const [chunk0, chunk1] = chunks;

    expect(chunk0.intervalType).toBe('night');
    expect(isEqual(sixAm, chunk0.from)).toBeTruthy();
    expect(isEqual(dayBoundary, chunk0.to)).toBeTruthy();

    expect(chunk1.intervalType).toBe('day');
    expect(isEqual(dayBoundary, chunk1.from)).toBeTruthy();
    expect(isEqual(eightAm, chunk1.to)).toBeTruthy();
  });
  it(`should have 2 chunk for ${tenPm.toTimeString()} to ${eightAmNextDay.toTimeString()}`, () => {
    const chunks = splitIntervalToChunks(tenPm, eightAmNextDay);
    expect(chunks).toHaveLength(2);
    const [chunk0, chunk1] = chunks;

    expect(chunk0.intervalType).toBe('night');
    expect(isEqual(tenPm, chunk0.from)).toBeTruthy();
    expect(isEqual(nextDayBoundary, chunk0.to)).toBeTruthy();

    expect(chunk1.intervalType).toBe('day');
    expect(isEqual(nextDayBoundary, chunk1.from)).toBeTruthy();
    expect(isEqual(eightAmNextDay, chunk1.to)).toBeTruthy();
  });
  it(`should have 2 chunk for ${eightPm.toTimeString()} to ${tenPm.toTimeString()}`, () => {
    const chunks = splitIntervalToChunks(eightPm, tenPm);
    expect(chunks).toHaveLength(2);
    const [chunk0, chunk1] = chunks;

    expect(chunk0.intervalType).toBe('day');
    expect(isEqual(eightPm, chunk0.from)).toBeTruthy();
    expect(isEqual(nightBoundary, chunk0.to)).toBeTruthy();

    expect(chunk1.intervalType).toBe('night');
    expect(isEqual(nightBoundary, chunk1.from)).toBeTruthy();
    expect(isEqual(tenPm, chunk1.to)).toBeTruthy();
  });

  it(`should have 3 chunks for ${sixAm.toTimeString()} to ${tenPm.toTimeString()}`, () => {
    const chunks = splitIntervalToChunks(sixAm, tenPm);
    expect(chunks).toHaveLength(3);
    const [chunk0, chunk1, chunk2] = chunks;

    expect(chunk0.intervalType).toBe('night');
    expect(isEqual(sixAm, chunk0.from)).toBeTruthy();
    expect(isEqual(dayBoundary, chunk0.to)).toBeTruthy();

    expect(chunk1.intervalType).toBe('day');
    expect(isEqual(dayBoundary, chunk1.from)).toBeTruthy();
    expect(isEqual(nightBoundary, chunk1.to)).toBeTruthy();

    expect(chunk2.intervalType).toBe('night');
    expect(isEqual(nightBoundary, chunk2.from)).toBeTruthy();
    expect(isEqual(tenPm, chunk2.to)).toBeTruthy();
  });

  it(`should have 3 chunks for ${eightPm.toTimeString()} to ${eightAmNextDay.toTimeString()}`, () => {
    const chunks = splitIntervalToChunks(eightPm, eightAmNextDay);
    expect(chunks).toHaveLength(3);
    const [chunk0, chunk1, chunk2] = chunks;

    expect(chunk0.intervalType).toBe('day');
    expect(isEqual(eightPm, chunk0.from)).toBeTruthy();
    expect(isEqual(nightBoundary, chunk0.to)).toBeTruthy();

    expect(chunk1.intervalType).toBe('night');
    expect(isEqual(nightBoundary, chunk1.from)).toBeTruthy();
    expect(isEqual(nextDayBoundary, chunk1.to)).toBeTruthy();

    expect(chunk2.intervalType).toBe('day');
    expect(isEqual(nextDayBoundary, chunk2.from)).toBeTruthy();
    expect(isEqual(eightAmNextDay, chunk2.to)).toBeTruthy();
  });

  it(`should have 5 chunk for 01.01 00:00 to 03.01 00:00`, () => {
    const chunks = splitIntervalToChunks(parseISO('2020-09-01T00:00:00Z'), parseISO('2020-09-03T00:00:00Z'));
    expect(chunks).toHaveLength(5);
    expect(chunks[0].intervalType).toBe('night');
    expect(chunks[1].intervalType).toBe('day');
    expect(chunks[2].intervalType).toBe('night');
    expect(chunks[3].intervalType).toBe('day');
    expect(chunks[4].intervalType).toBe('night');
  });
});
