import { HttpService, Inject, Injectable } from '@nestjs/common';
import { AttentionTargetEntity } from '../domains';
import { OUTDATED_REQUEST_URL } from '../injection.tokens';
import { map } from 'rxjs/operators';
import { LoadOutdatedRequestToRemindPort } from '../domains/ports/out/load-outdated-request-to-remind.port';

@Injectable()
export class OutdatedRequestService implements LoadOutdatedRequestToRemindPort {
  constructor(
    private http: HttpService,
    @Inject(OUTDATED_REQUEST_URL)
    private outdatedRequestUrl: string,
  ) {
  }

  async load(): Promise<AttentionTargetEntity[]> {
    const requests = await this.http.get<Array<{ id, parent, nanny, date, fromTime, toTime }>>(
      this.outdatedRequestUrl,
    ).pipe(
      map(response => response.data),
    ).toPromise();
    // const deals = [
    //   { id: 1, parent: 1, nanny: 1, date: new Date(), fromTime: '9:00', toTime: '11:00' },
    //   { id: 2, parent: 1, nanny: 1, date: new Date(), fromTime: '12:00', toTime: '15:00' },
    //   { id: 3, parent: 1, nanny: 1, date: new Date(), fromTime: '19:00', toTime: '21:00' },
    // ];
    return requests.map(({ id, ...data }) => AttentionTargetEntity.of('outdated-request', `${id}`, data));
  }
}
