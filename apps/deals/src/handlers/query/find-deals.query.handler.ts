import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { FindDealsQuery } from '../../queries';
import { Deal } from '../../models';
import { DealService } from '../../deal.service';

@QueryHandler(FindDealsQuery)
export class FindDealsQueryHandler
  implements IQueryHandler<FindDealsQuery, Deal[]> {
  constructor(private readonly service: DealService) {}

  execute(query: FindDealsQuery): Promise<Deal[]> {
    return this.service.find(query.params);
  }
}
