import { AttentionTargetEntity } from '../../entities/attention-target.entity';

export interface LoadDealsToRemindPort {
  load(): Promise<AttentionTargetEntity[]>;
}
