import { ApiProperty } from '@nestjs/swagger';
import { Expose, Type } from 'class-transformer';

export class MarkMessagesSeenHttpDto {

  @ApiProperty()
  @Expose()
  user: string;

  @ApiProperty({type: String, format: 'date'})
  @Expose()
  @Type(() => Date)
  lastSeenMessageDate: Date;
}
