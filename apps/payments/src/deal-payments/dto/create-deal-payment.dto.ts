import { ApiProperty } from '@nestjs/swagger';

export class CreateDealPaymentDto {
  @ApiProperty()
  deal: string;
  @ApiProperty()
  cost: number;
}
