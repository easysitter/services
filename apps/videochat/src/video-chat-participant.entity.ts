import {
  AfterInsert,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { VideoChatSessionEntity } from './video-chat-session.entity';
import { AggregateRoot } from '@nestjs/cqrs';
import { VideoChatParticipantAdded } from './video-chat-participant-added';
import { VideoChatTerminated } from './video-chat-terminated';

@Entity('participant')
export class VideoChatParticipantEntity extends AggregateRoot {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    type: 'enum',
    enum: ['nanny', 'parent'],
  })
  role: 'nanny' | 'parent';

  @Column()
  userId: string;

  @Column()
  sessionId: string;

  @ManyToOne(type => VideoChatSessionEntity, {})
  @JoinColumn({ name: 'sessionId', referencedColumnName: 'id' })
  session: VideoChatSessionEntity;

  @CreateDateColumn()
  createdAt: Date;

  @Column({ nullable: true })
  terminatedAt?: Date;

  addedToSession() {
    this.apply(new VideoChatParticipantAdded(this));
  }

  terminated() {
    this.terminatedAt = new Date();
    this.apply(new VideoChatTerminated(this));
  }

  @AfterInsert()
  afterCreate() {
    this.addedToSession();
  }
}
