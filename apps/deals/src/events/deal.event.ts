import { IEvent } from '@nestjs/cqrs';
import { Deal } from '../models';

export abstract class DealEvent implements IEvent {
  constructor(public readonly deal: Deal) {}
}
