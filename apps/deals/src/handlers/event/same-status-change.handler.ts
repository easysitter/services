import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { SameStatusChangeEvent } from '../../events';
import { Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@EventsHandler(SameStatusChangeEvent)
export class SameStatusChangeHandler implements IEventHandler<SameStatusChangeEvent> {

  constructor(
    @Inject('EVENT_BUS')
    private channel: ClientProxy,
  ) {
  }

  handle(event: SameStatusChangeEvent): any {
    const { deal } = event;
    this.channel.emit('exceptions', {
      key: 'deal-same-status',
      value: {
        id: deal.id,
        parent: deal.parent,
        nanny: deal.nanny,
        status: deal.state,
        request: deal.requestId,
        type: 'deal-same-status',
      },
    });
  }
}
