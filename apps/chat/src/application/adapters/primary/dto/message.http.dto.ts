import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';

export class MessageHttpDto {
  @ApiProperty({ required: false })
  id: string;

  @ApiProperty()
  @Expose()
  user: string;

  @ApiProperty()
  @Expose()
  text: string;

  @ApiProperty({ type: String, format: 'dateTime' })
  @Expose()
  createdAt: Date;

  @ApiProperty({ type: String, isArray: true })
  viewedBy: string[];
}
