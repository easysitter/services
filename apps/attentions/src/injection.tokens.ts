export const LOAD_DEALS_TO_REMIND_ADAPTER = Symbol();
export const LOAD_OUTDATED_REQUEST_TO_REMIND_ADAPTER = Symbol();
export const SAVE_ATTENTIONS_ADAPTER = Symbol();
export const LOAD_ATTENTIONS_ADAPTER = Symbol();
export const NOTIFY_NEW_ATTENTIONS_ADAPTER = Symbol();
export const COMING_DEALS_URL = Symbol();
export const OUTDATED_REQUEST_URL = Symbol();
export const EVENT_BUS = Symbol();
