import { EventPattern } from '@nestjs/microservices';
import { Body, Controller, Get, Logger, Post, ValidationPipe } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { DealConcluded, OfferCancelled, OfferCreated } from '@event-bus/contracts';

import { CreateRequestDto } from '../dto/create-request.dto';
import {
  CreateRequestCommand,
  DealProposedCommand,
  OfferAcceptedCommand,
  OfferRefusedCommand,
  SuspendOutdatedRequestCommand,
} from '../cqrs/commands';

import { NannyRequestQuery, OutdatedRequestsQuery } from '../cqrs/queries';

import { NannyRequest } from '../entities/nanny-request.entity';
import { ApiTags } from '@nestjs/swagger';
import { ParentRequest } from '../entities/parent-request.entity';

@Controller('/requests')
@ApiTags('common')
export class CommonRequestController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
    private readonly logger: Logger,
  ) {
  }

  @Post()
  create(@Body(ValidationPipe) data: CreateRequestDto) {
    return this.commandBus.execute(new CreateRequestCommand(data));
  }

  @Get('/outdated')
  getOutdated() {
    return this.queryBus.execute(new OutdatedRequestsQuery());
  }

  @Get('/coming-outdated')
  getComingOutdated() {
    return this.queryBus.execute(new OutdatedRequestsQuery(15));
  }

  @Post('/outdated/suspend')
  async suspendOutdated() {
    const requests = await this.queryBus.execute<OutdatedRequestsQuery, ParentRequest[]>(new OutdatedRequestsQuery());
    const promises = requests.map(async request => {
      try {
        await this.commandBus.execute(new SuspendOutdatedRequestCommand(request));
        return;
      } catch (e) {
        return;
      }
    });
    await Promise.all(promises);
  }

  @EventPattern(OfferCreated.eventName)
  async onOfferCreated({ value }: { value: OfferCreated['payload'] }) {
    this.logger.log({
      type: OfferCreated.eventName,
      value,
    });
    const request: NannyRequest = await this.queryBus.execute(
      new NannyRequestQuery(+value.request, value.nanny),
    );
    try {
      await this.commandBus.execute(new DealProposedCommand(request));
      this.logger.log({
        type: OfferCreated.eventName,
        value,
        status: 'done',
      });
    } catch (e) {
      this.logger.error({
        type: OfferCreated.eventName,
        status: 'error',
        value,
        error: e,
      });
    }
  }

  @EventPattern(OfferCancelled.eventName)
  async onOfferCancelled({ value }: { value: OfferCancelled['payload'] }) {
    this.logger.log({
      type: OfferCancelled.eventName,
      value,
    });
    const request: NannyRequest = await this.queryBus.execute(
      new NannyRequestQuery(+value.request, value.nanny),
    );
    try {
      await this.commandBus.execute(new OfferRefusedCommand(request));
      this.logger.log({
        type: OfferCancelled.eventName,
        value,
        status: 'done',
      });
    } catch (e) {
      this.logger.error({
        type: OfferCancelled.eventName,
        value,
        error: e,
        status: 'error',
      });
    }
  }

  @EventPattern(DealConcluded.eventName)
  async onDealConcluded({ value }: { value: DealConcluded['payload'] }) {
    this.logger.log({
      type: DealConcluded.eventName,
      value,
    });
    const request: NannyRequest = await this.queryBus.execute(
      new NannyRequestQuery(+value.request, value.nanny),
    );
    try {
      await this.commandBus.execute(new OfferAcceptedCommand(request));
      this.logger.log({
        type: DealConcluded.eventName,
        value,
        status: 'done',
      });
    } catch (e) {
      this.logger.error({
        type: DealConcluded.eventName,
        value,
        error: e,
        status: 'error',
      });
    }
  }
}
