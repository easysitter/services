import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { ServiceSubscriptionService } from '../services/service-subscription.service';
import { CreateSubscriptionDto } from '../dto/create-subscription.dto';

@Controller('/subscriptions')
export class SubscriptionController {

  constructor(
    private subscriptions: ServiceSubscriptionService,
  ) {
  }

  @Post()
  async create(
    @Body() data: CreateSubscriptionDto,
  ) {
    return await this.subscriptions.createSubscription({
      userId: data.userId,
      returnUrl: data.returnUrl,
    });
  }

  @Get('users/:userId')
  getByUser(
    @Param('userId') userId: string,
  ) {
    return this.subscriptions.getActiveSubscription(userId);
  }
}
