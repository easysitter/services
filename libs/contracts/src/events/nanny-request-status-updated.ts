import { EventBusEvent } from '@event-bus/contracts';

interface NannyRequestStatusUpdatedPayload {
  request: number;
  nanny: number;
  parent: number;
  status: string;
}

export class NannyRequestStatusUpdated implements EventBusEvent<NannyRequestStatusUpdatedPayload> {
  static readonly eventName = 'nrequest_status_updated';
  readonly name = NannyRequestStatusUpdated.eventName;

  constructor(public readonly payload: NannyRequestStatusUpdatedPayload) {
  }
}
