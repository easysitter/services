import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ParentProfile, ParentUser } from './entities';

@Injectable()
export class ParentProfileService {
  constructor(
    @InjectRepository(ParentProfile)
    private parentProfileRepository: Repository<ParentProfile>,
    @InjectRepository(ParentUser)
    private parentUserRepository: Repository<ParentUser>,
  ) {
  }

  async create(userId, profileData: Omit<ParentProfile, 'id'>): Promise<ParentProfile> {
    let user = await this.findByUser(userId);
    if (user) {
      return user.profile;
    }
    user = this.parentUserRepository.create({
      userId,
      profile: this.parentProfileRepository.create(profileData),
    });
    await this.parentUserRepository.save(user);
    return user.profile;
  }

  async findByUser(userId: string): Promise<ParentUser> {
    return await this.parentUserRepository.findOne({ userId });
  }

  findById(id: ParentProfile['id']) {
    return this.parentProfileRepository.findOne({
      id,
    });
  }

  save(profileData: ParentProfile) {
    return this.parentProfileRepository.save(profileData);
  }
}
