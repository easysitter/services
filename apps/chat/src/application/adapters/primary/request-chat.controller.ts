import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { ChatHttpDto } from './dto/chat.http.dto';
import { SearchForRequestHttpDto } from './dto/search-for-request.http.dto';
import { map } from 'rxjs/operators';
import { plainToClass } from 'class-transformer';
import { CreateChatForRequestCommand, RequestChatUseCase } from '../../../domain/ports/primary';
import { EventPattern } from '@nestjs/microservices';
import { RequestAccepted } from '@event-bus/contracts';

@Controller('/chat')
@ApiTags('chat', 'request')
export class RequestChatController {

  constructor(
    private requestChat: RequestChatUseCase,
  ) {
  }

  @Post('/search-for-request')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({
    summary: 'Найти чат по запросу',
  })
  @ApiOkResponse({ type: ChatHttpDto })
  async searchForRequest(
    @Body() searchParams: SearchForRequestHttpDto,
  ) {
    return this.requestChat.findChatForRequest([searchParams.nanny], searchParams.request).pipe(
      map(chat => chat && plainToClass(ChatHttpDto, chat, {
        strategy: 'exposeAll',
        excludeExtraneousValues: true,
      })),
    );
  }

  @EventPattern(RequestAccepted.eventName)
  onRequestAccepted(
    { value }: { value: RequestAccepted['payload'] },
  ) {
    return this.requestChat.createChatForRequest(
      new CreateChatForRequestCommand(
        [`p${value.parent}`, `n${value.nanny}`],
        value.request,
      ),
    );
  }
}
