import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { AggregateRoot } from '@nestjs/cqrs';
import { PaymentCompletedEvent } from '../events/payment-completed.event';
import { PaymentCancelledEvent } from '../events/payment-cancelled.event';

@Entity()
export class Payment extends AggregateRoot {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  extId: string;

  @Column({
    type: 'text',
    nullable: true,
    transformer: {
      from(value: string): any {
        return JSON.parse(value);
      },
      to(value: any): any {
        return JSON.stringify(value);
      },
    },
  })
  meta: any;

  @Column({ type: 'numeric' })
  amount: number;

  @Column()
  currency: string;

  @Column()
  isCompleted: boolean;

  @Column({ default: true })
  isActive: boolean;

  @Column({ nullable: true })
  completedAt: Date;

  @Column({ nullable: true })
  cancelledAt: Date;

  @Column()
  type: string;

  @CreateDateColumn()
  createdAt: Date;

  complete() {
    if (!this.isActive) {
      return;
    }
    if (this.isCompleted) {
      return;
    }
    this.isCompleted = true;
    this.completedAt = new Date();
    this.isActive = false;
    this.apply(new PaymentCompletedEvent(this));
  }

  cancel() {
    if (!this.isActive) {
      return;
    }
    this.isActive = false;
    this.cancelledAt = new Date();
    this.apply(new PaymentCancelledEvent(this));
  }
}
