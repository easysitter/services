import { Body, Controller, Get, Param, ParseIntPipe, Put } from '@nestjs/common';
import { Repository } from 'typeorm';
import { NannyProfile } from './nanny-profile.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { NannyUser } from './nanny-user.entity';
import { EventBus } from '@nestjs/cqrs';
import { ProfileUpdatedEvent } from './events/profile-updated.event';

@Controller('')
export class NannyProfileController {

  constructor(
    @InjectRepository(NannyProfile)
    private nannyProfileRepository: Repository<NannyProfile>,
    @InjectRepository(NannyUser)
    private nannyUserRepository: Repository<NannyUser>,
    private publisher: EventBus,
  ) {
  }

  @Get('profile/:profileId')
  async getProfile(
    @Param('profileId', ParseIntPipe)
      id: number,
  ) {
    return await this.nannyProfileRepository.findOne({ id });
  }

  @Get(':userId')
  async getProfileByUser(
    @Param('userId')
      userId: string,
  ) {
    const user = await this.nannyUserRepository.findOne({ userId });
    return user && { ...user.profile, documents: user.profile.documents || [] };
  }

  @Put(':userId')
  async updateProfile(
    @Param('userId')
      userId: string,
    @Body() profile,
  ) {
    let user = await this.nannyUserRepository.findOne({ userId });
    if (!user) {
      user = this.nannyUserRepository.create({
        userId,
        profile: this.nannyProfileRepository.create(profile as NannyProfile),
      });
    } else {
      this.nannyProfileRepository.merge(user.profile, profile);
    }
    await this.nannyUserRepository.save(user);
    this.publisher.publish(new ProfileUpdatedEvent(user.profile));
    return user.profile;
  }
}
