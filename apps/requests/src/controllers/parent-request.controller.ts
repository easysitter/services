import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, ParseIntPipe, Put, Query, UsePipes, ValidationPipe } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { NannyRequestQuery, ParentRequestsQuery, RequestQuery } from '../cqrs/queries/impl';
import { ParentRequest } from '../entities/parent-request.entity';
import { NannyRequest } from '../entities/nanny-request.entity';
import { CancelNannyRequestCommand, CancelRequestCommand } from '../cqrs/commands/impl';
import { ApiParam, ApiTags } from '@nestjs/swagger';
import { FindParentRequestsDto } from '../dto/find-parent-requests.dto';

@Controller('parents/:parent/requests')
@ApiTags('parent')
export class ParentRequestController {
  private parentCommandMap = {
    cancel: CancelRequestCommand,
    cancelNanny: CancelNannyRequestCommand,
  };

  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {
  }

  @Get('')
  @UsePipes(new ValidationPipe({
    transform: true,
  }))
  @ApiParam({ name: 'parent', required: true, type: Number })
  getRequests(
    @Param('parent', ParseIntPipe)
      parent: ParentRequest['user'],
    @Query()
      { sortBy, sortDirection, ...filter }: FindParentRequestsDto,
  ) {
    return this.queryBus.execute(
      new ParentRequestsQuery({
        filter: {
          ...filter,
          user: parent,
        },
        sort: {
          sortBy, sortDirection,
        },
      }),
    );
  }

  @Get(':request')
  @ApiParam({ name: 'parent', required: true, type: Number })
  @ApiParam({ name: 'request', required: true, type: Number })
  getRequest(
    @Param('request', ParseIntPipe) requestId: ParentRequest['id'],
  ) {
    return this.queryBus.execute(
      new RequestQuery(requestId),
    );
  }

  @Put(':request/cancel')
  @ApiParam({ name: 'parent', required: true, type: Number })
  @ApiParam({ name: 'request', required: true, type: Number })
  async cancelNanny(
    @Body('nanny', ParseIntPipe) nanny: NannyRequest['user'],
    @Param('request') requestId: ParentRequest['id'],
  ) {
    const request: NannyRequest = await this.queryBus.execute(
      new NannyRequestQuery(requestId, nanny),
    );
    await this.commandBus.execute(
      new this.parentCommandMap.cancelNanny(request),
    );
    return request;
  }

  @Delete(':request')
  @ApiParam({ name: 'parent', required: true, type: Number })
  @ApiParam({ name: 'request', required: true, type: Number })
  @HttpCode(HttpStatus.NO_CONTENT)
  async cancel(@Param('request', ParseIntPipe) requestId: ParentRequest['id']) {
    const request: ParentRequest = await this.queryBus.execute(
      new RequestQuery(requestId),
    );
    await this.commandBus.execute(new this.parentCommandMap.cancel(request));
    return;
  }
}
