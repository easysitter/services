import { NannyRequest } from '../../../entities/nanny-request.entity';
import { IEvent } from '@nestjs/cqrs';

export class RequestAcceptedEvent implements IEvent {
  constructor(public readonly request: NannyRequest) {}
}
