import { EventBusEvent } from '@event-bus/contracts';

interface ChatMessageDto {
  chat: {
    id: string,
    users: string[],
    meta: Record<string, any>,
  };
  message: {
    id: string,
    user: string,
    text: string,
    createdAt: number | Date,
  };
}

export class ChatMessageSent implements EventBusEvent<{ key: string, value: ChatMessageDto }> {
  static readonly eventName = 'chat';

  readonly name = ChatMessageSent.eventName;
  public readonly payload;

  constructor(
    payload: ChatMessageDto,
  ) {
    this.payload = { key: 'message', value: payload };
  }
}
