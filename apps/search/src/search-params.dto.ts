import { IsArray, IsBoolean, IsDate, IsIn, IsInt, IsOptional, Matches } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

const timePattern = /^\d{1,2}:\d{2}(\+\d{2,4}|Z)?$/;

export class SearchParamsDTO {

  @ApiProperty()
  @IsInt()
  @IsOptional()
  parent: number;

  @ApiProperty({ type: String, format: 'date' })
  @IsDate()
  @Type(() => Date)
  date: Date;

  @ApiProperty()
  @Matches(timePattern)
  fromTime: string;

  @ApiProperty()
  @Matches(timePattern)
  toTime: string;

  @ApiProperty({type: Number, isArray: true, enum: [0, 1, 2, 3, 4, 5] })
  @IsArray()
  @IsIn([0, 1, 2, 3, 4, 5], {
    each: true,
  })
  children: number[];

  @ApiProperty()
  @IsBoolean()
  trial: boolean;
}
