import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { DealService } from '../../deal.service';
import { StartVisitCommand } from '../../commands';
import { DealExceptionEvent } from '../../events/deal-exception.event';

@CommandHandler(StartVisitCommand)
export class StartVisitCommandHandler
  implements ICommandHandler<StartVisitCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly service: DealService,
  ) {
  }

  async execute(command: StartVisitCommand): Promise<any> {
    const deal = this.publisher.mergeObjectContext(command.deal);
    try {
      deal.startVisit();
    } catch (e) {
      deal.apply(new DealExceptionEvent(deal, 'start-visit', e));
      deal.commit();
      throw e;
    }
    await this.service.save(deal);
    deal.commit();
    return deal;
  }
}
