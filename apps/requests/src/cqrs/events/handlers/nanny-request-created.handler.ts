import { EventsHandler, IEventHandler, QueryBus } from '@nestjs/cqrs';
import { NannyRequestCreatedEvent } from '../impl';
import { NannyRequestCreated } from '@event-bus/contracts';
import { Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { NannyRequest } from '../../../entities/nanny-request.entity';
import { ParentRequest } from '../../../entities/parent-request.entity';
import { ParentRequestByNannyRequestIdQuery } from '../../queries/impl';

@EventsHandler(NannyRequestCreatedEvent)
export class NannyRequestCreatedHandler implements IEventHandler<NannyRequestCreatedEvent> {
  constructor(
    @Inject('EVENT_BUS')
    private eventBus: ClientProxy,
    private queryBus: QueryBus,
  ) {
  }

  async handle(event: NannyRequestCreatedEvent): Promise<any> {
    const { request } = event;
    await this.populateParentRequest(request);
    const cmd = new NannyRequestCreated({
      request: request.parentRequest.id,
      nanny: request.user,
      parent: request.parentRequest.user,
    });
    this.eventBus.emit(cmd.name, cmd.payload);
  }

  private async populateParentRequest(request: NannyRequest): Promise<void> {
    let { parentRequest } = request;

    if (parentRequest instanceof ParentRequest) {
      return;
    }
    parentRequest = await this.queryBus.execute(new ParentRequestByNannyRequestIdQuery(request.id));
    request.parentRequest = parentRequest;
  }
}
