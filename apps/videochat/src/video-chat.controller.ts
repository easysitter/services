import {
  Body,
  Controller,
  Delete,
  InternalServerErrorException,
  Param,
  Post,
} from '@nestjs/common';
import { CreateSessionDto } from './create-session.dto';
import { VideoChatService } from './video-chat.service';
import { VideoChatSessionEntity } from './video-chat-session.entity';
import { CreateTokenDto } from './create-token.dto';
import { TerminateSessionDto } from './terminate-session.dto';

@Controller('sessions')
export class VideoChatController {
  constructor(private videoChat: VideoChatService) {}

  @Post()
  async createSession(
    @Body() data: CreateSessionDto,
  ): Promise<VideoChatSessionEntity> {
    try {
      return await this.videoChat.createSession(data);
    } catch (error) {
      throw new InternalServerErrorException(error.message, error);
    }
  }

  @Post(':session/token')
  async createToken(
    @Param('session') sessionId: string,
    @Body() data: CreateTokenDto,
  ) {
    try {
      return await this.videoChat.getToken(sessionId, data);
    } catch (error) {
      throw new InternalServerErrorException(error.message, error);
    }
  }

  @Post(':session/terminate')
  async terminateSession(
    @Param('session') sessionId: string,
    @Body() data: TerminateSessionDto,
  ) {
    try {
      return await this.videoChat.terminateSession(sessionId, data);
    } catch (error) {
      throw new InternalServerErrorException(error.message, error);
    }
  }
}
