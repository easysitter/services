import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { CreateOfferCommand } from '../../commands';
import { Deal } from '../../models';
import { DealService } from '../../deal.service';
import { DealExceptionEvent } from '../../events/deal-exception.event';

@CommandHandler(CreateOfferCommand)
export class CreateOfferCommandHandler
  implements ICommandHandler<CreateOfferCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly service: DealService,
  ) {
  }

  async execute(command: CreateOfferCommand): Promise<any> {
    const deal = this.publisher.mergeObjectContext(new Deal());
    try {
      deal.createOffer(command.offer);
    } catch (e) {
      deal.apply(new DealExceptionEvent(deal, 'create-offer', e));
      deal.commit();
      throw e;
    }
    await this.service.save(deal);
    deal.commit();
    return deal;
  }
}
