import { ICommand } from '@nestjs/cqrs';

export class SendNotificationCommand implements ICommand {
  constructor(public readonly type: string, public readonly meta?: any) {}
}
