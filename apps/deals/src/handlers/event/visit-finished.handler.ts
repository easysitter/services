import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { VisitFinishedEvent } from '../../events/visit-finished.event';
import { VisitFinished } from '@event-bus/contracts';

@EventsHandler(VisitFinishedEvent)
export class VisitFinishedHandler implements IEventHandler<VisitFinishedEvent> {

  constructor(
    @Inject('EVENT_BUS')
    private channel: ClientProxy,
  ) {
  }

  handle(event: VisitFinishedEvent): any {
    const { deal } = event;
    const msg = new VisitFinished({
      ...deal,
      safePayment: false,
    });
    this.channel.emit(msg.name, msg.payload);
  }
}
