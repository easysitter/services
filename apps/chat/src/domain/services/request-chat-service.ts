import { CreateChatForRequestCommand, RequestChatUseCase } from '../ports/primary';
import { Observable, throwError } from 'rxjs';
import { ChatModel } from '../models';
import { ChatsPort } from '../ports/secondary';
import { mapTo, switchMap } from 'rxjs/operators';
import { v4 as uuid } from 'uuid';
import { ChatNotificationsPort } from '../ports/secondary/chat-notifications.port';

export class RequestChatService implements RequestChatUseCase {

  constructor(
    private readonly chatsPort: ChatsPort,
    private readonly notificationsPort: ChatNotificationsPort,
  ) {
  }

  createChatForRequest(command: CreateChatForRequestCommand): Observable<ChatModel> {
    const chat = new ChatModel(
      uuid(),
      command.users,
      {
        request: command.request,
        type: 'request',
      },
    );
    return this.chatsPort.save(chat).pipe(
      switchMap(result => {
        if (!result) {
          return throwError(new Error('failed to create chat'));
        } else {
          return this.notificationsPort.notifyNewChat(chat).pipe(mapTo(chat));
        }
      }),
    );
  }

  findChatForRequest(users: string[], request: number): Observable<ChatModel> {
    return this.chatsPort.loadByMetaForUsers(users, { request });
  }
}
