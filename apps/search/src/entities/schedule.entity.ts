import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Schedule {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  user: number;

  @Column()
  start: number;

  @Column()
  end: number;

  @Column()
  @Index()
  day: number;
}
