import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

interface Price {
  value: number;
  currency: string;
}

@Entity()
export class NannyProfile {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column({ nullable: true })
  birthDate: string;
  @Column({ nullable: true })
  description: string;
  @Column({ nullable: true })
  photo: string;
  @Column({ type: 'jsonb' })
  price: Price;
  @Column({ type: 'jsonb' })
  trial: Price;
  @Column()
  trialEnabled: boolean;
  @Column({ type: 'jsonb' })
  schedule: any[];
  @Column()
  phone: string;
  @Column({ type: 'jsonb' })
  ageCategories: number[];
  @Column({ type: 'int', default: 5 })
  maxChildrenCount: number;

  @Column({ type: 'jsonb', nullable: true })
  features: Record<string, boolean>;
  @Column({ type: 'jsonb', nullable: true })
  documents: Record<string, string>;
  @Column({ type: 'jsonb', nullable: true })
  districts: string[];
}
