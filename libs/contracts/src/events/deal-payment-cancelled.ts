import { EventBusEvent } from '@event-bus/contracts';

interface DealPaymentCancelledPayload {
  id: string;
  amount: number;
  currency: string;
  cancelledAt: Date;
  deal: string;
  user: string;
}

export class DealPaymentCancelled implements EventBusEvent<DealPaymentCancelledPayload> {
  static readonly eventName = 'deal_payment_cancelled';
  static readonly channelName = 'payments';

  public readonly channel = DealPaymentCancelled.channelName;
  public readonly name = DealPaymentCancelled.eventName;

  constructor(
    public readonly payload: DealPaymentCancelledPayload,
  ) {
  }
}
