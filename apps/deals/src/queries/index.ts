export * from './deal-by-id.query';
export * from './find-deals.query';
export * from './find-outdated-deals';
