import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CqrsModule } from '@nestjs/cqrs';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { getClientConfig } from '@utils/transport-connector';
import { LoggerModule } from 'nestjs-pino';
import { NannyRequest } from './entities/nanny-request.entity';
import { ParentRequest } from './entities/parent-request.entity';
import {
  AcceptRequestCommandHandler,
  CancelNannyRequestCommandHandler,
  CancelRequestCommandHandler,
  CloseNannyRequestCommandHandler,
  CreateRequestCommandHandler,
  DealProposedCommandHandler,
  OfferAcceptedCommandHandler,
  OfferRefusedCommandHandler,
  RefuseRequestCommandHandler,
  RemoveParentRequestsCommandHandler,
  SendNotificationCommandHandler, SuspendOutdatedRequestCommandHandler,
} from './cqrs/commands';

import {
  NannyRequestCreatedHandler,
  NannyRequestStatusUpdatedHandler,
  RequestAcceptedHandler,
  RequestCancelledHandler,
  RequestOutdatedHandler,
  SameStatusChangeHandler,
} from './cqrs/events/handlers';

import {
  NannyRequestQueryHandler,
  NannyRequestsQueryHandler,
  OutdatedRequestsQueryHandler,
  ParentRequestByNannyRequestIdQueryHandler,
  ParentRequestsQueryHandler,
  RequestQueryHandler,
} from './cqrs/queries';

import { CommonRequestController } from './controllers/common-request.controller';
import { NannyRequestController } from './controllers/nanny-request.controller';
import { ParentRequestController } from './controllers/parent-request.controller';

@Module({
  imports: [
    CqrsModule,
    LoggerModule.forRoot({
      pinoHttp: {
        level: process.env.NODE_ENV === 'dev' ? 'debug' : 'info',
        prettyPrint: process.env.NODE_ENV === 'dev',
      },
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      database: process.env.DB_NAME,
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      username: process.env.DB_USER_NAME,
      password: process.env.DB_USER_PASSWORD,
      schema: process.env.DB_SCHEMA || 'public',
      entities: [NannyRequest, ParentRequest],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([NannyRequest, ParentRequest]),
    ClientsModule.register([{
      name: 'EVENT_BUS',
      transport: Transport.KAFKA,
      options: {
        client: {
          ...getClientConfig(),
          clientId: 'requests-service',
        },
        producer: {
          allowAutoTopicCreation: true,
          idempotent: false,
        },
      },
    }]),
  ],
  controllers: [
    CommonRequestController,
    NannyRequestController,
    ParentRequestController,
  ],
  providers: [
    // queries
    NannyRequestQueryHandler,
    NannyRequestsQueryHandler,
    ParentRequestsQueryHandler,
    ParentRequestByNannyRequestIdQueryHandler,
    RequestQueryHandler,
    OutdatedRequestsQueryHandler,
    // events
    RequestAcceptedHandler,
    RequestCancelledHandler,
    NannyRequestCreatedHandler,
    NannyRequestStatusUpdatedHandler,
    SameStatusChangeHandler,
    RequestOutdatedHandler,
    // commands
    RemoveParentRequestsCommandHandler,
    CreateRequestCommandHandler,
    AcceptRequestCommandHandler,
    RefuseRequestCommandHandler,
    CloseNannyRequestCommandHandler,
    CancelNannyRequestCommandHandler,
    CancelRequestCommandHandler,
    OfferAcceptedCommandHandler,
    OfferRefusedCommandHandler,
    DealProposedCommandHandler,
    SendNotificationCommandHandler,
    SuspendOutdatedRequestCommandHandler,
  ],
})
export class RequestsModule {
}
