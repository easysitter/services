import { Body, Controller, Get, NotFoundException, Param, ParseIntPipe, Post, Put } from '@nestjs/common';
import { ApiOkResponse, ApiParam } from '@nestjs/swagger';
import { ParentProfileService } from './parent-profile.service';
import { CreateProfileDto, ProfileDto } from './dto';
import { ParentProfile } from './entities';

@Controller()
export class ParentProfileController {
  constructor(
    private profile: ParentProfileService,
  ) {
  }

  @Get('user/:userId')
  @ApiParam({
    name: 'userId',
    type: String,
  })
  async getProfileId(
    @Param('userId')
      userId: string,
  ) {
    const user = await this.profile.findByUser(userId);
    if (!user) {
      throw new NotFoundException();
    }
    return user.profile.id;
  }

  @Post('')
  async createUser(
    @Body() data: CreateProfileDto,
  ) {
    return await this.profile.create(data.userId, data.profile);
  }

  @Get(':profileId')
  @ApiParam({
    name: 'profileId',
    type: Number,
  })
  @ApiOkResponse({ type: ParentProfile })
  async getProfile(
    @Param('profileId', ParseIntPipe)
      profileId: number,
  ) {
    const profile = await this.profile.findById(profileId);
    if (!profile) {
      throw new NotFoundException();
    }
    return profile;
  }

  @Put(':profileId')
  @ApiParam({
    name: 'profileId',
    type: Number,
  })
  @ApiOkResponse({ type: ParentProfile })
  async updateProfile(
    @Param('profileId', ParseIntPipe)
      profileId: number,
    @Body() data: ProfileDto,
  ) {
    const profile = await this.profile.findById(profileId);
    if (!profile) {
      throw new NotFoundException();
    }
    return this.profile.save({
      ...data,
      id: profileId,
    });
  }
}
