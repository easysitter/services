import { ApiProperty } from '@nestjs/swagger';

export class ZipCheckInput {
  @ApiProperty()
  zip: string;
  @ApiProperty({ isArray: true, type: String })
  districtIds: string[];
}
