import { MessageDbEntity } from '../entities/message.db.entity';
import { ChatMessage } from '../../../../domain/models';

export class MessageMapper {
  toDomain(entity: MessageDbEntity): ChatMessage {
    return new ChatMessage(
      entity.id,
      entity.text,
      entity.user,
      new Date(entity.createdAt),
      entity.viewedBy,
    );
  }

  fromDomain(model: ChatMessage): MessageDbEntity {
    const entity = new MessageDbEntity();

    entity.id = model.id;
    entity.user = model.user;
    entity.text = model.text;
    entity.createdAt = model.createdAt;
    entity.viewedBy = model.viewedBy;
    return entity;
  }
}
