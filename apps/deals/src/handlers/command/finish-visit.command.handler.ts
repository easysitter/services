import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { DealService } from '../../deal.service';
import { FinishVisitCommand } from '../../commands';
import { DealExceptionEvent } from '../../events/deal-exception.event';

@CommandHandler(FinishVisitCommand)
export class FinishVisitCommandHandler
  implements ICommandHandler<FinishVisitCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly service: DealService,
  ) {
  }

  async execute(command: FinishVisitCommand): Promise<any> {
    const deal = this.publisher.mergeObjectContext(command.deal);
    try {
      deal.finishVisit();
    } catch (e) {
      deal.apply(new DealExceptionEvent(deal, 'finish-visit', e));
      deal.commit();
      throw e;
    }
    await this.service.save(deal);
    deal.commit();
    return deal;
  }
}
