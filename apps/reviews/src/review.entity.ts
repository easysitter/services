import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Review {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  rating: number;

  @Column()
  userId: string;

  @Column()
  userType: string;

  @Column()
  refId: string;

  @Column()
  refType: string;

  @Column({ type: 'text', default: '' })
  pros: string;

  @Column({ type: 'text', default: '' })
  cons: string;

  @CreateDateColumn()
  createdAt;
}
