import { ParentRequest } from '../../../entities/parent-request.entity';
import { IEvent } from '@nestjs/cqrs';

export class RequestCreatedEvent implements IEvent {
  constructor(public readonly request: ParentRequest) {}
}
