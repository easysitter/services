import { NannyRequest } from '../../../entities/nanny-request.entity';
import { ICommand } from '@nestjs/cqrs';

export class CancelNannyRequestCommand implements ICommand {
  constructor(public readonly request: NannyRequest) {}
}
