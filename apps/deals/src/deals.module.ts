import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { CqrsModule } from '@nestjs/cqrs';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LoggerModule } from 'nestjs-pino';

import { DealController } from './deal.controller';
import { Deal } from './models';
import {
  AcceptOfferCommandHandler,
  CancelDealCommandHandler,
  CompleteDealCommandHandler,
  CreateAcceptedDealCommandHandler,
  CreateOfferCommandHandler,
  FinishVisitCommandHandler,
  RefuseOfferCommandHandler,
  StartVisitCommandHandler,
} from './handlers/command';
import { DealByIdQueryHandler, FindDealsQueryHandler, FindOutdatedDealsQueryHandler } from './handlers/query';
import { DealService } from './deal.service';
import {
  DealCancelledHandler,
  DealConcludedHandler,
  DealExceptionEventHandler,
  OfferCancelledHandler,
  OfferCreatedHandler,
  SameStatusChangeHandler,
  VisitStartedHandler,
} from './handlers/event';
import { VisitFinishedHandler } from './handlers/event/visit-finished.handler';
import { getClientConfig } from '@utils/transport-connector';
import { SuspendOutdatedDealCommandHandler } from './handlers/command/suspend-outdated-deal.command.handler';
import { FindComingDealsQueryHandler } from './handlers/query/find-coming-deals.query.handler';

@Module({
  imports: [
    CqrsModule,
    LoggerModule.forRoot({
      pinoHttp: {
        level: process.env.NODE_ENV === 'dev' ? 'debug' : 'info',
        prettyPrint: process.env.NODE_ENV === 'dev',
      },
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      database: process.env.DB_NAME,
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      username: process.env.DB_USER_NAME,
      password: process.env.DB_USER_PASSWORD,
      entities: [Deal],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([Deal]),
    ClientsModule.register([{
      name: 'EVENT_BUS',
      transport: Transport.KAFKA,
      options: {
        client: {
          ...getClientConfig(),
          clientId: 'deals-service',
        },
        producer: {
          allowAutoTopicCreation: true,
          idempotent: true,
        },
      },
    }]),
  ],
  exports: [CqrsModule],
  controllers: [DealController],
  providers: [
    DealService,
    // command handlers
    AcceptOfferCommandHandler,
    CancelDealCommandHandler,
    CompleteDealCommandHandler,
    CreateAcceptedDealCommandHandler,
    CreateOfferCommandHandler,
    RefuseOfferCommandHandler,
    StartVisitCommandHandler,
    FinishVisitCommandHandler,
    SuspendOutdatedDealCommandHandler,
    // query handlers
    DealByIdQueryHandler,
    FindDealsQueryHandler,
    FindOutdatedDealsQueryHandler,
    FindComingDealsQueryHandler,
    // event handlers
    OfferCreatedHandler,
    OfferCancelledHandler,
    DealConcludedHandler,
    DealCancelledHandler,
    DealExceptionEventHandler,
    SameStatusChangeHandler,
    VisitStartedHandler,
    VisitFinishedHandler,
  ],
})
export class DealsModule {
}
