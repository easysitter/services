import { CalculatorTracer } from '../utils/calculator-tracer';
// tslint:disable-next-line:max-classes-per-file
import { splitIntervalToChunks } from '../utils/split-interval-to-chunks';
import { Calculator } from './calculator';
import { CalculatorParams } from './calculator-params';

export class DayNightSplitCalculator extends Calculator {
  constructor(
    private dayCalculator: Calculator,
    private nightCalculator: Calculator,
  ) {
    super();
  }

  async getPrice(
    deal: CalculatorParams,
    tracer: CalculatorTracer,
  ): Promise<number> {
    const from = deal.from;
    const to = deal.to;

    const chunks = splitIntervalToChunks(from, to);
    const prices = await Promise.all(
      chunks.map(async chunk => {
        const chunkDeal = { ...deal, from: chunk.from, to: chunk.to };
        // tslint:disable-next-line:one-variable-per-declaration
        let calculator;
        let chunkTracer: CalculatorTracer;
        if (chunk.intervalType === 'day') {
          calculator = this.dayCalculator;
          chunkTracer = tracer.fork('DAY');
        } else {
          calculator = this.nightCalculator;
          chunkTracer = tracer.fork('NIGHT');
        }

        return await calculator.getPrice(chunkDeal, chunkTracer);
      }),
    );

    return prices.reduce((sum, price) => sum + price, 0);
  }
}

export type IntervalType = 'day' | 'night';
