import { Merchant } from '../entities/merchant';
import { Payment } from '../entities/payment';

export interface CreateMerchantParams {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
}

export interface CreatePaymentParams {
  amount: number;
  currency: string;
  meta?: any;
  type: string;
}

export interface MerchantVerificationResult {
  extId: string;
  verified: boolean;
  meta: any;
}

export abstract class PaymentProvider {
  abstract createMerchant(params: CreateMerchantParams): Promise<Merchant>;

  abstract getOnBoardingUrl(account: Merchant): Promise<string>;

  abstract handleVerification(data: any): Promise<MerchantVerificationResult>;

  abstract createPayment(params: CreatePaymentParams): Promise<Payment>;

  abstract completePayment(payment: Payment): Promise<Payment>;
}
