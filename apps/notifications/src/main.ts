import { NestFactory } from '@nestjs/core';
import { NotificationsModule } from './notifications.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { Transport } from '@nestjs/microservices';
import { getClientConfig, patchNest } from '@utils/transport-connector';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  patchNest();
  const app = await NestFactory.create(NotificationsModule);
  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);
  try {
    app.connectMicroservice({
      transport: Transport.KAFKA,
      options: {
        client: getClientConfig(),
        consumer: {
          groupId: 'notifications-service',
        },
      },
    });
    await app.startAllMicroservicesAsync();
  } catch (e) {
    Logger.error(e.message, null, 'Boot');
    return process.exit(1);
  }
  const options = new DocumentBuilder()
    .setTitle('Notifications api')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  const port = process.env.HTTP_PORT || 3333;
  SwaggerModule.setup('/docs', app, document);
  await app.listen(port, () => {
    console.log('Listening at http://localhost:' + port + '/' + globalPrefix);
    console.log('Listening at http://localhost:' + port + '/docs');
  });
}

bootstrap();
