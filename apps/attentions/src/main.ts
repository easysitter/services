import { NestFactory } from '@nestjs/core';
import { AttentionsModule } from './attentions.module';
import { patchNest } from '@utils/transport-connector';
import { Logger } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  patchNest();
  const app = await NestFactory.create(AttentionsModule);
  const logger = app.get(Logger);
  app.useLogger(logger);
  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);

  const options = new DocumentBuilder()
    .setTitle('Request api')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  const port = process.env.HTTP_PORT || 3333;
  SwaggerModule.setup('/docs', app, document);
  await app.listen(port, () => {
    logger.log('Listening at http://localhost:' + port + '/' + globalPrefix);
    logger.log('Listening at http://localhost:' + port + '/docs');
  });

  process.on('SIGTERM', () => {
    logger.log('SIGTERM');
    app.close();
  });
}
bootstrap();
