import { Body, Controller, Delete, Get, NotFoundException, Param, Post, Put, Query, ValidationPipe } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { defer, Observable } from 'rxjs';
import { map, mergeMap, switchMap } from 'rxjs/operators';

import { OfferDto, SearchDto } from './dto';
import { ActionCommandMap, CommandCtor } from './interfaces';
import { Deal } from './models';
import { DealByIdPipe } from './deal-by-id.pipe';

import { DealByIdQuery, FindDealsQuery, FindOutdatedDeals } from './queries';
import {
  CancelDealCommand,
  CompleteDealCommand,
  CreateAcceptedDealCommand,
  DealCommand,
  FinishVisitCommand,
  StartVisitCommand,
  SuspendOutdatedDealCommand,
} from './commands';
import { ApiOkResponse, ApiParam } from '@nestjs/swagger';
import { DealPaymentCancelled, RequestCancelled } from '@event-bus/contracts';
import { FindComingDealsQuery } from './queries/find-coming-deals.query';

@Controller('/deals')
export class DealController {
  private visitActions: ActionCommandMap<Deal, DealCommand> = {
    start: (deal) => new StartVisitCommand(deal),
    finish: (deal) => new FinishVisitCommand(deal),
  };

  constructor(
    private readonly queryBus: QueryBus,
    private readonly commandBus: CommandBus,
  ) {
  }

  @Get('/outdated')
  @ApiOkResponse({ isArray: true, type: Deal })
  getOutdatedDeals(): Promise<Deal[]> {
    return this.queryBus.execute(new FindOutdatedDeals());
  }

  @Get('/coming')
  @ApiOkResponse({ isArray: true, type: Deal })
  getComingDeals(): Promise<Deal[]> {
    return this.queryBus.execute(new FindComingDealsQuery());
  }

  @Post('/outdated/suspend')
  @ApiOkResponse()
  async suspendOutdatedDeals(): Promise<void> {
    const deals = await this.queryBus.execute<FindOutdatedDeals, Deal[]>(new FindOutdatedDeals());

    const promiseList = deals.map(async deal => {
      try {
        await this.commandBus.execute(new SuspendOutdatedDealCommand(deal));
      } catch (e) {
        return;
      }
    });
    await Promise.all(promiseList);
  }

  @Get()
  @ApiOkResponse({ isArray: true, type: Deal })
  findDeals(
    @Query(
      new ValidationPipe({
        transform: true,
        whitelist: true,
        forbidNonWhitelisted: true,
        validationError: {
          value: false,
          target: false,
        },
      }),
    )
      search: SearchDto,
  ): Promise<Deal[]> {
    return this.queryBus.execute(new FindDealsQuery(search));
  }

  @Post()
  createAcceptedDeal(
    @Body(
      new ValidationPipe({
        transform: true,
        whitelist: true,
        forbidNonWhitelisted: true,
        validationError: { target: false, value: false },
      }),
    )      offer: OfferDto,
  ) {
    return this.commandBus.execute(new CreateAcceptedDealCommand(offer));
  }

  @Get('/:dealId')
  @ApiParam({
    name: 'dealId',
    type: Number,
  })
  getDeal(@Param('dealId', DealByIdPipe) deal: Deal) {
    return deal;
  }

  @Put('/:dealId/complete')
  @ApiParam({
    name: 'dealId',
    type: Number,
  })
  completeDeal(@Param('dealId', DealByIdPipe) deal: Deal) {
    return this.commandBus.execute(new CompleteDealCommand(deal));
  }

  @Put('/:dealId/visit/:visitAction')
  @ApiParam({
    name: 'dealId',
    type: Number,
  })
  @ApiParam({
    name: 'visitAction',
    type: String,
    enum: ['start', 'finish'],
  })
  processVisit(
    @Param('dealId', DealByIdPipe) deal: Deal,
    @Param('visitAction') action: string,
  ) {
    if (!this.visitActions[action]) {
      throw new NotFoundException();
    }
    return this.commandBus.execute(this.visitActions[action](deal));
  }

  @Delete(':dealId')
  @ApiParam(({
    name: 'dealId',
    type: Number,
  }))
  cancelDeal(@Param('dealId', DealByIdPipe) deal: Deal): Observable<any> {
    return defer(() =>
      this.commandBus.execute(new CancelDealCommand(deal, 'cancelled')),
    );
  }

  @EventPattern('request_suspended')
  requestSuspended(data: { request: string; nanny: number }) {
    const search = new SearchDto();
    search.request = data.request;
    search.nanny = data.nanny;
    search.state = 'offer';
    return this.handleEvent(
      search,
      deal => new CancelDealCommand(deal, 'suspended'),
    );
  }

  @EventPattern(RequestCancelled.eventName)
  requestCancelled({ value, ...brokerMessage }: { value: RequestCancelled['payload'] }) {
    const search = new SearchDto();
    search.request = value.id.toString();
    search.state = 'offer';
    search.isActive = true;
    return this.handleEvent(
      search,
      deal => new CancelDealCommand(deal, 'cancelled'),
    );
  }

  @EventPattern(DealPaymentCancelled.channelName)
  async onPaymentCancelled(
    { value, key }: { value: DealPaymentCancelled['payload'], key: string },
  ) {
    if (key !== DealPaymentCancelled.eventName) {
      return;
    }
    const query = new DealByIdQuery(+value.deal);
    return defer(() => this.queryBus.execute<DealByIdQuery, Deal>(query)).pipe(
      map(deal => new CancelDealCommand(deal, 'suspended')),
      switchMap(cmd => this.commandBus.execute(cmd)),
    );
  }

  handleEvent(search: SearchDto, commandCtor: CommandCtor<Deal, DealCommand>) {
    return defer(() =>
      this.queryBus.execute<FindDealsQuery, Deal[]>(new FindDealsQuery(search)),
    ).pipe(
      map(deals => deals.map(commandCtor)),
      mergeMap(commands =>
        commands.map(command => this.commandBus.execute(command)),
      ),
    );
  }
}
