import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LoggerModule } from 'nestjs-pino';

import * as OpenTok from 'opentok';

import { OpentokAdapter } from './opentok.adapter';
import { VideoChatService } from './video-chat.service';
import { VideoChatSessionEntity } from './video-chat-session.entity';
import { VideoChatParticipantEntity } from './video-chat-participant.entity';
import { VideoChatController } from './video-chat.controller';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { getClientConfig } from '@utils/transport-connector';
import { CqrsModule } from '@nestjs/cqrs';
import { VideoChatParticipantAddedHandler } from './video-chat-participant-added.handler';
import { VideoChatTerminatedHandler } from './video-chat-terminated.handler';

@Module({
  imports: [
    LoggerModule.forRoot({
      pinoHttp: {
        level: process.env.NODE_ENV === 'dev' ? 'debug' : 'info',
        prettyPrint: process.env.NODE_ENV === 'dev',
      },
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      database: process.env.DB_NAME,
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      username: process.env.DB_USER_NAME,
      password: process.env.DB_USER_PASSWORD,
      entities: [VideoChatSessionEntity, VideoChatParticipantEntity],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([
      VideoChatSessionEntity,
      VideoChatParticipantEntity,
    ]),
    CqrsModule,
    ClientsModule.register([
      {
        name: 'EVENT_BUS',
        transport: Transport.KAFKA,
        options: {
          client: {
            ...getClientConfig(),
            clientId: 'videochat-service',
          },
        },
      },
    ]),
  ],
  controllers: [VideoChatController],
  providers: [
    {
      provide: OpenTok,
      useFactory: () => {
        const { OPENTOK_API_KEY, OPENTOK_API_SECRET } = process.env;
        const apiKey = OPENTOK_API_KEY.trim();
        const apiSecret = OPENTOK_API_SECRET.trim();
        return new OpenTok(apiKey, apiSecret);
      },
    },
    OpentokAdapter,
    VideoChatService,
    VideoChatParticipantAddedHandler,
    VideoChatTerminatedHandler,
  ],
})
export class VideoChatApplication {}
