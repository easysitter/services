import { addDays, format, isBefore } from 'date-fns';
import { toDate } from 'date-fns-tz';
import { IntervalType } from '../calculator/day-night.calculator';

export function splitIntervalToChunks(
  from: Date,
  to: Date,
): Array<{ from: Date; to: Date; intervalType: IntervalType }> {
  const timeZoneOptions = {
    timeZone: 'Europe/Warsaw',
  };

  const dateStr = format(from, 'yyyy-MM-dd');

  let dayChunkBoundary = toDate(`${dateStr}T07:00:00`, timeZoneOptions);
  let nightChunkBoundary = toDate(`${dateStr}T21:00:00`, timeZoneOptions);

  let nextBottomBoundary;
  let isDay;
  if (isBefore(from, dayChunkBoundary)) {
    isDay = false;
    nextBottomBoundary = dayChunkBoundary;
  } else if (isBefore(from, nightChunkBoundary)) {
    isDay = true;
    nextBottomBoundary = nightChunkBoundary;
    dayChunkBoundary = addDays(dayChunkBoundary, 1);
  } else {
    isDay = false;
    dayChunkBoundary = addDays(dayChunkBoundary, 1);
    nightChunkBoundary = addDays(nightChunkBoundary, 1);
    nextBottomBoundary = dayChunkBoundary;
  }

  const chunks = [];

  while (isBefore(nextBottomBoundary, to)) {
    chunks.push({
      from,
      to: nextBottomBoundary,
      intervalType: isDay ? 'day' : 'night',
    });
    from = nextBottomBoundary;
    if (!isDay) {
      nextBottomBoundary = nightChunkBoundary;
      dayChunkBoundary = addDays(dayChunkBoundary, 1);
    } else {
      nextBottomBoundary = dayChunkBoundary;
      nightChunkBoundary = addDays(nightChunkBoundary, 1);
    }
    isDay = !isDay;
  }
// if (isBefore(to, from)) {
//   to = addDays(to, 1);
// }
  chunks.push({
    from,
    to,
    intervalType: isDay ? 'day' : 'night',
  });

  return chunks;
}
