import { Rates } from './rates/rates';

export class CalculatorRates {
  constructor(
    private dayRate: Rates,
    private nightRate: Rates,
  ) {
  }

  async getDayRate() {
    return this.dayRate.getRate(null);
  }

  async getNightRate() {
    return this.nightRate.getRate(null);
  }
}
