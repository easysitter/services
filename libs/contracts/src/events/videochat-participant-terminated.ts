class VideochatParticipantTerminatedPayload {
  role: 'nanny' | 'parent';
  userId: string;
  sessionId: string;
}

// tslint:disable-next-line:max-classes-per-file
export class VideochatParticipantTerminated {
  static readonly channelName = 'videochat';

  readonly channel = VideochatParticipantTerminated.channelName;

  get key() {
    return `terminated` as const;
  }

  constructor(public readonly payload: VideochatParticipantTerminatedPayload) {}
}
