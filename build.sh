#!/usr/bin/env bash
declare -a services=(
  "attentions"
  "addresses"
  "chat"
  "deals"
  "nanny-profile"
  "parent-profile"
  "requests"
  "search"
  "reviews"
  "notifications"
  "payments"
  "videochat"
)
CMD="echo Building services"
UNDEPLOY=""
for service in "${services[@]}"; do
  CMD="${CMD} \t&& docker build --file ${service}.dockerfile -t registry.gitlab.com/easysitter/services/${service}:latest ."
  CMD="${CMD} \t&& docker push registry.gitlab.com/easysitter/services/${service}:latest "
  UNDEPLOY="${UNDEPLOY} \t&& kubectl.exe delete deployment -l tier=backend,app=${service} -n easysitter"
  # or do whatever with individual element of the array
done

CMD="${CMD} ${UNDEPLOY}"
CMD="${CMD} \t&& cat configs/services.yaml | linkerd inject - | kubectl.exe apply -f -"
CMD="${CMD} \t&& cat configs/videochat.yaml | linkerd inject - | kubectl.exe apply -f -"
echo -e $CMD
