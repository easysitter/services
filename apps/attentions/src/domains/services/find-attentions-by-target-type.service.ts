import { FindAttentionsByTargetTypeUseCase } from '../ports/in/find-attentions-by-target-type.use-case';
import { AttentionEntity, AttentionTargetType, LoadAttentionsPort } from '..';

export class FindAttentionsByTargetTypeService implements FindAttentionsByTargetTypeUseCase {
  constructor(
    private loadAttentionsPort: LoadAttentionsPort,
  ) {
  }

  find(targetType: AttentionTargetType): Promise<AttentionEntity[]> {
    return this.loadAttentionsPort.loadAttentions({
      targetType,
    });
  }
}
