import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientsModule, Transport } from '@nestjs/microservices';

import { LoggerModule } from 'nestjs-pino/dist';

import { getClientConfig } from '@utils/transport-connector';

import { ChatController, DirectChatController, RequestChatController } from './adapters/primary';
import { ChatNotificationsKafkaAdapter, ChatsDbAdapter, MessagesDbAdapter } from './adapters/secondary';
import { ChatDbEntity, MessageDbEntity } from './adapters/secondary/entities';

import { ChattingUseCase, DirectChatUseCase, RequestChatUseCase } from '../domain/ports/primary';
import { ChatNotificationsPort, ChatsPort, MessagesPort } from '../domain/ports/secondary';
import { ChatService, DirectChatService, RequestChatService } from '../domain/services';

@Module({
  imports: [
    LoggerModule.forRoot({
      pinoHttp: {
        level: process.env.NODE_ENV === 'dev' ? 'debug' : 'info',
        prettyPrint: process.env.NODE_ENV === 'dev',
      },
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      database: process.env.DB_NAME,
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      username: process.env.DB_USER_NAME,
      password: process.env.DB_USER_PASSWORD,
      entities: [ChatDbEntity, MessageDbEntity],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([ChatDbEntity, MessageDbEntity]),
    ClientsModule.register([{
      name: 'EVENT_BUS',
      transport: Transport.KAFKA,
      options: {
        client: {
          ...getClientConfig(),
          clientId: 'chat-service',
        },
      },
    }]),
  ],
  providers: [
    ChatsDbAdapter,
    MessagesDbAdapter,
    ChatNotificationsKafkaAdapter,
    {
      provide: ChatService,
      useFactory: (
        chatsPort: ChatsPort,
        messagesPort: MessagesPort,
        notificationsPort: ChatNotificationsPort,
      ) => {
        return new ChatService(chatsPort, messagesPort, notificationsPort);
      },
      inject: [ChatsDbAdapter, MessagesDbAdapter, ChatNotificationsKafkaAdapter],
    },
    {
      provide: RequestChatService,
      useFactory: (
        chatsPort: ChatsPort,
        notificationsPort: ChatNotificationsPort,
      ): RequestChatService => {
        return new RequestChatService(chatsPort, notificationsPort);
      },
      inject: [ChatsDbAdapter, ChatNotificationsKafkaAdapter],
    },
    {
      provide: DirectChatService,
      useFactory: (
        chatsPort: ChatsPort,
        notificationsPort: ChatNotificationsPort,
      ): DirectChatService => {
        return new DirectChatService(chatsPort, notificationsPort);
      },
      inject: [ChatsDbAdapter, ChatNotificationsKafkaAdapter],
    },
    { provide: ChattingUseCase, useExisting: ChatService },
    { provide: RequestChatUseCase, useExisting: RequestChatService },
    { provide: DirectChatUseCase, useExisting: DirectChatService },
  ],
  controllers: [
    RequestChatController,
    DirectChatController,
    ChatController,
  ],
})
export class ChatApplication {
}
