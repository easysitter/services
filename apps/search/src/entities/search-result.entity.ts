import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Nanny } from './nanny.entity';

@Entity()
export class SearchResult {
  @PrimaryGeneratedColumn('increment')
  id: string;

  @Column({
    nullable: true,
  })
  parent: number;

  @Column()
  date: Date;

  @Column()
  fromTime: string;

  @Column()
  toTime: string;

  @Column({
    default: false,
    nullable: true,
  })
  trial: boolean;

  @Column({ type: 'jsonb' })
  children: number[];

  @Column({ type: 'jsonb' })
  nannies: Nanny[];
}
