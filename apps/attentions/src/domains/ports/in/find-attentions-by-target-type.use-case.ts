import { AttentionEntity, AttentionTargetType } from '../..';

export abstract class FindAttentionsByTargetTypeUseCase {
  abstract find(targetType: AttentionTargetType): Promise<AttentionEntity[]>;
}
