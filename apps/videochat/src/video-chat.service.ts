import { ForbiddenException, Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { EventPublisher } from '@nestjs/cqrs';
import { Connection, In, Repository } from 'typeorm';
import * as OpenTok from 'opentok';

import { VideoChatSessionEntity } from './video-chat-session.entity';
import { VideoChatParticipantEntity } from './video-chat-participant.entity';
import { OpentokAdapter } from './opentok.adapter';
import { CreateSessionDto } from './create-session.dto';
import { CreateTokenDto } from './create-token.dto';
import { TerminateSessionDto } from './terminate-session.dto';

@Injectable()
export class VideoChatService {
  constructor(
    private opentok: OpentokAdapter,
    @InjectConnection()
    private connection: Connection,
    @InjectRepository(VideoChatSessionEntity)
    private sessionRepository: Repository<VideoChatSessionEntity>,
    @InjectRepository(VideoChatParticipantEntity)
    private participantRepository: Repository<VideoChatParticipantEntity>,
    private eventPublisher: EventPublisher,
  ) {}

  async createSession(data: CreateSessionDto) {
    const existedSession = await this.findExistedSession(data);

    if (existedSession) {
      let nannyParticipant = await this.participantRepository.findOne({
        where: {
          role: 'nanny',
          userId: data.nanny,
          sessionId: existedSession.id,
        },
      });
      nannyParticipant = this.eventPublisher.mergeObjectContext(
        nannyParticipant,
      );
      nannyParticipant.addedToSession();
      nannyParticipant.commit();
      return existedSession;
    }
    const opentokSession = await this.createOpentokSession();
    const session = this.createSessionEntity(opentokSession);
    const participants = [
      this.createParticipantEntity(data.nanny, 'nanny'),
      this.createParticipantEntity(data.parent, 'parent'),
    ];
    await this.saveSession(session, participants);
    participants.forEach(participant => participant.commit());
    return session;
  }

  private async findExistedSession(data: CreateSessionDto) {
    const parentParticipant = await this.participantRepository.find({
      where: {
        role: 'parent',
        userId: data.parent,
      },
    });
    if (!parentParticipant || !parentParticipant.length) {
      return null;
    }
    const sessionsIds = parentParticipant.map(
      participant => participant.sessionId,
    );

    const nannyParticipant = await this.participantRepository.findOne({
      where: {
        sessionId: In(sessionsIds),
        userId: data.nanny,
        role: 'nanny',
      },
      relations: ['session'],
    });

    if (nannyParticipant) {
      return nannyParticipant.session;
    }
    return null;
  }

  private createSessionEntity(opentokSession: OpenTok.Session) {
    return this.sessionRepository.create({
      openTokSessionId: opentokSession.sessionId,
    });
  }

  private createParticipantEntity(
    userId: string,
    role: VideoChatParticipantEntity['role'],
  ) {
    return this.eventPublisher.mergeObjectContext(
      this.participantRepository.create({ userId, role }),
    );
  }

  async getToken(session, data: CreateTokenDto) {
    const participant = await this.participantRepository.findOne({
      where: {
        sessionId: session,
        userId: data.user,
        role: data.role,
      },
      relations: ['session'],
    });
    if (!participant) {
      throw new ForbiddenException('invalid participant');
    }
    const token = await this.opentok.createToken(
      participant.session.openTokSessionId,
    );
    return {
      session: participant.session.openTokSessionId,
      token,
    };
  }

  private async createOpentokSession() {
    try {
      return await this.opentok.createSession();
    } catch (e) {
      throw new Error(`Failed to create opentok session: ${e.message}`);
    }
  }

  private async saveSession(
    session: VideoChatSessionEntity,
    participants: VideoChatParticipantEntity[],
  ) {
    const runner = this.connection.createQueryRunner();
    const sessionRepository = runner.manager.getRepository(
      VideoChatSessionEntity,
    );
    const participantRepository = runner.manager.getRepository(
      VideoChatParticipantEntity,
    );

    try {
      await runner.startTransaction();
      session = await sessionRepository.save(session);
      participants.forEach(participant => (participant.session = session));
      await participantRepository.save(participants);
      await runner.commitTransaction();
    } catch (e) {
      await runner.rollbackTransaction();
      throw new Error('Failed to save session');
    }
    return session;
  }

  public async terminateSession(sessionId: string, data: TerminateSessionDto) {
    const terminatedBy = await this.participantRepository.findOne({
      where: {
        sessionId,
        userId: data.user,
        role: data.role,
      },
      relations: ['session'],
    });
    if (!terminatedBy) {
      return;
    }
    let participants = await this.participantRepository.find({
      where: {
        session: terminatedBy.session,
      },
    });

    participants = participants.map(participant =>
      this.eventPublisher.mergeObjectContext(participant),
    );
    participants.forEach(participant => participant.terminated());

    try {
      await this.saveSession(terminatedBy.session, participants);
      await this.opentok.sendTerminationSignal(
        terminatedBy.session.openTokSessionId,
      );
      participants.forEach(participant => participant.commit());
    } catch (e) {
      return;
    }
  }
}
