import { Entity, ManyToOne, PrimaryColumn } from 'typeorm';
import { NannyProfile } from './nanny-profile.entity';

@Entity()
export class NannyUser {
  @PrimaryColumn()
  userId: string;

  @ManyToOne(() => NannyProfile, {
    eager: true,
    cascade: ['insert', 'update'],
  })
  profile: NannyProfile;
}
