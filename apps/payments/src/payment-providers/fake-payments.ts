import { CreateMerchantParams, CreatePaymentParams, MerchantVerificationResult, PaymentProvider } from './payment-provider';
import { Merchant } from '../entities/merchant';
import { Payment } from '../entities/payment';
import { Repository } from 'typeorm';
import { MerchantService } from '../services/merchant.service';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { v4 as uuid } from 'uuid';

@Injectable()
export class FakePayments extends PaymentProvider {
  static readonly PROVIDER_NAME = 'fake-provider';

  constructor(
    private merchantService: MerchantService,
    @InjectRepository(Payment)
    private paymentRepository: Repository<Payment>,
  ) {
    super();
  }

  async completePayment(payment: Payment): Promise<Payment> {
    payment.complete();
    return payment;
  }

  async createMerchant(params: CreateMerchantParams): Promise<Merchant> {
    return this.merchantService.createMerchant({
      extId: '',
      isConfirmed: false,
      meta: {},
      paymentProviderId: FakePayments.PROVIDER_NAME,
    });
  }

  async createPayment(params: CreatePaymentParams): Promise<Payment> {
    const payment = this.paymentRepository.create({
      isCompleted: false,
      amount: params.amount,
      currency: params.currency,
      extId: uuid(),
      meta: params.meta || {},
      type: params.type,
    });
    await this.paymentRepository.save(payment);
    return payment;
  }

  async getOnBoardingUrl(account: Merchant): Promise<string> {
    return '';
  }

  async handleVerification(data: any): Promise<MerchantVerificationResult> {
    return {
      extId: '',
      verified: true,
      meta: data,
    };
  }

}
