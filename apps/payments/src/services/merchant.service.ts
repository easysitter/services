import { DeepPartial, Repository } from 'typeorm';
import { Merchant } from '../entities/merchant';

export class MerchantService {
  constructor(
    private merchantRepository: Repository<Merchant>,
  ) {
  }

  findMerchantByExtId(id: string): Promise<Merchant> {
    return this.merchantRepository.findOne({
      where: {
        extId: id,
      },
    });
  }

  findMerchantByUserId(id: string): Promise<Merchant> {
    return this.merchantRepository.findOne({
      where: {
        userId: id,
      },
    });
  }

  createMerchant(params: DeepPartial<Merchant>): Promise<Merchant> {
    const merchant = this.merchantRepository.create(params);
    return this.merchantRepository.save(merchant);
  }
}
