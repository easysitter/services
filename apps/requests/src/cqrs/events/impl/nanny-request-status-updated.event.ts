import { IEvent } from '@nestjs/cqrs';
import { NannyRequest } from '../../../entities/nanny-request.entity';

export class NannyRequestStatusUpdatedEvent implements IEvent {
  constructor(public readonly request: NannyRequest) {
  }
}
