import { NannyRequest } from '../../../entities/nanny-request.entity';
import { ICommand } from '@nestjs/cqrs';

export class DealProposedCommand implements ICommand {
  constructor(public readonly request: NannyRequest) {}
}
