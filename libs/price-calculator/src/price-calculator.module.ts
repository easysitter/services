import { DynamicModule, Module, Provider } from '@nestjs/common';
import { CalculatorRates } from './calculator-rates';
import { PriceCalculator } from './price-calculator';
import { PlainRate } from './rates/plain.rate';
import { DayNightSplitCalculator } from './calculator/day-night.calculator';
import { PlainCalculator } from './calculator/plain.calculator';
import { ExtraChildrenRate } from './rates/extra-children.rate';
import { Rates } from './rates/rates';
import { CalculatorTracer, NoopTracer, StackTracer } from './utils/calculator-tracer';
import { Calculator } from './calculator/calculator';
import { VisitTimeCalculator } from './calculator/visit-time.calculator';

const CALCULATOR_CONFIG = Symbol.for('CONFIG');
const DAY_RATE = Symbol.for('DAY_RATE');
const NIGHT_RATE = Symbol.for('NIGHT_RATE');

const staticProviders: Provider[] = [
  {
    provide: DAY_RATE,
    useFactory: (c: PriceCalculatorConfig) => new ExtraChildrenRate(new PlainRate(c.dayRate)),
    inject: [CALCULATOR_CONFIG],
  },
  {
    provide: NIGHT_RATE,
    useFactory: (c: PriceCalculatorConfig) => new ExtraChildrenRate(new PlainRate(c.nightRate)),
    inject: [CALCULATOR_CONFIG],
  },
  {
    provide: CalculatorTracer,
    useClass: StackTracer,
  },
  {
    provide: PriceCalculator,
    useFactory: (dayRate: Rates, nightRate: Rates, tracer: CalculatorTracer) => {
      const dayCalculator = new PlainCalculator(dayRate);
      const nightCalculator = new PlainCalculator(nightRate);
      const calculator = new DayNightSplitCalculator(dayCalculator, nightCalculator);
      const overtime: Calculator = new VisitTimeCalculator(calculator, 30, true);
      return new PriceCalculator(calculator, overtime, tracer);
    },
    inject: [DAY_RATE, NIGHT_RATE, CalculatorTracer],
  },
  {
    provide: CalculatorRates,
    useFactory: (dayRate: Rates, nightRate: Rates) => {
      return new CalculatorRates(dayRate, nightRate);
    },
    inject: [DAY_RATE, NIGHT_RATE],
  },
];

@Module({})
export class PriceCalculatorModule {
  static configure(config: PriceCalculatorConfig): DynamicModule {
    return {
      module: PriceCalculatorModule,
      providers: [
        {
          provide: CALCULATOR_CONFIG,
          useValue: config,
        },
        ...staticProviders,
      ],
      exports: [
        CalculatorRates,
        PriceCalculator,
      ],
    };
  }

  static configureAsync(configProvider: () => Promise<PriceCalculatorConfig>): DynamicModule {
    return {
      module: PriceCalculatorModule,
      providers: [
        {
          provide: CALCULATOR_CONFIG,
          useFactory: async () => await configProvider(),
        },
        ...staticProviders,
      ],
      exports: [
        CalculatorRates,
        PriceCalculator,
      ],
    };
  }
}

export interface PriceCalculatorConfig {
  dayRate: number;
  nightRate: number;
}
