import { ChatMessage, ChatModel } from '../../models';
import { Observable } from 'rxjs';

export interface ChatNotificationsPort {
  notifyNewChat(chatModel: ChatModel): Observable<void>;

  notifyNewMessage(chat: ChatModel, message: ChatMessage): Observable<void>;
}
