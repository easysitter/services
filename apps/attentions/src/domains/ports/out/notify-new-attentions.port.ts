import { AttentionGroupEntity } from '../..';

export interface NotifyNewAttentionsPort {
  notify(attentionGroup: AttentionGroupEntity): Promise<void>;
}
