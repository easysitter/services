import { IQuery } from '@nestjs/cqrs';
import { SearchDto } from '../dto';

export class FindDealsQuery implements IQuery {
  constructor(public readonly params: SearchDto) {}
}
