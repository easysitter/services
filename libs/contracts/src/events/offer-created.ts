import { EventBusEvent } from '../event-bus-event';

interface OfferCreatedPayload {
  deal: number;
  request: string;
  parent: number;
  nanny: number;
  initiator: 'parent' | 'nanny';
}

export class OfferCreated implements EventBusEvent<OfferCreatedPayload> {
  static readonly eventName = 'offer_created';

  public readonly name = OfferCreated.eventName;

  constructor(
    public readonly payload: OfferCreatedPayload,
  ) {
  }
}
