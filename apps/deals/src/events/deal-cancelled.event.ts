import { DealEvent } from './deal.event';

export class DealCancelledEvent extends DealEvent {}
