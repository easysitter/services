import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServiceSubscription } from '../entities/service-subscription';
import { Payment } from '../entities/payment';
import { ServiceSubscriptionService } from './service-subscription.service';
import { PaymentProvider } from '../payment-providers/payment-provider';
import { FakePayments } from '../payment-providers/fake-payments';
import { MerchantService } from './merchant.service';
import { Connection } from 'typeorm';
import { unlinkSync } from 'fs';

export async function setUpTestingModule(dbName: string): Promise<TestingModule> {
  return await Test.createTestingModule({
    imports: [
      TypeOrmModule.forRoot({
        type: 'sqlite',
        database: dbName,
        entities: [ServiceSubscription, Payment],
        retryAttempts: 0,
        synchronize: true,
      }),
      TypeOrmModule.forFeature([ServiceSubscription, Payment]),
    ],
    providers: [
      ServiceSubscriptionService,
      { provide: PaymentProvider, useClass: FakePayments },
      MerchantService,
    ],
  }).compile();
}

export async function tearDownTestingModule(module: TestingModule, dbName: string): Promise<void> {
  const connection = module.get<Connection>(Connection);
  await connection.close();
  unlinkSync(dbName);
}
