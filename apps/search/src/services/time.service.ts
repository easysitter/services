export class TimeService {
  private startWorkingDayAt = 6;

  parse(timeStr: string) {
    timeStr = timeStr.split('+')[0];
    const [hourSrt, minuteSrt] = timeStr.split(':');
    const hour = Number(hourSrt);
    const minute = Number(minuteSrt);
    return this.normalize(hour, minute);
  }

  normalize(hour, minute) {
    if (hour > 23) {
      hour = Math.floor(hour / 24);
    }
    // if (hour < this.startWorkingDayAt) {
    //   hour = hour + 24;
    // }

    return hour * 60 + minute;
  }
}
