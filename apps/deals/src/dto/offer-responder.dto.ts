import { Role } from '../types/role.type';
import { IsDefined, IsIn } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class OfferResponderDto {
  @ApiProperty({
    enum: ['parent', 'nanny'],
  })
  @IsDefined()
  @IsIn(['parent', 'nanny'])
  type: Role;
  @ApiProperty()
  @IsDefined()
  user: number;
}
