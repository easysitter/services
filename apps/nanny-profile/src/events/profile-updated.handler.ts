import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { ProfileUpdatedEvent } from './profile-updated.event';
import { Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { NannyProfileUpdated, ScheduleUpdated } from '@event-bus/contracts';

@EventsHandler(ProfileUpdatedEvent)
export class ProfileUpdatedHandler implements IEventHandler<ProfileUpdatedEvent> {
  constructor(
    @Inject('EVENT_BUS')
    private readonly events: ClientProxy,
  ) {
  }

  handle(event: ProfileUpdatedEvent): any {
    const scheduleUpdatedBusEvent = this.createScheduleUpdatedBusEvent(event);
    this.events.emit(scheduleUpdatedBusEvent.name, scheduleUpdatedBusEvent.payload);

    const profileUpdatedBusEvent = this.createProfileUpdatedBusEvent(event);
    this.events.emit(profileUpdatedBusEvent.name, profileUpdatedBusEvent.payload);
  }

  private createScheduleUpdatedBusEvent(event: ProfileUpdatedEvent) {
    return new ScheduleUpdated({
      user: event.profile.id,
      items: event.profile.schedule.map(item => ({
        day: item.day,
        from: item.from,
        to: item.to,
      })),
    });
  }

  private createProfileUpdatedBusEvent(event: ProfileUpdatedEvent) {
    return new NannyProfileUpdated({
      id: event.profile.id,
      acceptTrial: event.profile.trialEnabled,
      currency: event.profile.price.currency,
      rate: event.profile.price.value,
      allowedChildren: event.profile.ageCategories,
      maxChildrenCount: event.profile.maxChildrenCount,
    });
  }
}
