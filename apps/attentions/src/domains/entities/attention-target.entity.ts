export type AttentionTargetType = string;

export class AttentionTargetEntity {
  protected constructor(
    public readonly refType: AttentionTargetType,
    public readonly refId: string,
    public readonly data?: any,
  ) {
  }

  static of(refType: AttentionTargetType, refId: string, data?: any) {
    return new AttentionTargetEntity(refType, refId, data);
  }

  isTypeOf(refType: AttentionTargetType) {
    return this.refType === refType;
  }

  equals(target: AttentionTargetEntity) {
    return this.isTypeOf(target.refType) && this.refId === target.refId;
  }
}
