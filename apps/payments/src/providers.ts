import { Provider } from '@nestjs/common';
import { CheckoutAPI, Client, Config } from '@adyen/api-library';

export const PAYU_BASE_URL = Symbol('PAYU_BASE_URL');
export const PAYU_NOTIFY_URL = Symbol('PAYU_NOTIFY_URL');
export const PAYU_POSID = Symbol('PAYU_POSID');
export const PAYU_OAUTH_CLIENT_ID = Symbol('PAYU_OAUTH_CLIENT_ID');
export const PAYU_OAUTH_CLIENT_SECRET = Symbol('PAYU_OAUTH_CLIENT_SECRET');

export const ADYEN_CONFIG = Symbol('ADYEN_CONFIG');

export const providers: Provider[] = [
  {
    provide: PAYU_BASE_URL,
    useValue: process.env.PAYU_BASE_URL,
  },
  {
    provide: PAYU_NOTIFY_URL,
    useValue: process.env.PAYU_NOTIFY_URL,
  },
  {
    provide: PAYU_POSID,
    useValue: process.env.PAYU_POSID.trim(),
  },
  {
    provide: PAYU_OAUTH_CLIENT_ID,
    useValue: process.env.PAYU_OAUTH_CLIENT_ID.trim(),
  },
  {
    provide: PAYU_OAUTH_CLIENT_SECRET,
    useValue: process.env.PAYU_OAUTH_CLIENT_SECRET.trim(),
  },
  {
    provide: Config, useFactory: () => new Config({
      apiKey: process.env.ADYEN_API_KEY,
      merchantAccount: process.env.ADYEN_MERCHANT_ACCOUNT,
      // username: process.env.ADYEN_USERNAME,
      // password: process.env.ADYEN_PASSWORD,
      environment: process.env.ADYEN_ENVIRONMENT as Environment || 'TEST',
    }),
  },
  {
    provide: Client,
    useFactory: (config: Config) => new Client({ config }),
    inject: [Config],
  },
  {
    provide: CheckoutAPI,
    useFactory: (client: Client) => new CheckoutAPI(client),
    inject: [Client],
  },
];
