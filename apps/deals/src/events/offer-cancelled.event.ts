import { DealEvent } from './deal.event';

export class OfferCancelledEvent extends DealEvent {}
