import { HttpModule, Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { LoggerModule } from 'nestjs-pino/dist';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DealAttentionsController } from './deal-attentions.controller';
import {
  CreateDealRemindersService,
  CreateDealRemindersUseCase,
  LoadAttentionsPort,
  LoadDealsToRemindPort,
  NotifyNewAttentionsPort,
  SaveAttentionsPort,
} from './domains';
import { DealsService } from './services/deals.service';
import { AttentionsService } from './services/attentions.service';
import { FindAttentionsByTargetTypeUseCase } from './domains/ports/in/find-attentions-by-target-type.use-case';
import { FindAttentionsByTargetTypeService } from './domains/services/find-attentions-by-target-type.service';
import { Attention } from './db/attention.db-entity';
import {
  COMING_DEALS_URL,
  EVENT_BUS,
  LOAD_ATTENTIONS_ADAPTER,
  LOAD_DEALS_TO_REMIND_ADAPTER, LOAD_OUTDATED_REQUEST_TO_REMIND_ADAPTER,
  NOTIFY_NEW_ATTENTIONS_ADAPTER, OUTDATED_REQUEST_URL,
  SAVE_ATTENTIONS_ADAPTER,
} from './injection.tokens';
import { PublishAttentionsService } from './services/publish-attentions.service';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { getClientConfig } from '@utils/transport-connector';
import { OutdatedRequestAttentionsController } from './outdated-request-attentions.controller';
import { CreateOutdatedRequestRemindersUseCase } from './domains/ports/in/create-outdated-request-reminders.use-case';
import { LoadOutdatedRequestToRemindPort } from './domains/ports/out/load-outdated-request-to-remind.port';
import { CreateOutdatedRequestRemindersService } from './domains/services/create-outdated-request-reminders.service';
import { OutdatedRequestService } from './services/outdated-request.service';

@Module({
  imports: [
    CqrsModule,
    HttpModule,
    LoggerModule.forRoot({
      pinoHttp: {
        level: process.env.NODE_ENV === 'dev' ? 'error' : 'info',
        prettyPrint: process.env.NODE_ENV === 'dev',
      },
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      database: process.env.DB_NAME,
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      username: process.env.DB_USER_NAME,
      password: process.env.DB_USER_PASSWORD,
      schema: process.env.DB_SCHEMA || 'public',
      entities: [Attention],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([Attention]),
    ClientsModule.register([
      {
        name: EVENT_BUS as unknown as string,
        transport: Transport.KAFKA,
        options: {
          client: {
            ...getClientConfig(),
            clientId: 'attentions-service',
          },
          producer: {
            allowAutoTopicCreation: false,
            idempotent: true,
          },
        },
      },
    ]),
  ],
  controllers: [
    DealAttentionsController,
    OutdatedRequestAttentionsController,
  ],
  providers: [
    DealsService,
    OutdatedRequestService,
    AttentionsService,
    PublishAttentionsService,
    {
      provide: CreateDealRemindersUseCase,
      useFactory: (
        loadDealsToRemindAdapter: LoadDealsToRemindPort,
        saveAttentionsAdapter: SaveAttentionsPort,
        loadAttentionsAdapter: LoadAttentionsPort,
        notifyNewAttentionsAdapter: NotifyNewAttentionsPort,
      ) => {
        return new CreateDealRemindersService(
          loadDealsToRemindAdapter,
          saveAttentionsAdapter,
          loadAttentionsAdapter,
          notifyNewAttentionsAdapter,
        );
      },
      inject: [
        LOAD_DEALS_TO_REMIND_ADAPTER,
        SAVE_ATTENTIONS_ADAPTER,
        LOAD_ATTENTIONS_ADAPTER,
        NOTIFY_NEW_ATTENTIONS_ADAPTER,
      ],
    },
    {
      provide: CreateOutdatedRequestRemindersUseCase,
      useFactory: (
        loadOutdatedRequestToRemind: LoadOutdatedRequestToRemindPort,
        saveAttentionsAdapter: SaveAttentionsPort,
        loadAttentionsAdapter: LoadAttentionsPort,
        notifyNewAttentionsAdapter: NotifyNewAttentionsPort,
      ) => {
        return new CreateOutdatedRequestRemindersService(
          loadOutdatedRequestToRemind,
          saveAttentionsAdapter,
          loadAttentionsAdapter,
          notifyNewAttentionsAdapter,
        );
      },
      inject: [
        LOAD_OUTDATED_REQUEST_TO_REMIND_ADAPTER,
        SAVE_ATTENTIONS_ADAPTER,
        LOAD_ATTENTIONS_ADAPTER,
        NOTIFY_NEW_ATTENTIONS_ADAPTER,
      ],
    },
    {
      provide: FindAttentionsByTargetTypeUseCase,
      useFactory: (
        loadAttentionsPort: LoadAttentionsPort,
      ) => {
        return new FindAttentionsByTargetTypeService(
          loadAttentionsPort,
        );
      },
      inject: [
        LOAD_ATTENTIONS_ADAPTER,
      ],
    },
    {
      provide: LOAD_DEALS_TO_REMIND_ADAPTER,
      useExisting: DealsService,
    },
    {
      provide: LOAD_OUTDATED_REQUEST_TO_REMIND_ADAPTER,
      useExisting: OutdatedRequestService,
    },
    {
      provide: SAVE_ATTENTIONS_ADAPTER,
      useExisting: AttentionsService,
    },
    {
      provide: LOAD_ATTENTIONS_ADAPTER,
      useExisting: AttentionsService,
    },
    {
      provide: NOTIFY_NEW_ATTENTIONS_ADAPTER,
      useExisting: PublishAttentionsService,
    },
    {
      provide: COMING_DEALS_URL,
      useValue: process.env.COMING_DEALS_URL,
    },
    {
      provide: OUTDATED_REQUEST_URL,
      useValue: process.env.OUTDATED_REQUEST_URL,
    },
  ],
})
export class AttentionsModule {
}
