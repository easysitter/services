import { Body, Controller, Get, Post } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { from, Observable } from 'rxjs';
import { map, mergeMap, toArray } from 'rxjs/operators';
import { DistrictService } from './district.service';
import { ZipCheckResult } from './zip-check-result';
import { District } from './district';
import { ZipCheckInput } from './zip-check.input';

@Controller('districts')
@ApiTags('district')
export class DistrictController {
  constructor(
    private districtService: DistrictService,
  ) {
  }

  @Get()
  @ApiOkResponse({
    isArray: true,
    type: District,
  })
  getDistricts(): Observable<District[]> | Promise<District[]> {
    return this.districtService.getDistricts().pipe(
      map(items => items.map(({ id, title }) => ({ id, title }))),
    );
  }

  @Post('check')
  @ApiOkResponse({
    isArray: true,
    type: ZipCheckResult,
  })
  checkZipInDistricts(
    @Body() { zip, districtIds }: ZipCheckInput,
  ): Observable<ZipCheckResult[]> | Promise<ZipCheckResult[]> | ZipCheckResult[] {
    return from(districtIds)
      .pipe(
        mergeMap(districtId => this.districtService.check(zip, districtId)),
        toArray(),
      );
  }

}
