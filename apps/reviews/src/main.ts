import { NestFactory } from '@nestjs/core';
import { ReviewsModule } from './reviews.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(ReviewsModule);
  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);
  const options = new DocumentBuilder()
    .setTitle('Reviews api')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  const port = process.env.HTTP_PORT || 3333;
  SwaggerModule.setup('/docs', app, document);
  await app.listen(port, () => {
    console.log('Listening at http://localhost:' + port + '/' + globalPrefix);
    console.log('Listening at http://localhost:' + port + '/docs');
  });
}
bootstrap();
