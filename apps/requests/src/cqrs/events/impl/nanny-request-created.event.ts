import { NannyRequest } from '../../../entities/nanny-request.entity';

export class NannyRequestCreatedEvent {
  constructor(public readonly request: NannyRequest) {}
}
