import { HttpModule, Module } from '@nestjs/common';
import { providers } from './providers';
import { PaymentProvider } from './payment-providers/payment-provider';
import { PauyProvider } from './payment-providers/pauy';
import { MerchantService } from './services/merchant.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Merchant } from './entities/merchant';
import { Payment } from './entities/payment';
import { ServiceSubscription } from './entities/service-subscription';
import { ServiceSubscriptionService } from './services/service-subscription.service';
import { SubscriptionController } from './controllers/subscription.controller';
import { PaymentController } from './controllers/payment.controller';
import { PaymentService } from './services/payment.service';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { getClientConfig } from '@utils/transport-connector';
import { LoggerModule } from 'nestjs-pino';
import { UserDealsController } from './deal-payments/user-deals.controller';
import { DealPaymentService } from './deal-payments/deal-payment.service';
import { DealPayment } from './deal-payments/deal-payment';
import { SystemDealsController } from './deal-payments/system-deals.controller';
import { CqrsModule } from '@nestjs/cqrs';
import { DealPaymentCancelledHandler } from './deal-payments/event-handlers/deal-payment-cancelled.handler';
import { PriceCalculatorModule } from '@utils/price-calculator';
import { TimeParserModule } from '@utils/time-parser';

@Module({
  imports: [
    LoggerModule.forRoot({
      pinoHttp: {
        level: process.env.NODE_ENV === 'dev' ? 'debug' : 'info',
        prettyPrint: process.env.NODE_ENV === 'dev',
      },
    }),
    HttpModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      database: process.env.DB_NAME,
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      username: process.env.DB_USER_NAME,
      password: process.env.DB_USER_PASSWORD,
      schema: process.env.DB_SCHEMA || 'public',
      entities: [Merchant, Payment, ServiceSubscription, DealPayment],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([Merchant, Payment, ServiceSubscription, DealPayment]),
    ClientsModule.register([{
      name: 'EVENT_BUS',
      transport: Transport.KAFKA,
      options: {
        client: {
          ...getClientConfig(),
          clientId: 'payments-service',
        },
        producer: {
          allowAutoTopicCreation: true,
          idempotent: false,
        },
      },
    }]),
    CqrsModule,
    PriceCalculatorModule.configureAsync(async () => {
      return {
        dayRate: +process.env.PAYMENT_DAYTIME_RATE,
        nightRate: +process.env.PAYMENT_NIGHTTIME_RATE,
      };
    }),
    TimeParserModule,
  ],
  controllers: [
    PaymentController,
    SubscriptionController,
    UserDealsController,
    SystemDealsController,
  ],
  providers: [
    ...providers,
    MerchantService,
    DealPaymentService,
    { provide: PaymentProvider, useClass: PauyProvider },
    ServiceSubscriptionService,
    PaymentService,
    DealPaymentCancelledHandler,
  ],
})
export class PaymentModule {
}
