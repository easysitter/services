import { Body, Controller, Get, Post, Query, UsePipes, ValidationPipe } from '@nestjs/common';
import { ReviewService } from './review.service';
import { CreateReviewDto } from './create-review.dto';
import { SearchReviewDto } from './search-review.dto';
import { DuplicateError } from './duplicate.error';
import { ApiResponse } from '@nestjs/swagger';
import { ReviewDto } from './review.dto';

@Controller('/reviews')
@UsePipes(new ValidationPipe({ transform: true }))
export class ReviewController {
  constructor(
    private reviewService: ReviewService,
  ) {
  }

  @Post()
  @ApiResponse({
    status: 201,
    type: ReviewDto,
  })
  async create(
    @Body() data: CreateReviewDto,
  ) {
    try {
      return await this.reviewService.create(data).toPromise();
    } catch (error) {
      if (error instanceof DuplicateError) {
        return error.data;
      } else {
        throw error;
      }
    }
  }

  @Get()
  @ApiResponse({
    status: 200,
    type: ReviewDto,
    isArray: true,
  })
  search(
    @Query() params: SearchReviewDto,
  ) {
    return this.reviewService.find(params).toPromise();
  }
}
