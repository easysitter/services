import { ChatDbEntity } from '../entities/chat.db.entity';
import { ChatModel } from '../../../../domain/models';
import { MessageMapper } from './message.mapper';

export class ChatMapper {
  toDomain(entity: ChatDbEntity): ChatModel {
    if (!entity) {
      return null;
    }
    const messageMapper = new MessageMapper();
    return new ChatModel(
      entity.id,
      entity.users,
      entity.meta,
      entity.lastMessage && messageMapper.toDomain(entity.lastMessage),
    );
  }

  fromDomain(model: ChatModel): ChatDbEntity {
    if (!model) {
      return null;
    }
    const entity = new ChatDbEntity();

    const messageMapper = new MessageMapper();
    entity.id = model.id;
    entity.users = model.users;
    entity.meta = model.meta;
    entity.lastMessage = model.lastMessage && messageMapper.fromDomain(model.lastMessage);
    return entity;
  }
}
