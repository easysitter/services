import { DealEvent } from './deal.event';
import { Deal } from '../models';

export class DealExceptionEvent extends DealEvent {
  constructor(
    deal: Deal,
    public action: string,
    public readonly exception: Error,
  ) {
    super(deal);
  }
}
