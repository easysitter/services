import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { RefuseRequestCommand } from '../impl';

import { NannyRequest } from '../../../entities/nanny-request.entity';

@CommandHandler(RefuseRequestCommand)
export class RefuseRequestCommandHandler
  implements ICommandHandler<RefuseRequestCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    @InjectRepository(NannyRequest)
    private readonly nannyRequest: Repository<NannyRequest>,
  ) {}

  async execute(command: RefuseRequestCommand): Promise<void> {
    const request = this.publisher.mergeObjectContext(command.request);
    request.refuse();
    await this.nannyRequest.save(request);
    request.commit();
  }
}
