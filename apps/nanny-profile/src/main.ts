import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useLogger(app.get(Logger));
  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);
  const options = new DocumentBuilder()
    .setTitle('Nanny profile api')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  const port = process.env.HTTP_PORT || 3333;
  SwaggerModule.setup('/docs', app, document);
  await app.listen(port, () => {
    console.log('Listening at http://localhost:' + port + '/' + globalPrefix);
    console.log('Swagger at http://localhost:' + port + '/docs');
  });
}
bootstrap();
