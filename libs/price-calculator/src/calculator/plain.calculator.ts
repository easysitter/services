// tslint:disable-next-line:max-classes-per-file
import { differenceInMinutes } from 'date-fns';
import { Calculator } from './calculator';
import { Rates } from '../rates/rates';
import { CalculatorParams } from './calculator-params';
import { CalculatorTracer } from '../utils/calculator-tracer';

export class PlainCalculator extends Calculator {
  constructor(private rate: Rates) {
    super();
  }

  async getPrice(
    deal: CalculatorParams,
    tracer: CalculatorTracer,
  ): Promise<number> {
    const rate = await this.rate.getRate(deal);
    const hours = differenceInMinutes(deal.from, deal.to) / 60;
    // const hours = 0;

    const price = Math.abs(hours) * rate;
    tracer.trace({ from: deal.from, to: deal.to, price });
    return price;
  }
}
