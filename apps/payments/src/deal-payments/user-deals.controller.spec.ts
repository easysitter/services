import { DealConcluded } from '@event-bus/contracts';
import { Test } from '@nestjs/testing';
import { PriceCalculatorModule } from '@utils/price-calculator';
import { UserDealsController } from './user-deals.controller';
import { DealPaymentService } from './deal-payment.service';
import { TimeParserModule } from '@utils/time-parser';
import { Logger } from '@nestjs/common';
import { TestingLogger } from '@nestjs/testing/services/testing-logger.service';
import { CreateDealPaymentCommand } from './commands/create-deal-payment.command';

describe('User deals controller', () => {
  describe(DealConcluded.eventName, () => {
    let ctrl: UserDealsController;
    let createPayment: jest.Mock;
    beforeEach(async () => {
      createPayment = jest.fn((cmd) => {
        return null;
      });

      const app = await Test.createTestingModule({
        imports: [
          PriceCalculatorModule.configureAsync(async () => {
            return {
              dayRate: 34,
              nightRate: 46,
            };
          }),
          TimeParserModule,
        ],
        providers: [
          { provide: Logger, useClass: TestingLogger },
          UserDealsController,
          {
            provide: DealPaymentService,
            useValue: {
              createPayment,
            },
          },
        ],
      }).compile();

      ctrl = app.get(UserDealsController);
    });
    it('should handle deal', async () => {
      const data: DealConcluded['payload'] = {
        initiator: 'parent',
        nanny: 1,
        parent: 27,
        date: new Date('2020-12-18T00:00:00.000Z'),
        fromTime: '20:00+01',
        toTime: '02:00+01',
        children: [
          0,
        ],
        id: 213,
        request: '381',
      };
      await ctrl.onDealConcluded({
        value: data,
      });
      expect(createPayment).toBeCalledWith(CreateDealPaymentCommand.of(
        `${data.id}`,
        `${data.parent}`,
        264,
        'PLN',
      ));
    });
  });
});
