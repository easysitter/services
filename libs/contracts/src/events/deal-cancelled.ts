import { EventBusEvent } from '../event-bus-event';

interface DealCancelledPayload {
  id: number;
  date: Date;
  fromTime: string;
  toTime: string;
  request: string;
  nanny: number;
  parent: number;
  initiator: 'parent' | 'nanny';
}

export class DealCancelled implements EventBusEvent<DealCancelledPayload> {
  static eventName = 'deal_cancelled';

  readonly name = DealCancelled.eventName;

  constructor(
    public readonly payload: DealCancelledPayload,
  ) {
  }
}
