import { IsNotEmpty, IsOptional, ValidateIf } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class SearchReviewDto {
  @IsNotEmpty()
  @ValidateIf((data) => data.refId)
  @ApiProperty({
    required: false,
  })
  refType: string;

  @IsOptional()
  @ApiProperty({
    required: false,
  })
  refId: string;

  @IsNotEmpty()
  @ValidateIf((data) => data.userId)
  @ApiProperty({
    required: false,
  })
  userType: string;

  @IsOptional()
  @ApiProperty({
    required: false,
  })
  userId: string;
}
