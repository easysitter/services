import { ICommand } from '@nestjs/cqrs';

export type CommandCtor<Data extends any, Command extends ICommand> = (
  data: Data,
  ...rest: any[]
) => Command;

export interface ActionCommandMap<Data extends any, Command extends ICommand> {
  [key: string]: CommandCtor<Data, Command>;
}
