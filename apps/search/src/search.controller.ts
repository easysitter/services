import { Body, Controller, Get, HttpCode, HttpStatus, Param, Post, Put, UsePipes, ValidationPipe } from '@nestjs/common';
import { SearchService } from './services/search.service';
import { SearchParamsDTO } from './search-params.dto';

@Controller('/search')
export class SearchController {
  constructor(private searchService: SearchService) {}

  @Post('')
  @UsePipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
      // forbidNonWhitelisted: true,
    }),
  )
  @HttpCode(HttpStatus.OK)
  search(@Body() params: SearchParamsDTO) {
    return this.searchService.search(params);
  }

  @Get('results/:resultId')
  @HttpCode(HttpStatus.OK)
  getResult(
    @Param('resultId') resultId: string,
  ) {
    return this.searchService.getResult(resultId);
  }

  @Put('results/:resultId')
  @HttpCode(HttpStatus.OK)
  async approve(
    @Param('resultId') resultId: string,
    @Body('nannies') nannies: number[],
  ) {
    const result = await this.searchService.getResult(resultId);
    return await this.searchService.approve(result, nannies);
  }
}
