import { Injectable } from '@nestjs/common';
import { ScheduleService } from './schedule.service';
import { ReservationsService, TimeRange } from './reservations.service';
import { TimeService } from './time.service';
import { SearchParamsDTO } from '../search-params.dto';
import { NannyService } from './nanny.service';
import { InjectRepository } from '@nestjs/typeorm';
import { SearchResult } from '../entities/search-result.entity';
import { Repository } from 'typeorm';

@Injectable()
export class SearchService {
  constructor(
    @InjectRepository(SearchResult)
    private searchResultRepository: Repository<SearchResult>,
    private readonly schedule: ScheduleService,
    private readonly reservations: ReservationsService,
    private readonly nannies: NannyService,
    private readonly time: TimeService,
  ) {
  }

  async search(params: SearchParamsDTO) {
    const { timeStart, timeEnd, day } = this.prepareSearchParams(params);
    let users = await this.checkSchedule(day, timeStart, timeEnd);

    users = await this.checkReservations(
      params.date,
      { start: timeStart, end: timeEnd },
      users,
    );

    const found = await this.nannies.find({
      users,
      children: params.children,
      trial: params.trial,
    });

    const result = this.searchResultRepository.create({
      ...params,
      nannies: found.map(({ id, rate }) => ({
        id, rate, isApproved: true,
      })),
    });

    return await this.searchResultRepository.save(result);
  }

  private async checkReservations(date: Date, time: TimeRange, users: number[]) {
    const usersWithReserve = await this.reservations.getReserved({
      date,
      time,
      users,
    });
    return users.filter(user => !usersWithReserve.includes(user));
  }

  private async checkSchedule(day: number, timeStart, timeEnd) {
    return await this.schedule.findAvailable({
      day,
      timeStart,
      timeEnd,
    });
  }

  private prepareSearchParams({ fromTime, toTime, date }: SearchParamsDTO) {
    const timeStart = this.time.parse(fromTime);
    const timeEnd = this.time.parse(toTime);
    const day = date.getUTCDay();
    return { timeStart, timeEnd, day };
  }

  getResult(resultId: string) {
    return this.searchResultRepository.findOne(resultId);
  }

  async approve(searchResult: SearchResult, nannies: number[]) {
    searchResult.nannies = searchResult.nannies.map(nanny => {
      return {
        ...nanny,
        isApproved: nannies.includes(nanny.id),
      };
    });
    return this.searchResultRepository.save(searchResult);
  }
}
