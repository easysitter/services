import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Reserve {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  user: number;

  @Column({
    type: 'date',
  })
  date: Date;

  @Column()
  start: number;

  @Column()
  end: number;

  @Column()
  @Index()
  day: number;

  @Column()
  reason: string;

  @Column()
  reasonRef: string;
}
