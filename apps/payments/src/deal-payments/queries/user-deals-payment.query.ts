export class UserDealsPaymentsQuery {
  constructor(
    public readonly user: string,
  ) {
  }

  static of(user) {
    return new UserDealsPaymentsQuery(user);
  }
}
