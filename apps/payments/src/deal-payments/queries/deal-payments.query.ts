export class DealPaymentsQuery {
  constructor(
    public readonly deal: string,
    public readonly user: string,
  ) {
  }

  static of(
    deal: string,
    user: string,
  ) {
    return new DealPaymentsQuery(deal, user);
  }
}
