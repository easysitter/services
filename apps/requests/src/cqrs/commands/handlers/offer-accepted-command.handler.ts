import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Not, Repository, Transaction, TransactionRepository } from 'typeorm';

import { OfferAcceptedCommand } from '../impl';

import { NannyRequest } from '../../../entities/nanny-request.entity';
import { ParentRequest } from '../../../entities/parent-request.entity';

@CommandHandler(OfferAcceptedCommand)
export class OfferAcceptedCommandHandler
  implements ICommandHandler<OfferAcceptedCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    @InjectRepository(NannyRequest)
    private readonly nannyRequestRepo: Repository<NannyRequest>,
    @InjectRepository(ParentRequest)
    private readonly parentRequestRepo: Repository<ParentRequest>,
  ) {
  }

  async execute(command: OfferAcceptedCommand): Promise<void> {
    const request = this.publisher.mergeObjectContext(command.request);

    let parentRequest: ParentRequest = await this.nannyRequestRepo
      .createQueryBuilder('nr')
      .relation(NannyRequest, 'parentRequest')
      .of(request)
      .loadOne();
    request.parentRequest = parentRequest;
    parentRequest = this.publisher.mergeObjectContext(parentRequest);
    await this._execute(request, parentRequest);
    request.commit();
    parentRequest.commit();
  }

  @Transaction({
    isolation: 'READ COMMITTED',
  })
  private async _execute(
    request: NannyRequest,
    parentRequest: ParentRequest,
    @TransactionRepository(NannyRequest)
      nannyRequestRepo?: Repository<NannyRequest>,
    @TransactionRepository(ParentRequest)
      parentRequestRepo?: Repository<ParentRequest>,
  ) {
    request.offerAccepted();
    await nannyRequestRepo.save(request);
    await this.suppressRequestsForOtherNannies(parentRequest, request.user, nannyRequestRepo);
    await this.suppressOtherParentRequests(parentRequest, request.user, nannyRequestRepo, parentRequestRepo);
    parentRequest.deal();
    await parentRequestRepo.save(parentRequest);
  }

  async suppressRequestsForOtherNannies(
    parentRequest: ParentRequest,
    nanny: NannyRequest['user'],
    nannyRequestRepo?: Repository<NannyRequest>,
  ) {
    let requests = await this.nannyRequestRepo.find({
      where: {
        parentRequest,
        user: Not(nanny),
        isActive: true,
      },
    });
    if (!requests.length) {
      return;
    }
    requests = requests.map((req) => this.publisher.mergeObjectContext(req));
    requests.forEach(req => req.suspend());
    await nannyRequestRepo.save(requests);
    requests.forEach(req => req.commit());
  }

  private async suppressOtherParentRequests(
    parentRequest: ParentRequest,
    nanny: number,
    nannyRequestRepo: Repository<NannyRequest>,
    parentRequestRepo: Repository<ParentRequest>,
  ) {
    const alias = 'parent';
    const query = parentRequestRepo.createQueryBuilder(alias)
      .where(`'[${parentRequest.startTs.toISOString()},${parentRequest.endTs.toISOString()}]'::tsrange && CONCAT('[', "${alias}"."startTs", ',', "${alias}"."endTs", ']')::tsrange `)
      .andWhere(`"${alias}"."id"!=:id`, { id: parentRequest.id });

    const parentRequests = await query.getMany();
    if (parentRequests.length) {
      let requests = await nannyRequestRepo.find({
        where: {
          user: nanny,
          isActive: true,
          parentRequest: In(parentRequests.map(pr => pr.id)),
        },
      });

      requests = requests.map((req) => this.publisher.mergeObjectContext(req));
      requests.forEach(req => req.suspend());
      await nannyRequestRepo.save(requests);
      requests.forEach(req => req.commit());
    }
  }
}
