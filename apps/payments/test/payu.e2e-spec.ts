import { Test, TestingModule } from '@nestjs/testing';
import { HttpModule, HttpService } from '@nestjs/common';
import { pluck } from 'rxjs/operators';
import { v4 as uuid } from 'uuid';

describe('PayU', () => {
  const POS_ID = process.env.PAYU_POSID;
  const OAUTH_CLIENT_ID = process.env.PAYU_OAUTH_CLIENT_ID;
  const OAUTH_CLIENT_SECRET = process.env.PAYU_OAUTH_CLIENT_SECRET;

  let module: TestingModule;
  let http: HttpService;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [HttpModule.register({
        baseURL: 'https://secure.snd.payu.com',
      })],
    }).compile();
    http = module.get<HttpService>(HttpService);
  });
  describe('Authentication', () => {

    it('should return token for OAuth client', async () => {
      try {
        const credentials = await http.post(
          '/pl/standard/user/oauth/authorize',
          `grant_type=client_credentials&client_id=${OAUTH_CLIENT_ID}&client_secret=${OAUTH_CLIENT_SECRET}`,
          {
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
            },
          })
          .pipe(pluck('data'))
          .toPromise();

        expect(credentials).toHaveProperty('access_token');
        expect(credentials).toHaveProperty('token_type');
        expect(credentials).toHaveProperty('expires_in');
        expect(credentials).toHaveProperty('grant_type');
      } catch (e) {
        console.log(e);
        throw e;
      }
    });
  });
  describe('Authorized requests', () => {
    const EXT_CUSTOMER_ID = 'submerchant_A';
    let credentials: { access_token: string, token_type: string };
    beforeEach(async () => {
      credentials = await http.post(
        '/pl/standard/user/oauth/authorize',
        `grant_type=client_credentials&client_id=${OAUTH_CLIENT_ID}&client_secret=${OAUTH_CLIENT_SECRET}`,
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
        })
        .pipe(pluck('data'))
        .toPromise();
      console.log(credentials);
    });
    describe('Order', () => {

      it('should create order', async () => {
        try {
          const data: any = {
            customerIp: '127.0.0.1',
            merchantPosId: `${POS_ID}`,
            description: 'Test order',
            currencyCode: 'PLN',
            totalAmount: '1000',
            buyer: {
              email: 'test@example.com',
              phone: '+123 45 678-90-12',
              firstName: 'Testivy',
              lastName: 'Buyer',
              language: 'pl',
            },
            products: [
              {
                name: 'Визит няни',
                quantity: '1',
                unitPrice: '1000',
              },
            ],
            extOrderId: uuid(),
            shoppingCarts: [
              {
                extCustomerId: 'submerchant_A',
                amount: '1000',
                fee: 0,
                products: [
                  {
                    name: 'Визит няни',
                    quantity: '1',
                    unitPrice: '1000',
                  },
                ],
              },
            ],
          };
          const result = await http.post('/api/v2_1/orders', data, {
            headers: {
              ContentType: 'application/json',
              Authorization: `bearer ${credentials.access_token}`,
            },
            maxRedirects: 0,
            validateStatus: (status) => status >= 200 && status < 400,
          }).pipe(
            pluck('data'),
          ).toPromise();
          expect(result).toHaveProperty('status');
          expect(result.status.statusCode).toEqual('SUCCESS');
          expect(result).toHaveProperty('redirectUri');
          expect(result).toHaveProperty('orderId');
          console.log(result);
        } catch (e) {
          if (e.isAxiosError) {
            const { stack, config, ...err } = e.toJSON();
            console.error(err);
          }
          throw e;
        }
      });
    });
    describe('Merchant', () => {
      it('should generate merchant onboarding url', () => {
        const url = `https://secure.payu.com/boarding/#/form?lang=en&nsf=false&partnerId=${POS_ID}&marketplaceExtCustomerId=submerchant_A`;
        console.log(url);
      });
      it('should return verified submerchant status', async () => {
        const result = await http.get(`/api/v2_1/customers/ext/${EXT_CUSTOMER_ID}/status?currencyCode=PLN`, {
          headers: {
            Authorization: `bearer ${credentials.access_token}`,
          },
        })
          .pipe(
            pluck('data'),
          )
          .toPromise();
        expect(result.customerVerificationStatus).toEqual('Verified');
        console.log(result);
      });
      it('should return unverified submerchant status', async () => {
        const SUBMERCHANT_UNVERIFIED = 'submerchant_unverified';
        const result = await http.get(`/api/v2_1/customers/ext/${SUBMERCHANT_UNVERIFIED}/status?currencyCode=PLN`, {
          headers: {
            Authorization: `bearer ${credentials.access_token}`,
          },
        })
          .pipe(
            pluck('data'),
          )
          .toPromise();
        expect(result.customerVerificationStatus).toEqual('NotVerified');
        console.log(result);
      });
    });
  });
});
