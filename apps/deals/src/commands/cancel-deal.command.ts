import { DealCommand } from './deal.command';
import { Deal } from '../models';

export class CancelDealCommand extends DealCommand {
  constructor(deal: Deal, public readonly reason: 'cancelled' | 'suspended') {
    super(deal);
  }
}
