import OpenTok = require('opentok');
import { Injectable } from '@nestjs/common';

@Injectable()
export class OpentokAdapter {
  constructor(private opentok: OpenTok) {}

  createSession(
    options: OpenTok.SessionOptions = {},
  ): Promise<OpenTok.Session> {
    return new Promise<OpenTok.Session>((resolve, reject) => {
      this.opentok.createSession(
        {
          ...options,
          mediaMode: 'routed',
        },
        (error, session) => {
          if (error) {
            return reject(error);
          }
          resolve(session);
        },
      );
    });
  }

  createToken(
    sessionId: string,
    options: OpenTok.TokenOptions = {},
  ): Promise<string> {
    return new Promise((resolve, reject) => {
      try {
        const token = this.opentok.generateToken(sessionId, options);
        resolve(token);
      } catch (err) {
        reject(err);
      }
    });
  }

  async sendTerminationSignal(
    sessionId: string,
    data: any = {},
  ): Promise<void> {
    return new Promise(resolve => {
      this.opentok.signal(
        sessionId,
        undefined,
        { type: 'terminated', data },
        err => {
          resolve();
        },
      );
    });
  }
}
