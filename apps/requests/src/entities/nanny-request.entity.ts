import { AfterInsert, BeforeUpdate, Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { ParentRequest } from './parent-request.entity';
import { AggregateRoot } from '@nestjs/cqrs';
import * as assert from 'assert';
import { NannyRequestCreatedEvent, RequestAcceptedEvent } from '../cqrs/events';
import { NannyRequestStatusUpdatedEvent } from '../cqrs/events/impl/nanny-request-status-updated.event';
import { SameStatusChangeEvent } from '../cqrs/events/impl/same-status-change.event';

export const enum NannyRequestStatus {
  New = 'new', // created
  Accepted = 'accepted', // nanny accepted
  Offer = 'offer', // parent or nanny proposed a deal
  Refused = 'refused', // nanny refused
  Closed = 'closed', // nanny closed after acceptance
  Cancelled = 'cancelled', // parent cancelled
  Outdated = 'outdated', // request was active when date passed
  Suspended = 'suspended', // closed because was active when dealt with another
  Deal = 'deal', // deal created
}

@Entity('nanny-request')
export class NannyRequest extends AggregateRoot {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  user: number;

  @Column({
    transformer: {
      from(value: any): any {
        return Number(value) / 100;
      },
      to(value: number): any {
        return Math.floor(Number(value) * 100);
      },
    },
  })
  rate: number;

  @ManyToOne(() => ParentRequest, {
    onDelete: 'CASCADE',
  })
  parentRequest: ParentRequest;

  @Column({
    type: 'enum',
    enum: [
      NannyRequestStatus.New,
      NannyRequestStatus.Accepted,
      NannyRequestStatus.Offer,
      NannyRequestStatus.Refused,
      NannyRequestStatus.Closed,
      NannyRequestStatus.Cancelled,
      NannyRequestStatus.Suspended,
      NannyRequestStatus.Outdated,
      NannyRequestStatus.Deal,
    ],
    default: NannyRequestStatus.New,
  })
  status: NannyRequestStatus = NannyRequestStatus.New;

  @Column({
    type: 'boolean',
    default: true,
  })
  isActive = true;

  @CreateDateColumn({
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @UpdateDateColumn({ default: null, nullable: true })
  updatedAt: Date;

  accept() {
    if (this.status === NannyRequestStatus.Accepted) {
      this.apply(SameStatusChangeEvent.forNanny(this));
      return;
    }
    assert(
      this.status === NannyRequestStatus.New,
      'Nanny can accept only new request',
    );
    this.status = NannyRequestStatus.Accepted;
    this.apply(new RequestAcceptedEvent(this));
    this.apply(new NannyRequestStatusUpdatedEvent(this));
  }

  refuse() {
    if (this.status === NannyRequestStatus.Refused) {
      this.apply(SameStatusChangeEvent.forNanny(this));
      return;
    }
    assert(
      this.status === NannyRequestStatus.New,
      'Nanny can refuse only new request',
    );
    this.status = NannyRequestStatus.Refused;
    this.apply(new NannyRequestStatusUpdatedEvent(this));
    // this.apply(new RequestAcceptedEvent(this));
  }

  close() {
    if (this.status === NannyRequestStatus.Closed) {
      this.apply(SameStatusChangeEvent.forNanny(this));
      return;
    }
    assert(
      this.status === NannyRequestStatus.Accepted,
      'Request can be closed only after acceptance',
    );
    this.status = NannyRequestStatus.Closed;
    this.apply(new NannyRequestStatusUpdatedEvent(this));
  }

  cancel() {
    if (this.status === NannyRequestStatus.Cancelled) {
      this.apply(SameStatusChangeEvent.forNanny(this));
      return;
    }
    assert(this.isActive, 'Request is not active');
    this.status = NannyRequestStatus.Cancelled;
    this.apply(new NannyRequestStatusUpdatedEvent(this));
  }

  dealProposed() {
    assert(
      this.status === NannyRequestStatus.Accepted,
      'Deal can be offered only for accepted request',
    );
    this.status = NannyRequestStatus.Accepted;
    this.apply(new NannyRequestStatusUpdatedEvent(this));
  }

  offerAccepted() {
    if (this.status === NannyRequestStatus.Deal) {
      this.apply(SameStatusChangeEvent.forNanny(this));
      return;
    }
    this.status = NannyRequestStatus.Deal;
    this.apply(new NannyRequestStatusUpdatedEvent(this));
  }

  offerRefused() {
    if (this.status === NannyRequestStatus.Deal) {
      this.apply(SameStatusChangeEvent.forNanny(this));
      return;
    }
    this.status = NannyRequestStatus.Accepted;
    this.apply(new NannyRequestStatusUpdatedEvent(this));
  }

  suspend() {
    if (this.status === NannyRequestStatus.Suspended) {
      this.apply(SameStatusChangeEvent.forNanny(this));
      return;
    }
    this.status = NannyRequestStatus.Suspended;
    this.apply(new NannyRequestStatusUpdatedEvent(this));
  }

  markOutdated() {
    if (this.status === NannyRequestStatus.Outdated) {
      this.apply(SameStatusChangeEvent.forNanny(this));
      return;
    }
    this.status = NannyRequestStatus.Outdated;
    this.apply(new NannyRequestStatusUpdatedEvent(this));
  }

  @BeforeUpdate()
  beforeSave() {
    this.isActive = [
      NannyRequestStatus.New,
      NannyRequestStatus.Accepted,
      NannyRequestStatus.Offer,
    ].includes(this.status);
  }

  @AfterInsert()
  afterInsert() {
    this.apply(new NannyRequestCreatedEvent(this));
    this.apply(new NannyRequestStatusUpdatedEvent(this));
  }
}
