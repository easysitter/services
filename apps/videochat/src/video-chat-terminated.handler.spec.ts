import { VideoChatTerminatedHandler } from './video-chat-terminated.handler';
import { Test } from '@nestjs/testing';
import { VideoChatParticipantEntity } from './video-chat-participant.entity';
import { VideochatParticipantTerminated } from '@event-bus/contracts';
import { AggregateRoot, CqrsModule, EventPublisher } from '@nestjs/cqrs';
import { VideoChatTerminated } from './video-chat-terminated';

const mockClientProxy = {
  emit: jest.fn(),
};

describe('VideoChatTerminatedHandler', () => {
  let handler: VideoChatTerminatedHandler;
  let eventPublisher: EventPublisher;
  beforeEach(async () => {
    const testingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        VideoChatTerminatedHandler,
        {
          provide: 'EVENT_BUS',
          useValue: mockClientProxy,
        },
      ],
    }).compile();

    await testingModule.init();
    handler = testingModule.get(VideoChatTerminatedHandler);
    eventPublisher = testingModule.get(EventPublisher);
  });
  it('should be provided', () => {
    expect(handler).toBeDefined();
  });
  it('should send event to event bus', async () => {
    const FAKE_PARTICIPANT = {
      role: 'parent',
      userId: '1',
      sessionId: '1',
    } as VideoChatParticipantEntity;
    await handler.handle({
      participant: FAKE_PARTICIPANT,
    });

    expect(mockClientProxy.emit).toHaveBeenCalledWith(
      VideochatParticipantTerminated.channelName,
      expect.objectContaining({
        key: 'terminated',
        value: FAKE_PARTICIPANT,
      }),
    );
  });

  it('should handle VideoChatTerminated event', () => {
    const EventProducer = class extends AggregateRoot {};

    const FAKE_PARTICIPANT = eventPublisher.mergeObjectContext(
      new EventProducer(),
    );

    const event = new VideoChatTerminated(null);
    const spyHandle = jest.spyOn(handler, 'handle').mockResolvedValue();

    FAKE_PARTICIPANT.apply(event);
    FAKE_PARTICIPANT.commit();

    expect(spyHandle).toBeCalledWith(event);
  });
});
