import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { CreateAcceptedDealCommand } from '../../commands';
import { DealService } from '../../deal.service';
import { Deal } from '../../models';
import { DealExceptionEvent } from '../../events/deal-exception.event';

@CommandHandler(CreateAcceptedDealCommand)
export class CreateAcceptedDealCommandHandler implements ICommandHandler<CreateAcceptedDealCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly service: DealService,
  ) {
  }

  async execute(command: CreateAcceptedDealCommand): Promise<any> {
    const deal = this.publisher.mergeObjectContext(new Deal());
    try {
      deal.createAccepted(command.offer);
    } catch (e) {
      deal.apply(new DealExceptionEvent(deal, 'create-offer', e));
      deal.commit();
      throw e;
    }
    await this.service.save(deal);
    deal.commit();
    return deal;
  }
}
