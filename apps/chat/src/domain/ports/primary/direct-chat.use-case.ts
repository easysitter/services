import { Observable } from 'rxjs';
import { ChatModel } from '../../models';
import { CreateDirectChatCommand } from './create-direct-chat.command';

export abstract class DirectChatUseCase {
  abstract createDirectChat(command: CreateDirectChatCommand): Observable<ChatModel>;

  abstract findDirectChat(users: string[]): Observable<ChatModel>;

  abstract findDirectChatListForUser(user: string): Observable<ChatModel[]>;
}
