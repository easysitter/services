import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { DealConcluded } from '@event-bus/contracts';
import { DealConcludedEvent } from '../../events';

@EventsHandler(DealConcludedEvent)
export class DealConcludedHandler implements IEventHandler<DealConcludedEvent> {

  constructor(
    @Inject('EVENT_BUS')
    private channel: ClientProxy,
  ) {
  }

  async handle(command: DealConcludedEvent): Promise<void> {
    const { deal } = command;
    const msg = new DealConcluded({
      ...deal,
      request: deal.requestId,
    });

    this.channel.emit(msg.name, msg.payload);
  }
}
