import { ApiProperty } from '@nestjs/swagger';

export class ZipCheckResult {
  @ApiProperty()
  zip: string;
  @ApiProperty()
  districtId: string;
  @ApiProperty()
  zipBelongsToDistrict: boolean;
}
