export interface EventBusEvent<T extends any> {
  name: string;
  payload: T;
}
