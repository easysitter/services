import { Raw, Repository } from 'typeorm';
import { defer, Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { ChatDbEntity } from './entities/chat.db.entity';
import { ChatMapper } from './mappers/chat.mapper';

import { ChatId, ChatMeta, ChatModel } from '../../../domain/models';
import { ChatsPort } from '../../../domain/ports/secondary';

@Injectable()
export class ChatsDbAdapter implements ChatsPort {
  private mapper = new ChatMapper();

  constructor(
    @InjectRepository(ChatDbEntity)
    private readonly repo: Repository<ChatDbEntity>,
    private logger: Logger,
  ) {
  }

  loadById(chatId: ChatId): Observable<ChatModel> {
    return defer(() => this.repo.findOne(chatId)).pipe(
      map(data => this.mapper.toDomain(data)),
    );
  }

  loadByMetaForUsers(users: string[], meta: ChatMeta): Observable<ChatModel> {
    return defer(() => this.repo.findOne({
      where: {
        users: Raw(columnAlias => {
          const [table, column] = columnAlias.split('.');
          return `"${table}"."${column}"::jsonb @> \'${JSON.stringify(users)}\'`;
        }),
        meta: Raw(columnAlias => {
          const [table, column] = columnAlias.split('.');
          return `"${table}"."${column}"::jsonb @> \'${JSON.stringify(meta)}\'`;
        }),
      },
    })).pipe(
      map(data => this.mapper.toDomain(data)),
    );
  }

  loadAllByMetaForUser(user: string, meta: ChatMeta): Observable<ChatModel[]> {
    return defer(() => this.repo.createQueryBuilder('chat')
      .leftJoinAndSelect('chat.lastMessage', 'lastMessage')
      .where({
        users: Raw(columnAlias => {
          const [table, column] = columnAlias.split('.');
          return `"${table}"."${column}"::jsonb @> \'${JSON.stringify(user)}\'`;
        }),
        meta: Raw(columnAlias => {
          const [table, column] = columnAlias.split('.');
          return `"${table}"."${column}"::jsonb @> \'${JSON.stringify(meta)}\'`;
        }),
      })
      .orderBy('lastMessage.createdAt', 'DESC', 'NULLS LAST')
      .getMany(),
    ).pipe(
      map(list => list.map(data => this.mapper.toDomain(data))),
    );
  }

  save(model: ChatModel): Observable<boolean> {
    return of(this.mapper.fromDomain(model)).pipe(
      switchMap(entity => this.repo.save(entity)),
      map(() => true),
      catchError((err: Error) => {
        this.logger.error(err.message, err.stack, 'ChatsDbAdapter');
        return of(false);
      }),
    );
  }
}
