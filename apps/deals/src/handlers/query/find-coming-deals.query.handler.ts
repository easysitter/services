import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { FindComingDealsQuery } from '../../queries/find-coming-deals.query';
import { Deal } from '../../models';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@QueryHandler(FindComingDealsQuery)
export class FindComingDealsQueryHandler implements IQueryHandler<FindComingDealsQuery, Deal[]> {
  constructor(
    @InjectRepository(Deal)
    private repository: Repository<Deal>,
  ) {
  }

  execute(query: FindComingDealsQuery): Promise<Deal[]> {
    /**
     * @todo fix timezone
     */
    return this.repository.createQueryBuilder()
      .where(`"isActive" = true`)
      .andWhere(`"state" = 'deal'`)
      .andWhere(`"startedAt" IS NULL`)
      .andWhere(`("date" + "fromTime"::timetz)::timestamptz < (CURRENT_TIMESTAMP  + INTERVAL '2 hours')`)
      .getMany();
  }
}
