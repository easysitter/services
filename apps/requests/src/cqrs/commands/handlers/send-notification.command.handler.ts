import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';

import { SendNotificationCommand } from '../impl';

@CommandHandler(SendNotificationCommand)
export class SendNotificationCommandHandler
  implements ICommandHandler<SendNotificationCommand> {
  async execute(command: SendNotificationCommand): Promise<void> {
    Logger.log(
      `I will think about kafka later (${JSON.stringify(
        command.meta,
        null,
        2,
      )})`,
      ['Notification', command.type].join(']['),
      false,
    );
  }
}
