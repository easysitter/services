import { ApiProperty } from '@nestjs/swagger';

export class CreateSessionDto {
  @ApiProperty({
    example: '1',
  })
  parent: string;
  @ApiProperty({
    example: '1',
  })
  nanny: string;
}
