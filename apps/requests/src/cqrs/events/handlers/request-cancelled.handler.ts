import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { RequestCancelledEvent } from '../impl/request-cancelled.event';
import { Inject } from '@nestjs/common';
import { ClientKafka } from '@nestjs/microservices';
import { RequestCancelled } from '@event-bus/contracts';

@EventsHandler(RequestCancelledEvent)
export class RequestCancelledHandler implements IEventHandler<RequestCancelledEvent> {
  constructor(
    @Inject('EVENT_BUS')
    private eventBusClient: ClientKafka,
  ) {
  }

  handle(event: RequestCancelledEvent): any {
    const { request } = event;
    const message = new RequestCancelled({
      date: request.date.toISOString(),
      fromTime: request.fromTime,
      toTime: request.toTime,
      parent: request.user,
      id: request.id,
    });
    this.eventBusClient.emit(message.name, message.payload);
  }
}
