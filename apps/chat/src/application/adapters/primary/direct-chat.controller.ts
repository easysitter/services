import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { ApiParam, ApiTags } from '@nestjs/swagger';
import { CreateDirectChatCommand, DirectChatUseCase } from '../../../domain/ports/primary';
import { CreateDirectChatDto } from './dto/create-direct-chat.dto';

@Controller('/chat')
@ApiTags('chat', 'direct')
export class DirectChatController {

  constructor(
    private chat: DirectChatUseCase,
  ) {
  }

  @Get('/:user/direct')
  @ApiParam({
    name: 'user',
    type: String,
  })
  listDirect(
    @Param('user') user,
  ) {
    return this.chat.findDirectChatListForUser(user);
  }

  @Post('/:user/direct')
  @ApiParam({
    name: 'user',
    type: String,
  })
  createDirect(
    @Param('user') user,
    @Body() createDirectChatDto: CreateDirectChatDto,
  ) {
    return this.chat.createDirectChat(new CreateDirectChatCommand(
      [user, createDirectChatDto.directWith],
    ));
  }

  @Get('/:user/direct/:with')
  @ApiParam({
    name: 'user',
    type: String,
  })
  @ApiParam({
    name: 'with',
    type: String,
  })
  findDirect(
    @Param('user') user1,
    @Param('with') user2,
  ) {
    return this.chat.findDirectChat([user1, user2]);
  }
}
