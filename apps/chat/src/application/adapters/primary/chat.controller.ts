import { Body, Controller, Get, Param, Post, Put, Query, ValidationPipe } from '@nestjs/common';
import { ApiCreatedResponse, ApiOkResponse, ApiParam, ApiTags } from '@nestjs/swagger';

import { map } from 'rxjs/operators';
import { plainToClass } from 'class-transformer';

import { ChatHttpDto } from './dto/chat.http.dto';
import { ChatByIdPipe } from './chat-by-id.pipe';

import { ChatModel } from '../../../domain/models';
import { ChattingUseCase, MessageDto } from '../../../domain/ports/primary';
import { MessageHttpDto } from './dto/message.http.dto';
import { Observable } from 'rxjs';
import { ApiImplicitQuery } from '@nestjs/swagger/dist/decorators/api-implicit-query.decorator';
import { MarkMessagesSeenHttpDto } from './mark-messages-seen.http.dto';

@Controller('/chat')
@ApiTags('chat')
export class ChatController {
  constructor(
    private chat: ChattingUseCase,
  ) {
  }

  @Get('/:id')
  @ApiParam({
    name: 'id',
    description: 'Chat id',
    type: String,
  })
  @ApiOkResponse({ type: ChatHttpDto })
  getChat(@Param('id', ChatByIdPipe) chat: ChatModel): ChatHttpDto {
    return plainToClass(ChatHttpDto, chat, {
      strategy: 'exposeAll',
      excludeExtraneousValues: true,
    });
  }

  @Post('/:id/messages')
  @ApiTags('messages')
  @ApiParam({
    name: 'id',
    description: 'Chat id',
    type: String,
  })
  @ApiCreatedResponse({ type: MessageHttpDto })
  sendMessage(
    @Param('id', ChatByIdPipe) chat: ChatModel,
    @Body() data: MessageHttpDto,
  ): Observable<MessageHttpDto> {
    return this.chat.sendMessage(
      chat,
      new MessageDto(
        data.text,
        data.user,
      ),
    ).pipe(
      map(message => plainToClass(MessageHttpDto, message)),
    );
  }

  @Get('/:id/messages')
  @ApiParam({
    name: 'id',
    description: 'Chat id',
    type: String,
  })
  @ApiTags('messages')
  @ApiOkResponse({ type: MessageHttpDto, isArray: true })
  getMessages(@Param('id', ChatByIdPipe) chat: ChatModel) {
    return this.chat.getMessages(chat).pipe(
      map(messages => messages.map(message => plainToClass(MessageHttpDto, message))),
    );
  }

  @Get('/:id/unseen')
  @ApiParam({
    name: 'id',
    description: 'Chat id',
    type: String,
  })
  @ApiImplicitQuery({
    name: 'user',
    description: 'User id',
    type: String,
  })
  @ApiTags('messages')
  getUnseenCount(
    @Param('id') chat: ChatModel['id'],
    @Query('user') user: string,
  ) {
    return this.chat.getUnseenMessages(chat, user).pipe(
      map(count => ({ count })),
    );
  }

  @Put('/:id/unseen')
  @ApiParam({
    name: 'id',
    description: 'Chat id',
    type: String,
  })
  @ApiTags('messages')
  markSeen(
    @Param('id', ChatByIdPipe) chat: ChatModel,
    @Body(new ValidationPipe({transform: true})) {user, lastSeenMessageDate}: MarkMessagesSeenHttpDto,
  ) {
    return this.chat.markMessagesAsViewed(chat, lastSeenMessageDate, user).pipe(
      map(success => ({ success })),
    );
  }
}
