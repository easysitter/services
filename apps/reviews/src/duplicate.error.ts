export class DuplicateError<T> extends Error {
  static readonly message = 'already exists';
  constructor(public readonly data: T) {
    super(DuplicateError.message);
    Object.setPrototypeOf(this, DuplicateError.prototype);
  }
}
