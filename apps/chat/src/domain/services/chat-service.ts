import { Observable, of, throwError } from 'rxjs';
import { map, mapTo, switchMap } from 'rxjs/operators';
import { v4 as uuid } from 'uuid';

import { ChatId, ChatMessage, ChatModel } from '../models';
import { ChattingUseCase, MessageDto } from '../ports/primary';
import { ChatNotificationsPort, ChatsPort, MessagesPort } from '../ports/secondary';

export class ChatService implements ChattingUseCase {
  constructor(
    private readonly chatsPort: ChatsPort,
    private readonly messagesPort: MessagesPort,
    private readonly notificationsPort: ChatNotificationsPort,
  ) {
  }

  findChatById(id: ChatId): Observable<ChatModel> {
    return this.chatsPort.loadById(id);
  }

  sendMessage(chat: ChatModel, data: MessageDto): Observable<ChatMessage> {
    const { text, user } = data;
    const message = new ChatMessage(
      uuid(),
      text,
      user,
      new Date(),
    );
    try {
      chat.addMessage(message);
    } catch (e) {
      return throwError(e);
    }
    return this.messagesPort.save(chat).pipe(
      switchMap(result => {
        if (!result) {
          return throwError(new Error('failed to save message'));
        } else {
          return this.chatsPort.save(chat).pipe(
            switchMap(
              () => this.notificationsPort.notifyNewMessage(chat, message).pipe(mapTo(message)),
            ),
          );
        }
      }),
    );
  }

  getMessages(chat: ChatModel): Observable<ChatMessage[]> {
    return this.messagesPort.loadByChatId(chat.id);
  }

  getUnseenMessages(chat: ChatId, user: string): Observable<number> {
    return this.messagesPort.loadByChatId(chat).pipe(
      map(messages => messages.filter(message => !message.isViewedBy(user))),
      map(messages => messages.length),
    );
  }

  markMessagesAsViewed(chat: ChatModel, lastMessageDate: Date, user: string): Observable<boolean> {
    if (!chat.users.includes(user)) {
      return of(false);
    }
    return this.messagesPort.loadByChatId(chat.id).pipe(
      map(messages => messages.filter(message => message.createdAt <= lastMessageDate)),
      map(messages => messages.map(message => {
        message.addViewedBy(user);
        return message;
      })),
      switchMap(messages => this.messagesPort.saveViewed(messages)),
    );
  }
}
