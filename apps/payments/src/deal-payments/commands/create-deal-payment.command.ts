export class CreateDealPaymentCommand {
  constructor(
    public readonly deal: string,
    public readonly user: string,
    public readonly amount: number,
    public readonly currency: string,
  ) {
  }

  static of(
    deal: string,
    user: string,
    amount: number,
    currency: string,
  ) {
    return new CreateDealPaymentCommand(deal, user, amount, currency);
  }
}
