import { Test } from '@nestjs/testing';
import { OpentokAdapter } from './opentok.adapter';
import OpenTok = require('opentok');

describe('OpentokAdapter', () => {
  const FAKE_SESSION_ID = 'FAKE_SESSION_ID';
  const FAKE_SESSION = {
    sessionId: FAKE_SESSION_ID,
  };
  const mockOpenTok = {
    createSession: jest.fn(),
    generateToken: jest.fn(),
    signal: jest.fn(),
  };
  let opentokAdapter: OpentokAdapter;
  beforeEach(async () => {
    const testingModule = await Test.createTestingModule({
      providers: [OpentokAdapter, { provide: OpenTok, useValue: mockOpenTok }],
    }).compile();

    opentokAdapter = testingModule.get(OpentokAdapter);
  });
  describe('create session', () => {
    it('should return object with sessionId', async () => {
      mockOpenTok.createSession.mockImplementation((options, cb) =>
        cb(null, FAKE_SESSION),
      );

      const session = await opentokAdapter.createSession();

      expect(session).toHaveProperty('sessionId', FAKE_SESSION_ID);
    });

    it('should handle opentok error', () => {
      mockOpenTok.createSession.mockImplementation((options, cb) =>
        cb(new Error()),
      );

      expect(opentokAdapter.createSession()).rejects.toBeInstanceOf(Error);
    });
  });

  describe('create token', () => {
    it('should return token', async () => {
      const FAKE_TOKEN = 'fake token';
      mockOpenTok.generateToken.mockImplementation(() => FAKE_TOKEN);
      const token: string = await opentokAdapter.createToken(FAKE_SESSION_ID);
      expect(token).toBe(FAKE_TOKEN);
    });
    it('should return token', async () => {
      const FAKE_TOKEN = 'fake token';
      const FAKE_OPTIONS = {};
      mockOpenTok.generateToken.mockImplementation(() => FAKE_TOKEN);
      await opentokAdapter.createToken(FAKE_SESSION_ID, FAKE_OPTIONS);
      expect(mockOpenTok.generateToken).toBeCalledWith(
        FAKE_SESSION_ID,
        FAKE_OPTIONS,
      );
    });

    it('should handle generate token error', () => {
      mockOpenTok.generateToken.mockImplementation((): never => {
        throw new Error();
      });

      expect(
        opentokAdapter.createToken(FAKE_SESSION_ID, null),
      ).rejects.toBeInstanceOf(Error);
    });
  });

  describe('send termination signal', () => {
    it('should send signal and resolve', async () => {
      const FAKE_DATA = {};
      mockOpenTok.signal.mockImplementation((sessionId, _, data, cb) =>
        cb(null),
      );
      await opentokAdapter.sendTerminationSignal(FAKE_SESSION_ID, FAKE_DATA);
      expect(mockOpenTok.signal).toBeCalledWith(
        FAKE_SESSION_ID,
        undefined,
        expect.objectContaining({ type: 'terminated', data: FAKE_DATA }),
        expect.anything(),
      );
    });
  });
});
