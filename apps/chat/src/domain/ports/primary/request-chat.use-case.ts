import { ChatModel } from '../../models';
import { Observable } from 'rxjs';
import { CreateChatForRequestCommand } from './create-chat-for-request.command';

export abstract class RequestChatUseCase {
  abstract createChatForRequest(command: CreateChatForRequestCommand): Observable<ChatModel>;

  abstract findChatForRequest(users: string[], request: number): Observable<ChatModel>;
}
