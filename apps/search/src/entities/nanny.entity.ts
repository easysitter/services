import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class Nanny {
  @PrimaryColumn()
  id: number;

  @Column({ type: 'numeric' })
  rate: number;

  @Column()
  currency: string;

  @Column()
  acceptTrial: boolean;

  @Column({
    type: 'jsonb',
  })
  allowedChildren: number[];

  @Column({
    type: 'int',
  })
  maxChildrenCount: number;
}
