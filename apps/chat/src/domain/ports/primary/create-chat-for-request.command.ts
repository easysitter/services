export class CreateChatForRequestCommand {
  constructor(
    public readonly users: string[],
    public readonly request: number) {
  }
}
