import { Observable } from 'rxjs';

import { ChatId, ChatMeta, ChatModel } from '../../models';

export interface ChatsPort {
  save(model: ChatModel): Observable<boolean>;

  loadById(chatId: ChatId): Observable<ChatModel>;

  loadByMetaForUsers(users: string[], meta: ChatMeta): Observable<ChatModel>;

  loadAllByMetaForUser(user: string, meta: ChatMeta): Observable<ChatModel[]>;
}
