import { EventBusEvent } from '../event-bus-event';

interface RequestAcceptedPayload {
  request: number;
  parent: number;
  nanny: number;
}

export class RequestAccepted implements EventBusEvent<RequestAcceptedPayload> {
  static readonly eventName = 'request_accepted';

  public readonly name = RequestAccepted.eventName;

  constructor(
    public readonly payload: RequestAcceptedPayload,
  ) {
  }
}
