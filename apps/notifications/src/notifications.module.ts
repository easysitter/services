import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PushToken } from './push-token.entity';
import { PushNotificationsController } from './push-notifications.controller';
import { PushService } from './push.service';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { LoggingInterceptor } from '../../requests/src/logging.interceptor';
import { MESSAGING } from './tokens';
import { resolve } from 'path';
import * as admin from 'firebase-admin';
import { PushNotification } from './push-notification.entity';

@Module({
  imports: [

    TypeOrmModule.forRoot({
      type: 'postgres',
      database: process.env.DB_NAME,
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      username: process.env.DB_USER_NAME,
      password: process.env.DB_USER_PASSWORD,
      schema: process.env.DB_SCHEMA || 'public',
      entities: [PushToken, PushNotification],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([PushToken, PushNotification]),
  ],
  controllers: [
    PushNotificationsController,
  ],
  providers: [
    PushService,
    {
      provide: MESSAGING,
      useFactory: () => {
        admin.initializeApp({
          credential: admin.credential.cert(resolve(process.env.FIREBASE_SECRET_PATH)),
        });
        return admin.messaging();
      },
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor,
    },
  ],
})
export class NotificationsModule {
}
