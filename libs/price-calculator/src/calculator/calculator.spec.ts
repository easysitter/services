import { Calculator } from './calculator';
import { calculatorFactory } from './calculator-factory';
import { parse } from 'date-fns';
import { CalculatorParams } from './calculator-params';
import { CalculatorTracer, NoopTracer } from '../utils/calculator-tracer';

describe('Calculator', () => {
  let calculator: Calculator;
  let tracer: CalculatorTracer;
  beforeEach(() => {
    calculator = calculatorFactory({
      dayRate: 10,
      nightRate: 100,
      overtimeStep: 30,
    });
    tracer = new NoopTracer();
  });

  it('should work', async () => {
    const deal: CalculatorParams = {
      from: parse('20:00+01', 'HH:mmX', new Date('2020-12-12')),
      to: parse('02:00+01', 'HH:mmX', new Date('2020-12-13')),
      children: [1, 1],
    };
    expect(await calculator.getPrice(deal, tracer)).toBe(
      // day
      10 +
      // night
      5 * 100,
    );
  });

  it('should apply all plugins', async () => {
    const deal: CalculatorParams = {
      from: parse('18:00+02', 'HH:mmX', new Date()),
      to: parse('22:00+02', 'HH:mmX', new Date()),
      startedAt: parse('10:00+02', 'HH:mmX', new Date()),
      finishedAt: parse('23:00+02', 'HH:mmX', new Date()),
      children: [1, 1, 1, 1],
    };
    expect(await calculator.getPrice(deal, tracer)).toBe(
      // day
      (21 - 10) * (10 + 7 + 7) +
      // night
      (23 - 21) * (100 + 7 + 7),
    );
  });
  it('should take fractional values', async () => {
    const deal: CalculatorParams = {
      from: parse('08:00+02', 'HH:mmX', new Date()),
      to: parse('12:15+02', 'HH:mmX', new Date()),
      children: [1, 1, 1, 1],
    };
    expect(await calculator.getPrice(deal, tracer)).toBe(
      (12.25 - 8) * (10 + 7 + 7),
    );
  });
  describe('Corner cases', () => {

    it('should handle xx to 7:00+02', async () => {
      const deal: CalculatorParams = {
        from: parse('05:00+02', 'HH:mmX', new Date()),
        to: parse('07:00+02', 'HH:mmX', new Date()),
        children: [1, 1],
      };
      expect(await calculator.getPrice(deal, tracer)).toBe(200);
    });
    it('should handle from 21:00+02 to xx ', async () => {
      const deal: CalculatorParams = {
        from: parse('21:00+02', 'HH:mmX', new Date()),
        to: parse('23:00+02', 'HH:mmX', new Date()),
        children: [1, 1],
      };
      expect(await calculator.getPrice(deal, tracer)).toBe(200);
    });
    it('should handle from 7:00+02 to xx ', async () => {
      const deal: CalculatorParams = {
        from: parse('07:00+02', 'HH:mmX', new Date()),
        to: parse('09:00+02', 'HH:mmX', new Date()),
        children: [1, 1],
      };
      expect(await calculator.getPrice(deal, tracer)).toBe(20);
    });
    it('should handle xx to 21:00+02 ', async () => {
      const deal: CalculatorParams = {
        from: parse('19:00+02', 'HH:mmX', new Date()),
        to: parse('21:00+02', 'HH:mmX', new Date()),
        children: [1, 1],
      };
      expect(await calculator.getPrice(deal, tracer)).toBe(20);
    });
  });
});
