export class OutdatedDealsQuery {
  constructor(
    public readonly outdatedPeriod: number,
  ) {
  }

  static of(outdatedPeriod: number) {
    return new OutdatedDealsQuery(outdatedPeriod);
  }
}
