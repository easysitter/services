import { EventBusEvent } from '../event-bus-event';

interface VisitFinishedPayload {
  id: number;
  trial: boolean;
  safePayment: boolean;
  rate: number;
  date: Date;
  fromTime: string;
  toTime: string;
  startedAt: Date;
  finishedAt: Date;
  nanny: number;
  parent: number;
  children: number[];
}

export class VisitFinished implements EventBusEvent<VisitFinishedPayload> {
  static eventName = 'visit_finished';

  readonly name = VisitFinished.eventName;

  constructor(
    public readonly payload: VisitFinishedPayload,
  ) {
  }
}
