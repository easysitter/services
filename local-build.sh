EXTRACT_ALL='Object.entries(this.projects)'
FILTER_APPLICATIONS='.filter(([_, data]) => data.type==="application" )'
MAP_TO_COMMAND='.map(([name]) => `nest build ${name}` ).join(" && ")'
cat nest-cli.json \
  | fx "${EXTRACT_ALL}${FILTER_APPLICATIONS}${MAP_TO_COMMAND}" \
  | bash -
