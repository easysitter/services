import { Payment } from '../../entities/payment';

export class CancelDealPaymentsCommand {
  constructor(
    public readonly payments: Payment[],
  ) {
  }

  static of(dealPayments: Payment[]) {
    return new CancelDealPaymentsCommand(dealPayments);
  }
}
