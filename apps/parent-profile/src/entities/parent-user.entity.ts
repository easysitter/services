import { Entity, ManyToOne, PrimaryColumn } from 'typeorm';
import { ParentProfile } from './parent-profile.entity';

@Entity()
export class ParentUser {
  @PrimaryColumn()
  userId: string;

  @ManyToOne(() => ParentProfile, {
    eager: true,
    cascade: ['insert', 'update'],
  })
  profile: ParentProfile;
}
