import { Injectable, PipeTransform } from '@nestjs/common';

import { ChattingUseCase } from '../../../domain/ports/primary';

@Injectable()
export class ChatByIdPipe implements PipeTransform<string> {
  constructor(private chat: ChattingUseCase) {}

  async transform(value: string) {
    return this.chat.findChatById(value).toPromise();
  }
}
