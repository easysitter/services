import {
  AfterInsert,
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import * as assert from 'assert';
import { AggregateRoot } from '@nestjs/cqrs';
import { RequestCreatedEvent } from '../cqrs/events';
import { NannyRequest } from './nanny-request.entity';
import { RequestCancelledEvent } from '../cqrs/events/impl/request-cancelled.event';
import { addDays, isBefore, parse, parseISO } from 'date-fns';
import { SameStatusChangeEvent } from '../cqrs/events/impl/same-status-change.event';
import { RequestOutdatedEvent } from '../cqrs/events/impl';

export const enum ParentRequestStatus {
  Active = 'active',
  Cancelled = 'cancelled',
  Outdated = 'outdated',
  Deal = 'deal',
}

@Entity('parent-request')
export class ParentRequest extends AggregateRoot {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({
    default: '',
  })
  trackId: string;

  @Column()
  user: number;

  @Column({
    type: 'timestamp',
  })
  date: Date;

  @Column()
  fromTime: string;

  @Column()
  toTime: string;

  @Column({
    type: 'timestamp with time zone',
  })
  startTs: Date;

  @Column({
    type: 'timestamp with time zone',
  })
  endTs: Date;

  @Column({
    type: 'jsonb',
  })
  children: number[];

  @Column({ type: 'boolean' })
  trial: boolean;

  @Column({
    default: '',
  })
  address: string;

  @Column({
    default: '',
    nullable: true,
  })
  zipCode: string;

  @Column({
    default: '',
  })
  phone: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  comment: string;

  @Column({
    type: 'boolean',
    default: true,
  })
  isActive = true;
  @Column({
    type: 'enum',
    enum: [
      ParentRequestStatus.Active,
      ParentRequestStatus.Cancelled,
      ParentRequestStatus.Outdated,
      ParentRequestStatus.Deal,
    ],
    default: ParentRequestStatus.Active,
  })
  status: ParentRequestStatus = ParentRequestStatus.Active;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;

  @OneToMany(
    () => NannyRequest,
    (nannyRequest) => nannyRequest.parentRequest,
    {
      cascade: true,
    },
  )
  nannyRequests: NannyRequest[];

  deal() {
    assert(this.isActive, 'Only active request can be dealt');
    this.status = ParentRequestStatus.Deal;
  }

  cancel() {
    if (this.status === ParentRequestStatus.Cancelled) {
      this.apply(SameStatusChangeEvent.forParent(this));
      return;
    }
    assert(this.isActive, 'Only active request can be cancelled');
    this.status = ParentRequestStatus.Cancelled;
    this.apply(new RequestCancelledEvent(this));
  }

  markOutdated() {
    if (this.status === ParentRequestStatus.Outdated) {
      this.apply(SameStatusChangeEvent.forParent(this));
      return;
    }
    assert(this.isActive, 'Only active request can be outdated');
    this.status = ParentRequestStatus.Outdated;
    this.apply(new RequestOutdatedEvent(this));
  }

  @BeforeInsert()
  beforeInsert() {
    /**
     * @todo fix timezone
     */
    const timezone = '';
    const fromTime = `${this.fromTime}${timezone}`;
    const toTime = `${this.toTime}${timezone}`;

    const date = parseISO(this.date as unknown as string);
    this.startTs = parse(fromTime, 'HH:mmX', date);
    this.endTs = parse(toTime, 'HH:mmX', date);
    if (isBefore(this.endTs, this.startTs)) {
      this.endTs = addDays(this.endTs, 1);
    }
    this.isActive = this.status === ParentRequestStatus.Active;
  }

  @AfterInsert()
  afterInsert() {
    this.apply(new RequestCreatedEvent(this));
  }

  @BeforeUpdate()
  beforeUpdate() {
    this.isActive = this.status === ParentRequestStatus.Active;
  }
}
