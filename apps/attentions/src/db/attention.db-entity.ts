import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Attention {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  type: string;

  @Column()
  refType: string;

  @Column()
  refId: string;

  @Column({ type: 'jsonb' })
  data?: any;
}
