import { Body, Controller, Get, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { PaymentService } from '../services/payment.service';
import { ServiceSubscriptionService } from '../services/service-subscription.service';
import { PaymentProvider } from '../payment-providers/payment-provider';
import { CalculatorRates } from '@utils/price-calculator';

@Controller('/payments')
export class PaymentController {
  constructor(
    private readonly payment: PaymentService,
    private readonly calculatorRates: CalculatorRates,
    private readonly paymentProvider: PaymentProvider,
    private readonly subscriptions: ServiceSubscriptionService,
  ) {
  }

  @Get('settings')
  async getSettings() {
    const dayRate = await this.calculatorRates.getDayRate();
    const nightRate = await this.calculatorRates.getNightRate();
    return {
      rates: {
        day: dayRate,
        night: nightRate,
      },
    };
  }

  @Post('/notifications/payu/')
  @HttpCode(HttpStatus.OK)
  async handleNotification(@Body() { order }) {
    if (!order) {
      return;
    }
    const { extOrderId, status } = order;
    if (status !== 'COMPLETED') {
      return;
    }

    const payment = await this.payment.getPaymentById(extOrderId);
    if (!payment) {
      return;
    }
    await this.paymentProvider.completePayment(payment);
    const subscription = await this.subscriptions.getSubscriptionForPayment(payment);
    if (!subscription) {
      return;
    }
    await this.subscriptions.activate(subscription);
    // console.log(data);
  }
}
