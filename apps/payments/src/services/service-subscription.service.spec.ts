import { TestingModule } from '@nestjs/testing';
import { ServiceSubscriptionService } from './service-subscription.service';
import { v4 as uuid } from 'uuid';
import { setUpTestingModule, tearDownTestingModule } from './utils.spec';

describe('ServiceSubscriptionService', () => {
  let module: TestingModule;
  let serviceSubscriptionService: ServiceSubscriptionService;
  let dbName: string;
  const userId = 'p1';
  beforeEach(async () => {
    dbName = `${uuid()}.test.sql`;
    module = await setUpTestingModule(dbName);
    serviceSubscriptionService = module.get<ServiceSubscriptionService>(ServiceSubscriptionService);
  });
  afterEach(async () => {
    await tearDownTestingModule(module, dbName);
  });

  async function createSubscription() {
    return serviceSubscriptionService.createSubscription({
      userId,
    });
  }

  async function getActiveSubscription() {
    return await serviceSubscriptionService.getActiveSubscription(userId);
  }

  it('should create ServiceSubscriptionService', () => {
    expect(serviceSubscriptionService).toBeDefined();
    expect(serviceSubscriptionService).not.toBeNull();
  });

  it('should create service subscription', async () => {
    const subscription = await createSubscription();
    expect(subscription.isActive).toBe(false);
  });

  it('should activate subscription', async () => {
    const subscription = await createSubscription();
    await serviceSubscriptionService.activate(subscription);
    expect(subscription.isActive).toBe(true);
  });

  it('should find user subscription', async () => {
    const subscription = await createSubscription();
    expect(await getActiveSubscription()).not.toBeDefined();
    await serviceSubscriptionService.activate(subscription);
    expect(await getActiveSubscription()).toBeDefined();
  });

  it('should find currently active subscription', async () => {
    const subscription1 = await createSubscription();
    const subscription2 = await createSubscription();

    await serviceSubscriptionService.activate(subscription2);
    const activeSubscription = await getActiveSubscription();
    expect(activeSubscription.id).toEqual(subscription2.id);
  });
});
