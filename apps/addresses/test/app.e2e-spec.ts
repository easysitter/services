import { Test, TestingModule } from '@nestjs/testing';
import { AddressesModule } from '../src/addresses.module';

describe('AppController (e2e)', () => {
  let app;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AddressesModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });
});
