import { IEvent } from '@nestjs/cqrs';
import { ParentRequest } from '../../../entities/parent-request.entity';

export class RequestCancelledEvent implements IEvent {
  constructor(public readonly request: ParentRequest) {
  }
}
