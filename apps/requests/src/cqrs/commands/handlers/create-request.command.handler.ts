import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { Repository, Transaction, TransactionRepository } from 'typeorm';

import { CreateRequestCommand } from '../impl';
import { CreateRequestDto } from '../../../dto/create-request.dto';

import { ParentRequest } from '../../../entities/parent-request.entity';
import { NannyRequest } from '../../../entities/nanny-request.entity';

@CommandHandler(CreateRequestCommand)
export class CreateRequestCommandHandler
  implements ICommandHandler<CreateRequestCommand> {
  constructor(private readonly publisher: EventPublisher) {
  }

  async execute(command: CreateRequestCommand): Promise<ParentRequest> {
    return this.create(command.data);
  }

  @Transaction({
    isolation: 'READ COMMITTED',
  })
  private async create(
    data: CreateRequestDto,
    @TransactionRepository(NannyRequest)
      nannyRequestRepo?: Repository<NannyRequest>,
    @TransactionRepository(ParentRequest)
      parentRequestRepo?: Repository<ParentRequest>,
  ) {
    const parentRequest = this.publisher.mergeObjectContext(
      parentRequestRepo.create({
        user: data.parent,
        date: data.date,
        fromTime: data.fromTime,
        toTime: data.toTime,
        children: data.children,
        trial: data.trial,
        address: data.address,
        zipCode: data.zipCode,
        phone: data.phone,
        comment: data.comment,
        trackId: data.trackId,
      }),
    );

    await parentRequestRepo.save(parentRequest);
    parentRequest.commit();

    const nannyRequests = data.nannies.map(nanny => {
        const nannyRequest = this.publisher.mergeObjectContext(
          nannyRequestRepo.create({
            user: nanny.id,
            rate: nanny.rate,
            parentRequest,
          }),
        );
        nannyRequest.accept();
        return nannyRequest;
      },
    );
    await nannyRequestRepo.save(nannyRequests);
    nannyRequests.forEach(r => r.commit());
    parentRequest.nannyRequests = nannyRequests;
    return parentRequest;
  }
}
