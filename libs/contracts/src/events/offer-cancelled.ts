import { EventBusEvent } from '../event-bus-event';

interface OfferCancelledPayload {
  deal: number;
  request: string;
  parent: number;
  nanny: number;
}

export class OfferCancelled implements EventBusEvent<OfferCancelledPayload> {
  static readonly eventName = 'offer_refused';

  public readonly name = OfferCancelled.eventName;

  constructor(
    public readonly payload: OfferCancelledPayload,
  ) {
  }
}
