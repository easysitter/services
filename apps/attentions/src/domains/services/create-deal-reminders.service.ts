import { AttentionEntity } from '../entities/attention.entity';
import { AttentionGroupEntity } from '../entities/attention-group.entity';
import { AttentionTargetEntity } from '../entities/attention-target.entity';

import { CreateDealRemindersUseCase } from '../ports/in/create-deal-reminders.use-case';
import { CreateDealRemindersCommand } from '../ports/in/create-deal-reminders.command';

import { SaveAttentionsPort } from '../ports/out/save-attentions.port';
import { LoadDealsToRemindPort } from '../ports/out/load-deals-to-remind.port';
import { LoadAttentionsPort } from '../ports/out/load-attentions.port';
import { LoadAttentionsQuery } from '../ports/out/load-attentions.query';
import { NotifyNewAttentionsPort } from '../ports/out/notify-new-attentions.port';

export class CreateDealRemindersService implements CreateDealRemindersUseCase {
  constructor(
    private readonly loadDealsToRemindPort: LoadDealsToRemindPort,
    private readonly saveAttentionPort: SaveAttentionsPort,
    private readonly loadAttentionsPort: LoadAttentionsPort,
    private readonly notifier: NotifyNewAttentionsPort,
  ) {
  }

  async createReminders(command: CreateDealRemindersCommand): Promise<AttentionGroupEntity> {
    const { attentionType } = command;
    const targets = await this.loadDealsToRemindPort.load();

    const attentions = await this.createAttentions(targets, attentionType);

    const attentionGroup = this.createAttentionGroup(attentions);

    const newAttentions = await this.saveAttentionPort.save(attentionGroup);
    await this.notifier.notify(newAttentions);
    return newAttentions;
  }

  private createAttentionGroup(attentions: AttentionEntity[]) {
    return attentions
      .reduce(
        (group, attention) => group.addAttention(attention),
        new AttentionGroupEntity(),
      );
  }

  private async createAttentions(targets: AttentionTargetEntity[], attentionType: string) {
    const attentions = await Promise.all(targets.map(target => this.createAttentionForTarget(attentionType, target)));
    return attentions.filter(attention => attention !== null);
  }

  private async createAttentionForTarget(attentionType: string, target: AttentionTargetEntity): Promise<AttentionEntity | null> {
    const query: LoadAttentionsQuery = {
      attentionType,
      targetType: target.refType,
      targetId: target.refId,
    };
    const existedAttentions = await this.loadAttentionsPort.loadAttentions(query);
    if (existedAttentions.length !== 0) {
      return null;
    }
    return AttentionEntity.of(attentionType, target);
  }
}
