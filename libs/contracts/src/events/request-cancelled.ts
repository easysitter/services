import { EventBusEvent } from '../event-bus-event';

interface RequestCancelledPayload {
  id: number;
  parent: number;
  date: string;
  fromTime: string;
  toTime: string;
}

export class RequestCancelled implements EventBusEvent<RequestCancelledPayload> {
  static readonly eventName = 'request-cancelled';

  public readonly name = RequestCancelled.eventName;

  constructor(
    public readonly payload: RequestCancelledPayload,
  ) {
  }
}
