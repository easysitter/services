import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { SameStatusChangeEvent } from '../impl/same-status-change.event';
import { NannyRequest } from '../../../entities/nanny-request.entity';
import { ParentRequest } from '../../../entities/parent-request.entity';
import { ClientKafka } from '@nestjs/microservices';
import { Inject } from '@nestjs/common';

@EventsHandler(SameStatusChangeEvent)
export class SameStatusChangeHandler implements IEventHandler<SameStatusChangeEvent<NannyRequest | ParentRequest>> {
  constructor(
    @Inject('EVENT_BUS')
    private kafka: ClientKafka,
  ) {
  }

  handle(event: SameStatusChangeEvent<NannyRequest | ParentRequest>): any {
    let key;
    let value;
    const { request } = event;
    if (request instanceof NannyRequest) {
      key = 'nanny-request-same-status';

      value = {
        id: request.id,
        status: request.status,
        nanny: request.user,
        parent: request.parentRequest.user,
      };
    } else {
      key = 'parent-request-same-status';

      value = {
        id: request.id,
        status: request.status,
        parent: request.user,
      };
    }
    this.kafka.emit('exceptions', { key, value: { ...value, type: key } });
  }
}
