import { ApiProperty } from '@nestjs/swagger';

export class SearchForRequestHttpDto {
  @ApiProperty()
  request: number;

  @ApiProperty()
  nanny: string;
}
