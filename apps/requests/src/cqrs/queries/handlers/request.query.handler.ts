import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { RequestQuery } from '../impl';

import { ParentRequest } from '../../../entities/parent-request.entity';

@QueryHandler(RequestQuery)
export class RequestQueryHandler implements IQueryHandler<RequestQuery> {
  constructor(
    @InjectRepository(ParentRequest)
    private readonly parentRequestRepository: Repository<ParentRequest>,
  ) {}

  execute(query: RequestQuery): Promise<ParentRequest> {
    return this.parentRequestRepository.findOne(query.id, {
      relations: ['nannyRequests'],
    });
  }
}
