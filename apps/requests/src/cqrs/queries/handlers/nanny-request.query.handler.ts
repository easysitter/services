import { EventPublisher, IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { NannyRequestQuery } from '../impl';

import { NannyRequest } from '../../../entities/nanny-request.entity';
import { ParentRequest } from '../../../entities/parent-request.entity';

@QueryHandler(NannyRequestQuery)
export class NannyRequestQueryHandler
  implements IQueryHandler<NannyRequestQuery> {
  constructor(
    private readonly publisher: EventPublisher,
    @InjectRepository(NannyRequest)
    private readonly nannyRequest: Repository<NannyRequest>,
  ) {}

  execute(query: NannyRequestQuery): Promise<any> {
    let { parentRequest } = query;
    if (!(parentRequest instanceof ParentRequest)) {
      parentRequest = { id: parentRequest } as ParentRequest;
    }
    return this.nannyRequest.findOne({
      where: {
        user: query.nanny,
        parentRequest,
      },
      relations: ['parentRequest'],
    });
  }
}
