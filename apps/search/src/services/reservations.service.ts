import { Injectable } from '@nestjs/common';
import { Reserve } from '../entities/reserve.entity';
import { Brackets, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { TimeService } from './time.service';

export interface TimeRange {
  start: number;
  end: number;
}

export interface ReserveCreateOptions {
  date: Date;
  timeRange: TimeRange;
  user: number;
  reason: string;
  reasonRef: string;
}

export interface ReserveFindOptions {
  date: Date;
  time: TimeRange;
  users?: number[];
}

@Injectable()
export class ReservationsService {
  constructor(
    @InjectRepository(Reserve) private readonly repository: Repository<Reserve>,
    private readonly time: TimeService,
  ) {}

  create(options: ReserveCreateOptions) {
    const reservation = this.repository.create({
      date: options.date,
      day: options.date.getDay(),
      start: options.timeRange.start,
      end: options.timeRange.end,
      user: options.user,
      reason: options.reason,
      reasonRef: options.reasonRef,
    });
    return this.repository.save(reservation);
  }

  async remove(options: { reason: string; reasonRef: string }) {
    await this.repository.delete({
      reason: options.reason,
      reasonRef: options.reasonRef,
    });
  }

  async hasReserve(user: number, date: Date, time: TimeRange) {
    const count = await this.createQuery(date, time)
      .andWhere('user=:user', { user })
      .getCount();
    return count > 0;
  }

  async getReserved(options: ReserveFindOptions): Promise<number[]> {
    const { date, time, users } = options;

    const queryBuilder = this.createQuery(date, time);
    queryBuilder.select('DISTINCT ("user")', 'user');

    if (users && users.length) {
      queryBuilder.andWhere('"user" IN (:...users)', { users });
    }
    const result = await queryBuilder.getRawMany();
    return result.map(r => r.user);
  }

  private createQuery(date: Date, time: TimeRange) {
    return this.repository
      .createQueryBuilder()
      .where('date=:date', {
        date: `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`,
      })
      .andWhere(
        new Brackets(qb => {
          qb.where(':start BETWEEN "start" AND "end"', { start: time.start })
            .orWhere(':end BETWEEN "start" AND "end"', { end: time.end })
            .orWhere(':start < "start" AND :end > "end"', {
              start: time.start,
              end: time.end,
            });
        }),
      );
  }
}
