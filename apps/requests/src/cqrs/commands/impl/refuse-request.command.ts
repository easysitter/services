import { NannyRequest } from '../../../entities/nanny-request.entity';
import { ICommand } from '@nestjs/cqrs';

export class RefuseRequestCommand implements ICommand {
  constructor(public readonly request: NannyRequest) {}
}
