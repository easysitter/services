import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { DealCancelledEvent } from '../../events';
import { Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { DealCancelled } from '@event-bus/contracts';

@EventsHandler(DealCancelledEvent)
export class DealCancelledHandler implements IEventHandler<DealCancelledEvent> {

  constructor(
    @Inject('EVENT_BUS')
    private channel: ClientProxy,
  ) {
  }

  handle(event: DealCancelledEvent): any {
    const { deal } = event;
    const msg = new DealCancelled({
      ...deal,
      request: deal.requestId,
    });

    this.channel.emit(msg.name, msg.payload);
  }
}
