import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Inject } from '@nestjs/common';
import { ClientKafka } from '@nestjs/microservices';
import { RequestCancelled } from '@event-bus/contracts';
import { RequestOutdatedEvent } from '../impl';

@EventsHandler(RequestOutdatedEvent)
export class RequestOutdatedHandler implements IEventHandler<RequestOutdatedEvent> {
  constructor(
    @Inject('EVENT_BUS')
    private eventBusClient: ClientKafka,
  ) {
  }

  handle(event: RequestOutdatedEvent): any {
    const { request } = event;
    const message = new RequestCancelled({
      date: request.date.toISOString(),
      fromTime: request.fromTime,
      toTime: request.toTime,
      parent: request.user,
      id: request.id,
    });
    this.eventBusClient.emit(message.name, message.payload);
  }
}
