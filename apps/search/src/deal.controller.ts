import { Body, Controller, Post } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { ReservationsService } from './services/reservations.service';
import { TimeService } from './services/time.service';
import { DealCancelled, DealConcluded, VisitFinished } from '@event-bus/contracts';
import { addMinutes } from 'date-fns';

@Controller('/reserve')
export class DealController {
  constructor(
    private reservations: ReservationsService,
    private time: TimeService,
  ) {
  }

  @EventPattern(DealConcluded.eventName)
  async newDealEvent({ value }: { value: DealConcluded['payload'] }) {
    const { date, fromTime, toTime, id, nanny } = value;
    const startDate = new Date(date);
    const deal: DealDTO = {
      id,
      startAt: +addMinutes(startDate, this.time.parse(fromTime)),
      endAt: +addMinutes(startDate, this.time.parse(toTime)),
      user: nanny,
    };
    return this.onDealCreated(deal);
  }

  @EventPattern(DealCancelled.eventName)
  async dealCancelledEvent({ value }: { value: DealCancelled['payload'] }) {
    return this.onDealCancelled(value.id.toString());
  }

  @EventPattern(VisitFinished.eventName)
  async visitFinishedEvent({ value }: { value: VisitFinished['payload'] }) {
    return this.onVisitFinished(value.id.toString());
  }

  @EventPattern('deal_closed')
  async dealClosedEvent(deal: DealDTO) {
    return this.onDealClosed(deal);
  }

  @Post('deal')
  async newDeal(@Body() deal: DealDTO) {
    return this.onDealCreated(deal);
  }

  @Post('deal-closed')
  async dealClosed(@Body() deal: DealDTO) {
    return this.onDealClosed(deal);
  }

  private async onVisitFinished(id: string) {
    return this.reservations.remove({
      reason: 'deal',
      reasonRef: id,
    });
  }

  private async onDealCancelled(id: string) {
    return this.reservations.remove({
      reason: 'deal',
      reasonRef: id,
    });
  }

  private async onDealClosed(deal: DealDTO) {
    return this.reservations.remove({
      reason: 'deal',
      reasonRef: deal.id,
    });
  }

  private async onDealCreated(deal: DealDTO) {
    const { startAt, endAt } = deal;

    const start = new Date(startAt);
    const end = new Date(endAt);

    await this.reservations.create({
      user: deal.user,
      reason: 'deal',
      reasonRef: deal.id.toString(),
      date: start,
      timeRange: {
        start: this.time.normalize(start.getHours(), start.getMinutes()),
        end: this.time.normalize(end.getHours(), end.getMinutes()),
      },
    });
  }
}

interface DealDTO {
  id: any;
  user: number;
  startAt: number;
  endAt: number;
}
