import { NannyRequest } from '../../../entities/nanny-request.entity';

export class ParentRequestByNannyRequestIdQuery {
  constructor(
    public id: NannyRequest['id'],
  ) {
  }
}
