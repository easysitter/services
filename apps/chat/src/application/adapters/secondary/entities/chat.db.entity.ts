import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { MessageDbEntity } from './message.db.entity';

@Entity('chat')
export class ChatDbEntity {
  @PrimaryGeneratedColumn('uuid')
  @ApiProperty()
  id: string;

  @Column({ type: 'jsonb' })
  @ApiProperty()
  users: string[];

  @Column({ type: 'jsonb' })
  @ApiProperty()
  meta: any;

  @OneToOne(() => MessageDbEntity, {
    nullable: true,
    eager: true,
  })
  @JoinColumn()
  lastMessage: MessageDbEntity;
}
