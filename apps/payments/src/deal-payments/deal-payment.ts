import { Payment } from '../entities/payment';
import { Column, CreateDateColumn, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class DealPayment {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  dealId: string;

  @Column()
  userId: string;

  @OneToOne(() => Payment, null, { eager: true, cascade: ['insert', 'update'] })
  @JoinColumn()
  payment: Payment;

  @CreateDateColumn()
  createdAt: Date;
}
