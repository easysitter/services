import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { SuspendOutdatedDealCommand } from '../../commands';
import { Logger } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Deal } from '../../models';
import { InjectRepository } from '@nestjs/typeorm';

@CommandHandler(SuspendOutdatedDealCommand)
export class SuspendOutdatedDealCommandHandler implements ICommandHandler<SuspendOutdatedDealCommand> {
  constructor(
    private logger: Logger,
    @InjectRepository(Deal)
    private repo: Repository<Deal>,
    private eventPublisher: EventPublisher,
  ) {
  }

  async execute(command: SuspendOutdatedDealCommand): Promise<void> {
    const deal = this.eventPublisher.mergeObjectContext(command.deal);
    try {
      deal.cancel('suspended');
      await this.repo.save(deal);
      deal.commit();
    } catch (e) {
      this.logger.error(e.message, e.stackTrace, 'SuspendOutdatedDealCommandHandler');
    }
  }
}
