import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@Entity()
export class ParentProfile {
  @PrimaryGeneratedColumn()
  @ApiProperty()
  id: number;

  @Column()
  @ApiProperty()
  name: string;

  @Column({ nullable: true })
  @ApiProperty()
  birthDate: string;

  @Column({ nullable: false, default: '' })
  @ApiProperty()
  photo: string;

  @Column()
  @ApiProperty()
  phone: string;

  @Column({
    default: '',
  })
  @ApiProperty()
  description: string;
  @Column({
    default: '',
  })
  @ApiProperty()
  address: string;
  @Column({
    default: '',
  })
  @ApiProperty()
  zipCode: string;

  @Column({ type: 'jsonb' })
  @ApiProperty({ type: Number, isArray: true })
  children: number[];
}
