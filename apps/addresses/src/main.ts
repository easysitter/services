import { NestFactory } from '@nestjs/core';
import { AddressesModule } from './addresses.module';
import { patchNest } from '@utils/transport-connector';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  patchNest();
  const app = await NestFactory.create(AddressesModule);
  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);

  const options = new DocumentBuilder()
    .setTitle('Addresses api')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  const port = process.env.HTTP_PORT || 3333;
  SwaggerModule.setup('/docs', app, document);
  await app.listen(port, () => {
    console.log('Listening at http://localhost:' + port + '/' + globalPrefix);
    console.log('Listening at http://localhost:' + port + '/docs');
  });
}
bootstrap();
