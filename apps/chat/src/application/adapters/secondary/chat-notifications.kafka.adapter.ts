import { ClientProxy } from '@nestjs/microservices';
import { Inject, Injectable } from '@nestjs/common';
import { ChatNotificationsPort } from '../../../domain/ports/secondary/chat-notifications.port';
import { ChatMessage, ChatModel } from '../../../domain/models';
import { Observable } from 'rxjs';
import { ChatMessageSent } from '@event-bus/contracts';

@Injectable()
export class ChatNotificationsKafkaAdapter implements ChatNotificationsPort {
  constructor(
    @Inject('EVENT_BUS')
    private events: ClientProxy,
  ) {
  }

  notifyNewChat(chat: ChatModel): Observable<void> {
    return this.events.emit('chat', {
      key: 'created',
      value: {
        id: chat.id,
        meta: chat.meta,
        users: chat.users,
      },
    });
  }

  notifyNewMessage(chat: ChatModel, message: ChatMessage): Observable<void> {
    const msg = new ChatMessageSent({
      chat: {
        id: chat.id,
        meta: chat.meta,
        users: chat.users,
      },
      message: {
        id: message.id,
        text: message.text,
        user: message.user,
        createdAt: message.createdAt,
      },
    });
    return this.events.emit(msg.name, msg.payload);
  }
}
