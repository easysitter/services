import { NannyRequest } from '../../../entities/nanny-request.entity';
import { ParentRequest } from '../../../entities/parent-request.entity';

export class SameStatusChangeEvent<T extends NannyRequest | ParentRequest = NannyRequest | ParentRequest> {

  constructor(
    public readonly request: T,
  ) {
  }

  static forParent(request: ParentRequest) {
    return new SameStatusChangeEvent(request);
  }

  static forNanny(request: NannyRequest) {
    return new SameStatusChangeEvent(request);
  }
}
