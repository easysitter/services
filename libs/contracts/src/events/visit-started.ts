import { EventBusEvent } from '../event-bus-event';

interface VisitStartedPayload {
  id: number;
  startedAt: Date;
  nanny: number;
  parent: number;
}

export class VisitStarted implements EventBusEvent<VisitStartedPayload> {
  static eventName = 'visit_started';

  readonly name = VisitStarted.eventName;

  constructor(
    public readonly payload: VisitStartedPayload,
  ) {
  }
}
