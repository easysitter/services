import { EventPattern } from '@nestjs/microservices';
import { ScheduleService } from './services/schedule.service';
import { Body, Controller, Post, ValidationPipe } from '@nestjs/common';
import { NannyProfileUpdated, ScheduleUpdated } from '@event-bus/contracts';
import { NannyService } from './services/nanny.service';

@Controller('/schedule')
export class ScheduleController {
  constructor(
    private scheduleService: ScheduleService,
    private nannyService: NannyService,
  ) {
  }

  @EventPattern(ScheduleUpdated.eventName)
  async onScheduleUpdated(
    { value }: { value: ScheduleUpdated['payload'] },
  ) {
    await this.scheduleService.update(value.user, value.items);
  }

  @EventPattern(NannyProfileUpdated.eventName)
  onProfileUpdated(
    { value }: { value: NannyProfileUpdated['payload'] },
  ) {
    return this.nannyService.update(
      this.nannyService.create(value),
    );
  }

  @Post('schedule-updated')
  async updateSchedule(@Body(ValidationPipe) schedule: ScheduleUpdated['payload']) {
    await this.scheduleService.update(schedule.user, schedule.items);
  }
}
