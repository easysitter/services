import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { CloseNannyRequestCommand } from '../impl';

import { NannyRequest } from '../../../entities/nanny-request.entity';

@CommandHandler(CloseNannyRequestCommand)
export class CloseNannyRequestCommandHandler
  implements ICommandHandler<CloseNannyRequestCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    @InjectRepository(NannyRequest)
    private readonly nannyRequest: Repository<NannyRequest>,
  ) {}

  async execute(command: CloseNannyRequestCommand): Promise<void> {
    const request = this.publisher.mergeObjectContext(command.request);
    request.close();
    await this.nannyRequest.save(request);
    request.commit();
  }
}
