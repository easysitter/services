import { EventBusEvent } from '../event-bus-event';

interface DealConcludedPayload {
  id: number;
  date: Date;
  fromTime: string;
  toTime: string;
  request: string;
  nanny: number;
  parent: number;
  initiator: 'parent' | 'nanny';
  children: number[];
}

export class DealConcluded implements EventBusEvent<DealConcludedPayload> {
  static eventName = 'deal_concluded';

  readonly name = DealConcluded.eventName;

  constructor(
    public readonly payload: DealConcludedPayload,
  ) {
  }
}
