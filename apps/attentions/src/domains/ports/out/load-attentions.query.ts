import { AttentionType } from '../../entities/attention.entity';
import { AttentionTargetType } from '../../entities/attention-target.entity';

interface LoadAttentionsByTypeQuery {
  attentionType?: AttentionType;
}

interface LoadAttentionsByTargetTypeQuery {
  targetType?: AttentionTargetType;
}

interface LoadAttentionsByTargetQuery {
  targetType: AttentionTargetType;
  targetId: string;
}

export type LoadAttentionsQuery =
  | LoadAttentionsByTypeQuery
  | LoadAttentionsByTargetTypeQuery
  | LoadAttentionsByTargetQuery;
