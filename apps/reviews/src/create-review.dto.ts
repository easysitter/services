import { IsNotEmpty, IsNumber, IsOptional, IsString, Max, Min } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateReviewDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  refType: string;
  @IsNotEmpty()
  @ApiProperty()
  refId: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  userType: string;
  @IsNotEmpty()
  @ApiProperty()
  userId: string;

  @IsNotEmpty()
  @IsNumber()
  @Max(5)
  @Min(1)
  @ApiProperty()
  rating: number;
  @IsOptional()
  @ApiProperty()
  pros: string;
  @IsOptional()
  @ApiProperty()
  cons: string;
}
