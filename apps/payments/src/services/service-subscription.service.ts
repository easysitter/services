import { Inject, Injectable } from '@nestjs/common';
import { ServiceSubscription } from '../entities/service-subscription';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Payment } from '../entities/payment';
import { PaymentProvider } from '../payment-providers/payment-provider';
import { ClientProxy } from '@nestjs/microservices';

interface CreateSubscriptionParams {
  userId: string;
  returnUrl?: string;
}

const SUBSCRIPTION_PAYMENT_TYPE = 'subscription';

@Injectable()
export class ServiceSubscriptionService {
  constructor(
    @Inject('EVENT_BUS')
    private eventBus: ClientProxy,
    @InjectRepository(ServiceSubscription)
    private readonly serviceSubscriptionRepository: Repository<ServiceSubscription>,
    private readonly paymentProvider: PaymentProvider,
  ) {
  }

  async createSubscription(params: CreateSubscriptionParams): Promise<ServiceSubscription> {
    const { userId } = params;
    let subscription = await this.getActiveSubscription(userId);
    if (subscription) {
      return subscription;
    }
    const payment: Payment = await this.paymentProvider.createPayment({
      amount: 10,
      currency: 'PLN',
      type: SUBSCRIPTION_PAYMENT_TYPE,
      meta: {
        returnUrl: params.returnUrl,
        name: 'Подписка на сервис',
      },
    });
    subscription = this.serviceSubscriptionRepository.create({
      ...params,
      payment,
    });
    await this.serviceSubscriptionRepository.save(subscription);
    return subscription;
  }

  async activate(subscription: ServiceSubscription): Promise<void> {
    subscription.activate();
    await this.serviceSubscriptionRepository.save(subscription);
    this.eventBus.emit('payments', {
      key: 'subscription_activated',
      value: {
        user: subscription.userId,
        isActive: subscription.isActive,
      },
    });
  }

  async getSubscriptionForPayment(payment: Payment) {
    if (payment.type !== SUBSCRIPTION_PAYMENT_TYPE) {
      return null;
    }
    return this.serviceSubscriptionRepository.findOne({
      where: {
        payment,
      },
    });
  }

  async getActiveSubscription(userId: string) {
    return await this.serviceSubscriptionRepository.findOne({
      where: {
        userId,
        isActive: true,
      },
    });
  }

}
