import { Controller, Get, Post } from '@nestjs/common';
import { FindAttentionsByTargetTypeUseCase } from './domains/ports/in/find-attentions-by-target-type.use-case';
import { CreateOutdatedRequestRemindersCommand } from './domains/ports/in/create-outdated-request-reminders.command';
import { CreateOutdatedRequestRemindersUseCase } from './domains/ports/in/create-outdated-request-reminders.use-case';

@Controller('/requests/outdated')
export class OutdatedRequestAttentionsController {
  constructor(
    private createOutdatedRequestReminders: CreateOutdatedRequestRemindersUseCase,
    private findAttentionsByTargetType: FindAttentionsByTargetTypeUseCase,
  ) {
  }

  @Post('/reminding')
  async createOutdatedRequestReminding() {
    const command = new CreateOutdatedRequestRemindersCommand();
    const result = await this.createOutdatedRequestReminders.createReminders(command);
    return { message: 'Create reminders', result };
  }

  @Get('')
  async getOutdatedRequestAttentions() {
    return this.findAttentionsByTargetType.find('outdated-request');
  }
}
