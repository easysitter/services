import { Observable } from 'rxjs';

import { ChatId, ChatMessage, ChatModel } from '../../models';

export interface MessagesPort {
  loadByChatId(chatId: ChatId): Observable<ChatMessage[]>;

  save(chat: ChatModel): Observable<boolean>;

  saveViewed(messages: ChatMessage[]): Observable<boolean>;
}
