import { parseISO } from 'date-fns';
import { Calculator } from './calculator';
import { DayNightSplitCalculator } from './day-night.calculator';
import { PlainCalculator } from './plain.calculator';
import { PlainRate } from '../rates/plain.rate';
import { CalculatorTracer, NoopTracer } from '../utils/calculator-tracer';

describe('DayNight Calculator', () => {
  let calculator: Calculator;
  let tracer: CalculatorTracer;
  beforeEach(() => {
    calculator = new DayNightSplitCalculator(
      new PlainCalculator(new PlainRate(10)),
      new PlainCalculator(new PlainRate(100)),
    );
    tracer = new NoopTracer();
  });
  describe('Day only', () => {
    it('should cost 120PLN for 12 day hours', async () => {
      const deal = {
        from: parseISO('2020-09-01T08:00:00+02'),
        to: parseISO('2020-09-01T20:00:00+02'),
      }; // 👈 всё днем (12 часов) 120PLN
      expect(await calculator.getPrice(deal, tracer)).toEqual(120);
    });
  });
  describe('Night only', () => {
    it('should cost 800PLN for 8 night hours', async () => {
      const deal = {
        from: parseISO('2020-09-01T22:00:00+02'),
        to: parseISO('2020-09-02T06:00:00+02'),
      }; // 👈 всё ночью (8 часов) 800PLN
      expect(await calculator.getPrice(deal, tracer)).toEqual(800);
    });
  });
  describe('Day to night', () => {
    it('should cost 230PLN for 13 day hours and 1 night hour', async () => {
      const deal = {
        from: parseISO('2020-09-01T08:00:00+02'),
        to: parseISO('2020-09-01T22:00:00+02'),
      }; // 👈 пересекает вечер (13 часов днем + 1 час вечером) 230PLN
      expect(await calculator.getPrice(deal, tracer)).toEqual(230);
    });
  });
  describe('Night to day', () => {
    it('should cost 910PLN for 9 night hours and 1 day hour', async () => {
      const deal = {
        from: parseISO('2020-09-01T22:00:00+02'),
        to: parseISO('2020-09-02T08:00:00+02'),
      }; // 👈 пересекает вечер (9 часов ночью + 1 час утром) 910PLN
      expect(await calculator.getPrice(deal, tracer)).toEqual(910);
    });
  });
});
