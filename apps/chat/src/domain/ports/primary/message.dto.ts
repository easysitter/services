export class MessageDto {
  constructor(
    public readonly text: string,
    public readonly user: string,
  ) {
  }
}
