import { Rates } from './rates';
import { CalculatorParams } from '../calculator/calculator-params';

export class ExtraChildrenRate extends Rates {
  constructor(
    private wrapped: Rates,
  ) {
    super();
  }

  async getRate(deal: CalculatorParams): Promise<number> {
    const wrappedRate = await this.wrapped.getRate(deal);
    if (!deal.children || !deal.children.length) {
      return wrappedRate;
    }
    return Math.max(0, deal.children.length - 2) * 7 + wrappedRate;
  }
}
