FROM node:12-alpine AS deps

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm ci
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .
EXPOSE 3000

FROM deps as deals-build
RUN npm run build deals
RUN npm prune --production

FROM node:12-alpine AS deals
WORKDIR /usr/src/app
COPY package*.json ./
COPY --from=deals-build /usr/src/app/node_modules ./node_modules
COPY --from=deals-build /usr/src/app/dist/apps/deals ./dist/apps/deals
CMD [ "node", "dist/apps/deals/main.js" ]

