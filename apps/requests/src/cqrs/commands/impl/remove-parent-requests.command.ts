import { ICommand } from '@nestjs/cqrs';
import { ParentRequest } from '../../../entities/parent-request.entity';

export class RemoveParentRequestsCommand implements ICommand {
  constructor(public readonly parent: ParentRequest['user']) {}
}
