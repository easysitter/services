import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class PushNotification {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  user: string;

  @Column({
    type: 'jsonb',
  })
  notification: any;

  @Column({
    type: 'boolean',
    default: false,
  })
  isSent: boolean;

  @Column({
    type: 'jsonb',
  })
  messageIds: any[];
}
