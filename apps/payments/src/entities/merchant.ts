import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Merchant {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true })
  userId: string;

  @Column()
  extId: string;

  @Column()
  paymentProviderId: string;

  @Column()
  isConfirmed: boolean;

  @Column({ type: 'jsonb' })
  meta: any;

}
