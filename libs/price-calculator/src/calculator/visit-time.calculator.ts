import { addMinutes, differenceInMinutes } from 'date-fns';
import { CalculatorTracer } from '../utils/calculator-tracer';
import { Calculator } from './calculator';
import { CalculatorParams } from './calculator-params';

export class VisitTimeCalculator extends Calculator {
  constructor(
    private wrapped: Calculator,
    private step: number,
    private overtimeOnly: boolean = false,
  ) {
    super();
  }

  async getPrice(
    deal: CalculatorParams,
    tracer: CalculatorTracer,
  ): Promise<number> {
    const { startedAt, finishedAt } = deal;

    if (!startedAt || !finishedAt) {
      return this.dealPrice(deal, tracer);
    }
    const dealDuration = differenceInMinutes(deal.to, deal.from);
    const visitDuration = differenceInMinutes(finishedAt, startedAt);

    if (dealDuration >= visitDuration || visitDuration - dealDuration <= 15) {
      return this.dealPrice(deal, tracer);
    }
    return this.visitPrice(deal, tracer);
  }

  private async dealPrice(deal: CalculatorParams, tracer: CalculatorTracer) {
    return await this.wrapped.getPrice(deal, tracer.fork('DEAL'));
  }

  private async visitPrice(deal: CalculatorParams, tracer: CalculatorTracer) {
    const { startedAt, finishedAt, ...rest } = deal;
    const visitDuration = differenceInMinutes(finishedAt, startedAt);
    const from = startedAt;
    const to = addMinutes(
      startedAt,
      Math.ceil(visitDuration / this.step) * this.step,
    );
    const totalPrice = await this.wrapped.getPrice(
      {
        ...rest,
        from,
        to,
        startedAt,
        finishedAt,
      },
      tracer.fork('VISIT'),
    );
    if (!this.overtimeOnly) {
      return totalPrice;
    }
    const dealPrice = await this.dealPrice(deal, tracer);

    return totalPrice - dealPrice;
  }
}
