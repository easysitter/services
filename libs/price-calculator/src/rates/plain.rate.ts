import { Rates } from './rates';
import { CalculatorParams } from '../calculator/calculator-params';

export class PlainRate extends Rates {
  constructor(
    private rate: number,
  ) {
    super();
  }

  async getRate(deal: CalculatorParams): Promise<number> {
    return this.rate;
  }
}
