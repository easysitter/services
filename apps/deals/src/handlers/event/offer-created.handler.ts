import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { OfferCreated } from '@event-bus/contracts';
import { OfferCreatedEvent } from '../../events';

@EventsHandler(OfferCreatedEvent)
export class OfferCreatedHandler implements IEventHandler<OfferCreatedEvent> {

  constructor(
    @Inject('EVENT_BUS')
    private channel: ClientProxy,
  ) {
  }

  async handle(command: OfferCreatedEvent): Promise<void> {
    const { deal } = command;
    const msg = new OfferCreated({
      deal: deal.id,
      request: deal.requestId,
      nanny: deal.nanny,
      parent: deal.parent,
      initiator: deal.initiator,
    });

    this.channel.emit(msg.name, msg.payload);
  }
}
