import { CreateMerchantParams, CreatePaymentParams, MerchantVerificationResult, PaymentProvider } from './payment-provider';
import { Payment } from '../entities/payment';
import { Merchant } from '../entities/merchant';
import { MerchantService } from '../services/merchant.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { HttpService, Inject, Injectable } from '@nestjs/common';
import { PAYU_BASE_URL, PAYU_NOTIFY_URL, PAYU_OAUTH_CLIENT_ID, PAYU_OAUTH_CLIENT_SECRET, PAYU_POSID } from '../providers';
import { v4 as uuid } from 'uuid';
import { pluck } from 'rxjs/operators';

@Injectable()
export class PauyProvider extends PaymentProvider {

  constructor(
    private http: HttpService,
    private merchantService: MerchantService,
    @InjectRepository(Payment)
    private paymentRepository: Repository<Payment>,
    @Inject(PAYU_BASE_URL)
    private readonly  baseUrl: string,
    @Inject(PAYU_NOTIFY_URL)
    private readonly  notifyUrl: string,
    @Inject(PAYU_POSID)
    private readonly  posId: string,
    @Inject(PAYU_OAUTH_CLIENT_ID)
    private readonly  oauthClientId: string,
    @Inject(PAYU_OAUTH_CLIENT_SECRET)
    private readonly  oauthSecret: string,
  ) {
    super();
  }

  async completePayment(payment: Payment): Promise<Payment> {
    payment.complete();
    await this.paymentRepository.save(payment);
    return payment;
  }

  createMerchant(params: CreateMerchantParams): Promise<Merchant> {
    return undefined;
  }

  async createPayment(params: CreatePaymentParams): Promise<Payment> {
    const id = uuid();
    const payment = this.paymentRepository.create({
      id,
      isCompleted: false,
      amount: params.amount,
      currency: params.currency,
      extId: '',
      meta: params.meta || {},
      type: params.type,
    });
    await this.paymentRepository.save(payment);

    const data: any = {
      notifyUrl: this.notifyUrl,
      continueUrl: params.meta.returnUrl,
      customerIp: '127.0.0.1',
      merchantPosId: `${this.posId}`,
      description: params.meta.name,
      currencyCode: payment.currency,
      totalAmount: `${payment.amount * 100}`,
      buyer: {
        language: 'pl',
      },
      products: [
        {
          name: params.meta.name,
          quantity: '1',
          unitPrice: `${payment.amount * 100}`,
        },
      ],
      extOrderId: payment.id,
    };
    const result = await this.createOrder(data);
    payment.meta.redirect_uri = result.redirectUri;
    payment.extId = result.orderId;
    await this.paymentRepository.save(payment);
    return payment;
  }

  private async createOrder(data: any) {
    const credentials = await this.getToken();
    return await this.http.post<{ redirectUri: string, orderId: string }>('/api/v2_1/orders', data, {
      headers: {
        ContentType: 'application/json',
        Authorization: `bearer ${credentials.access_token}`,
      },
      baseURL: this.baseUrl,
      maxRedirects: 0,
      validateStatus: (status) => status >= 200 && status < 400,
    }).pipe(
      pluck('data'),
    ).toPromise();
  }

  getOnBoardingUrl(account: Merchant): Promise<string> {
    return undefined;
  }

  handleVerification(data: any): Promise<MerchantVerificationResult> {
    return undefined;
  }

  private async getToken(): Promise<{ access_token: string }> {
    return await this.http.post<{ access_token: string }>(
      '/pl/standard/user/oauth/authorize',
      `grant_type=client_credentials&client_id=${this.oauthClientId}&client_secret=${this.oauthSecret}`,
      {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        baseURL: this.baseUrl,
      })
      .pipe(pluck('data'))
      .toPromise();
  }
}
