import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { RequestAcceptedEvent } from '../impl';
import { RequestAccepted } from '@event-bus/contracts';
import { Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@EventsHandler(RequestAcceptedEvent)
export class RequestAcceptedHandler implements IEventHandler<RequestAcceptedEvent> {
  constructor(
    @Inject('EVENT_BUS')
    private eventBus: ClientProxy,
  ) {
  }

  handle(event: RequestAcceptedEvent): any {
    const { request } = event;
    const cmd = new RequestAccepted({
      request: request.parentRequest.id,
      nanny: request.user,
      parent: request.parentRequest.user,
    });
    this.eventBus.emit(cmd.name, cmd.payload);
  }
}
