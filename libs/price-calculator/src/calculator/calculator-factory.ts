import { ExtraChildrenRate } from '../rates/extra-children.rate';
import { PlainCalculator } from './plain.calculator';
import { DayNightSplitCalculator } from './day-night.calculator';
import { PlainRate } from '../rates/plain.rate';
import { VisitTimeCalculator } from './visit-time.calculator';

interface CalculatorFactoryParams {
  dayRate: number;
  nightRate: number;
  overtimeStep?: number;
  onlyOvertime?: boolean;
}

export function calculatorFactory({
                                    dayRate,
                                    nightRate,
                                    overtimeStep = 30,
                                    onlyOvertime = false,
                                  }: CalculatorFactoryParams) {
  const daySplit = new DayNightSplitCalculator(
    new PlainCalculator(new ExtraChildrenRate(new PlainRate(+dayRate))),
    new PlainCalculator(new ExtraChildrenRate(new PlainRate(+nightRate))),
  );
  return new VisitTimeCalculator(daySplit, overtimeStep, onlyOvertime);
}
